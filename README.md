# avaluos

Proyecto Avalúos V1
Version laravel: 6.0

Autores: Arredondo, José (arredondojose8@gmail.com) Marcano, Lezaida (lezaida@gmail.com) Galindo, javier (delpierojg_16@hotmail.com)

FTP SSH web622.webfaction.com Credenciales heredia f3rhumaya17

Cambiar version de php alias php="/usr/local/bin/php71"

Comando git :q! para salir de vim

Base de datos Producción avaluos avaluosuser avaluospass

Test
	avaluos_test
	avaluosuser
	avaluospass

URL http://avaluos.fesoluciones.com.mx/public/

url-dev http://avaluos.test.fesoluciones.com.mx/public/

Pasos de instalación:
1) Ejecutar comando composer install
2) Configurar base de datos con nombre backorder
3) Crea usuario de base de datos, user: workuser Password=workpass
4) Ejecutar comando php artisan storage:link (Crear enlace simbolico a storage)

Comando GIT:
- :x salir de vim de git bash en windows

Comandos de laravel comunes
- php artisan route:list (Listado de rutas)
- php artisan migrate:refresh (Permite realizar roolback a toda la bd y volver a ejecutar todas las migraciones)
- php artisan db:seed (Ejecuta los datos semillas)
- php artisan krlove:generate:model User --table-name=user (Permite generar los modelos a partir de la db)
- php artisan storage:link (Crear enlace simbolico a storage)
- php artisan make:migration create_users_table (Crear migracion)
- php artisan migrate (Ejecuta las migraciones pendientes)
- php artisan migrate:rollback (Retornar antes de la ultima ejecucion de una migración)
- php artisan cache:clear
- php artisan route:cache
- php artisan config:clear
- php artisan view:clear
- php artisan config:cache

Paquetes extras instalados

1) Collective Html Helper composer require laravelcollective/html
2) Laravel Breadcrumbs composer require davejamesmiller/laravel-breadcrumbs:5.x
3) Laravel fpdf composer require crabbly/fpdf-laravel
Pasos para crear nueva funcionalidad
1) Crear controlador y funcion a trabajar
2) Crear semilla (Verificar como es la semilla dependiendo de la acción, por ejemplo create tiene su forma, storage tiene su forma)
3) Crear la ruta en web
4) Hacer migraciones y cargar semillas (debe ejecutar los sql de gastos y tipogasto)
5) Programar la funcion en el controlador
6) Anexar vista si es necesario
7) En vista debes crear en routes->breadcrumbs.php el que usaras en la vista (mapa de navegacion)
8) copiar archivo de vendor\symfony\http-foundation\request.php

Semillas
dependede => -1   (eliminar)
dependede => -2   (mostrar)
dependede => -3   (index)

Pasos para ejecutar archivos muy grande
1) Situarse en xamp\mysql\bin
2) Colocar archivo en particion origen C
3) Ejecutar comando mysql -u usuario -p nombredb < C:\avaluos.sql