<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudio extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estudios';

    /**
     * @var array
     */
    protected $fillable = ['id', 'idnivelestudio', 'idtituloprofesional','cedula','estatus','comentario','iduser', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nivelestudio()
    {
        return $this->belongsTo('App\Nivelestudio', 'idnivelestudio', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tituloprofesional()
    {
        return $this->belongsTo('App\Tituloprofesional', 'idtituloprofesional', 'id');
    }
}