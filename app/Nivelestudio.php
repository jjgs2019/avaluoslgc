<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivelestudio extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nivelestudios';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre', 'estatus', 'created_at', 'updated_at'];
}