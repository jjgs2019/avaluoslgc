<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitudAprobada extends Mailable
{
    use Queueable, SerializesModels;

    public $data=[];

    //public $subject='Haz sido asignado a un Avalúo';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.solicitud.aprobada')->subject($this->data["subject"])->from('siaepudo@gmail.com', 'Notificaciones LGC');
    }
}
