<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detallesolicitud extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'detallessolicitudes';

    /**
     * @var array
     */
    protected $fillable = ['id', 'idusuario', 'idtipobien', 'idsolicitud','fecha','estatus', 'created_at', 'updated_at'];
}