<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosLGC extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'datoslgc';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre','apellidopat','apellidomat','celular','telefonofijo','iduser', 'estatus', 'created_at', 'updated_at'];
}