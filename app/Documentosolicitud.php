<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentosolicitud extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documentosolicitudes';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre','idsolicitud', 'nombreruta', 'created_at', 'updated_at'];
}