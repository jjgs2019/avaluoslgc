<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bienevaluar extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bienesevaluar';

    /**
     * @var array
     */
    protected $fillable = ['id', 'idsolicitud','idtipoavaluo','idtipobien','dato', 'datoadicional','tipodato','tipodatoadicional','bienobservaciones','idrevisor','avaluoobservaciones','nombreruta','aprobacionbservaciones', 'idusercotizante', 'foliointerno','idevaluador','importe','valorbien','idestado','idmunicipio','cp','colonia','calle','numero','estatus','nombrerutaevidencia','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoavaluo()
    {
        return $this->belongsTo('App\Tipoavaluo', 'idtipoavaluo', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipobien()
    {
        return $this->belongsTo('App\Tipobien', 'idtipobien', 'id');
    }
}