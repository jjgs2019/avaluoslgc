<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'municipios';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre', 'idestado', 'estatus', 'created_at', 'updated_at'];
}