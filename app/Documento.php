<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documentos';

    /**
     * @var array
     */
    protected $fillable = ['id', 'idtipodocumento', 'descripcion','fecha','estatus','iduser', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipodocumento()
    {
        return $this->belongsTo('App\Tipodocumento', 'idtipodocumento', 'id');
    }
}