<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tituloprofesional extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tituloprofesionales';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre', 'idnivelestudio', 'estatus', 'created_at', 'updated_at'];

    public function nivelestudio()
    {
        return $this->belongsTo('App\Nivelestudio', 'idnivelestudio', 'id');
    }
}