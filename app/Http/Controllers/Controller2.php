<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use DateTime;
use DateInterval;
use App\Permiso;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Permite retornar las variables globales que se usan en todo el sistema
     * @param type $iduser
     * @param type $controlador
     * @return type $arraybtn
     */
    public function variablesglobales($nombre_variable){
        $data=null;
        if($nombre_variable=='estatus_prestamos'){
            $data= array(
                'A' => 'Activa',
                'S' => 'Autorizada',
                'E' => 'Depositada',
                'P' => 'Pagado',
                'R' => 'Rechazada',
                'D' => 'Eliminada',
            );
        }
        if($nombre_variable=='estatus_amortizacionesprestamos'){
            $data= array(
                'E' => 'Por pagar',
                'P' => 'Pagado',
                'N' => 'No pagado',
                'R' => 'Restructurado',
                'C' => 'Cancelado',
                'M' => 'Pago por parte',
            );
        }
        if($nombre_variable=='estatus_etapas'){
            $data= array(
                'A' => 'Activa',
                'D' => 'Cancelada',
                'C' => 'Cerrada',
            );
        }
        if($nombre_variable=='tipo_movimientos'){
            $data= array(
                'A' => 'Ahorros',
                'P' => 'Préstamos',
            );
         }
        if($nombre_variable=='semana_institucion'){
            $data= array(
                '1' => 'Lunes',
                '2' => 'Martes',
                '3' => 'Miércoles',
                '4' => 'Jueves',
                '5' => 'Viernes',
                '6' => 'Sábado',
                '7' => 'Domingo',
            );
        }
        if($nombre_variable=='anios_reportes'){
            $data= array(
                '2019' => '2019',
                '2020' => '2020',
            );
        }
        if($nombre_variable=='estatus_cobranzas'){
            $data= array(
                '1' => 'Realizada',
                '2' => 'Generado archivo para banco',
                '3' => 'Finalizada',
            );
        }
        if($nombre_variable=='estatus_detallescobranzas'){
            $data= array(
                '-1' => 'Rechazado',
                '0' => 'Por ejecutar',
                '1' => 'Ejecutado',
            );
        }
        if($nombre_variable=='estatus_ahorros'){
            $data= array(
                'A' => 'Activa',
                'P' => 'Pagado',
                'M' => 'Pago por parte',
            );
        }
        return $data;
    }

    /**
     * Permite verificar si puede ejecutar acciones del controlador y mostrar los botones de acciones en el index del controlador
     * @param type $iduser
     * @param type $controlador
     * @return type $arraybtn
     */
    public function btnfunciones($iduser,$controlador){
        //BUSCAR PERMISOS LIGADOS AL CONTROLADOR DEL USUARIO
        $objPermiso = new Permiso;
        $datas = $objPermiso->VerificarPermisosDelControlador($iduser,$controlador);
        $arraybtn = array();
        if(count($datas)>0){
            foreach ($datas as $data) {
                //TODOS LOS CONTROLADORES
	                if($data->accion=='create'){
	                    $arraybtn['create']=true;
	                }
	                if($data->accion=='editar'){
                        $arraybtn['editar']=true;
	                }
	                if($data->accion=='eliminar'){
	                    $arraybtn['eliminar']=true;
	                }
					if($data->accion=='mostrar'){
						$arraybtn['mostrar']=true;
					}

                if($controlador=='UsersController')
                {
                    if($data->accion=='cambiarpassword'){
                        $arraybtn['cambiarpassword']=true;
                    }
                    if($data->accion=='permisos'){
                        $arraybtn['permisos']=true;
                    }
                    if($data->accion=='permizonas'){
                        $arraybtn['permizonas']=true;
                    }
                }

            }
        }
        return $arraybtn;
    }

    /**
     * Permite actualizar la configuración de la sessión actual
     * @param type $tipo_configuracion
     * @return type
     */
    public function ActualizarSesionConfiguracion($tipo_configuracion=1)
    {
        //BUSCANDO LAS CONFIGURACIONES DEL SITIO WEB
            $ConfiguracionWebs = Configuracionweb::where('tipo', '=', $tipo_configuracion)->get();
            foreach ($ConfiguracionWebs as $ConfiguracionWeb) {
                if($ConfiguracionWeb->tipo==$tipo_configuracion){
                    $nombre = $ConfiguracionWeb->nombre;
                    $parametros = $ConfiguracionWeb->parametros;

                    //session()->forget('Configuracion.Web.'.$nombre);
                    session()->put('Configuracion.Web.'.$nombre, $parametros);
                }
            }
        return true;
    }

    /**
     * Permite crear un slug
     * @param type $text
     * @return type $text
     */
    public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        //lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return '';
        }

        return $text;
    }

     /**
     * Permite formatear un campo para convertir en decimal
     * @param  string $valor $paramname description
     * @return decimal $valor
     */
    public function formatearADecimal($valor){
        $valor = str_replace(",", "", (string)$valor);
        // $valor = str_replace(".", ",", (string)$valor);

        return $valor;
    }

    /**
     * Permite convertir el formato money de un excel a un decimal para el campo en la tabla
     * @param  [money] $numero
     * @return [decimal] $numero
     */
    public function converExcelMoneyADecimal($numero){
        $numero = str_replace("$", "", $numero);
        $numero = str_replace(",", "", $numero);
        $numero = (float)trim($numero);

        return $numero;
    }

    /**
     * Obtener el formato del archivo
     * @param  text $file
     */
    public function typeFile($file){
        $array = explode(".", $file);
        return end( $array );
    }
}
