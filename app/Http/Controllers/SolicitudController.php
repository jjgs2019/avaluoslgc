<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Route;


use App\User;
use App\Modulo;
use App\Permiso;
use App\Solicitud;
use App\Estado;
use App\Tipopersona;
use App\Tipoavaluo;
use App\Tipobien;
use App\Bienevaluar;
use App\Notificacion;
use App\Detallesolicitud;
use App\Municipio;
use App\Documentosolicitud;
use App\Configuracion;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//CORREOS
use App\Mail\SolicitudCreada;
use App\Mail\SolicitudCotizada;
use App\Mail\SolicitudAutorizada;
use App\Mail\SolicitudAsignada;
use App\Mail\SolicitudValuada;
use App\Mail\SolicitudAprobada;
use App\Mail\SolicitudCancelada;
use Mail;

class SolicitudController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->nombremodelo='Solicitud';
        // $this->middleware('auth');
       
    }

    protected function inicializaColeccion($modelo)
    {

         $NombreCompletoCotizante=DB::raw('CONCAT(datoslgc.nombre, " ", datoslgc.apellidopat) AS usuariocotizante');
         $NombreCompletoSolicitante=DB::raw('CONCAT(lgcsolicitante.nombre, " ", lgcsolicitante.apellidopat) AS usuariosolicitantelgc');
         $NombreCompletoValuador=DB::raw('CONCAT(lgcvaluador.nombre, " ", lgcvaluador.apellidopat) AS valuador');
         $NombreCompletoRevisor=DB::raw('CONCAT(lgcrevisor.nombre, " ", lgcrevisor.apellidopat) AS revisor');

        if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' ){

            $BienesSolicitud = DB::table('bienesevaluar')
            ->select('bienesevaluar.nombreruta as nombreruta','bienesevaluar.id as idbienevaluar','solicitudes.id as idsolicitud','solicitudes.fecha as fechasolicitud','solicitudes.folioexterior as foliosolicitud','solicitudes.nombre as contribuyente','tipobienes.nombre as bien','bienesevaluar.estatus as estatusbien','solicitudes.idusersolicitante as solicitante','bienesevaluar.idusercotizante as cotizante','bienesevaluar.foliointerno as foliobien','oficinassat.nombre as oficina',$NombreCompletoCotizante,'datossat.nombre as usuariosolicitantesat',$NombreCompletoSolicitante,$NombreCompletoValuador,$NombreCompletoRevisor)     
            ->leftJoin('solicitudes', 'solicitudes.id', '=', 'bienesevaluar.idsolicitud')
            ->leftJoin('tipobienes', 'tipobienes.id', '=', 'bienesevaluar.idtipobien')
            ->leftJoin('datossat', 'datossat.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'bienesevaluar.idusercotizante')
            ->leftJoin('datoslgc as lgcsolicitante', 'lgcsolicitante.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc as lgcvaluador', 'lgcvaluador.iduser', '=', 'bienesevaluar.idevaluador')
            ->leftJoin('datoslgc as lgcrevisor', 'lgcrevisor.iduser', '=', 'bienesevaluar.idrevisor')
            ->leftJoin('oficinassat', 'oficinassat.id', '=', 'datossat.idoficinasat')
            ->where("bienesevaluar.estatus", "<>", 'E');

        }
        
        if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT' ){

            
            $BienesSolicitud = DB::table('bienesevaluar')
            ->select('bienesevaluar.nombreruta as nombreruta','bienesevaluar.id as idbienevaluar','solicitudes.id as idsolicitud','solicitudes.fecha as fechasolicitud','solicitudes.folioexterior as foliosolicitud','solicitudes.nombre as contribuyente','tipobienes.nombre as bien','bienesevaluar.estatus as estatusbien','solicitudes.idusersolicitante as solicitante','bienesevaluar.idusercotizante as cotizante','bienesevaluar.foliointerno as foliobien','oficinassat.nombre as oficina',$NombreCompletoCotizante,'datossat.nombre as usuariosolicitantesat',$NombreCompletoSolicitante,$NombreCompletoValuador,$NombreCompletoRevisor)      
            ->leftJoin('solicitudes', 'solicitudes.id', '=', 'bienesevaluar.idsolicitud')
            ->leftJoin('tipobienes', 'tipobienes.id', '=', 'bienesevaluar.idtipobien')
            ->leftJoin('datossat', 'datossat.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'bienesevaluar.idusercotizante')
            ->leftJoin('datoslgc as lgcsolicitante', 'lgcsolicitante.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc as lgcvaluador', 'lgcvaluador.iduser', '=', 'bienesevaluar.idevaluador')
            ->leftJoin('datoslgc as lgcrevisor', 'lgcrevisor.iduser', '=', 'bienesevaluar.idrevisor')
            ->leftJoin('oficinassat', 'oficinassat.id', '=', 'datossat.idoficinasat')
            ->where("solicitudes.idusersolicitante", "=", Auth::user()->id)
            ->where("bienesevaluar.estatus", "<>", 'E');
            
        }

        if( Auth::user()->tipousuario['nombre'] == 'Valuador'){

            $SolicitudEvaluador=Bienevaluar::select("idsolicitud")->where("idevaluador", "=", Auth::user()->id)
            ->groupBy("idsolicitud")
            ->get();

            $BienesSolicitud = DB::table('bienesevaluar')
            ->select('bienesevaluar.nombreruta as nombreruta','bienesevaluar.id as idbienevaluar','solicitudes.id as idsolicitud','solicitudes.fecha as fechasolicitud','solicitudes.folioexterior as foliosolicitud','solicitudes.nombre as contribuyente','tipobienes.nombre as bien','bienesevaluar.estatus as estatusbien','solicitudes.idusersolicitante as solicitante','bienesevaluar.idusercotizante as cotizante','bienesevaluar.foliointerno as foliobien','oficinassat.nombre as oficina',$NombreCompletoCotizante,'datossat.nombre as usuariosolicitantesat',$NombreCompletoSolicitante,$NombreCompletoValuador,$NombreCompletoRevisor)      
            ->leftJoin('solicitudes', 'solicitudes.id', '=', 'bienesevaluar.idsolicitud')
            ->leftJoin('tipobienes', 'tipobienes.id', '=', 'bienesevaluar.idtipobien')
            ->leftJoin('datossat', 'datossat.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'bienesevaluar.idusercotizante')
            ->leftJoin('datoslgc as lgcsolicitante', 'lgcsolicitante.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc as lgcvaluador', 'lgcvaluador.iduser', '=', 'bienesevaluar.idevaluador')
            ->leftJoin('datoslgc as lgcrevisor', 'lgcrevisor.iduser', '=', 'bienesevaluar.idrevisor')
            ->leftJoin('oficinassat', 'oficinassat.id', '=', 'datossat.idoficinasat')
            ->whereIn("solicitudes.id", $SolicitudEvaluador)
            ->where("bienesevaluar.idevaluador", "=", Auth::user()->id)
            ->where("bienesevaluar.estatus", "<>", 'E');
            
        }
        
        if( Auth::user()->tipousuario['nombre'] == 'Revisor'){

            $SolicitudRevisor=Bienevaluar::select("idsolicitud")->where("idrevisor", "=", Auth::user()->id)
            ->groupBy("idsolicitud")    
            ->get();

            $BienesSolicitud = DB::table('bienesevaluar')
            ->select('bienesevaluar.nombreruta as nombreruta','bienesevaluar.id as idbienevaluar','solicitudes.id as idsolicitud','solicitudes.fecha as fechasolicitud','solicitudes.folioexterior as foliosolicitud','solicitudes.nombre as contribuyente','tipobienes.nombre as bien','bienesevaluar.estatus as estatusbien','solicitudes.idusersolicitante as solicitante','bienesevaluar.idusercotizante as cotizante','bienesevaluar.foliointerno as foliobien','oficinassat.nombre as oficina',$NombreCompletoCotizante,'datossat.nombre as usuariosolicitantesat',$NombreCompletoSolicitante,$NombreCompletoValuador,$NombreCompletoRevisor)      
            ->leftJoin('solicitudes', 'solicitudes.id', '=', 'bienesevaluar.idsolicitud')
            ->leftJoin('tipobienes', 'tipobienes.id', '=', 'bienesevaluar.idtipobien')
            ->leftJoin('datossat', 'datossat.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'bienesevaluar.idusercotizante')
            ->leftJoin('datoslgc as lgcsolicitante', 'lgcsolicitante.iduser', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datoslgc as lgcvaluador', 'lgcvaluador.iduser', '=', 'bienesevaluar.idevaluador')
            ->leftJoin('datoslgc as lgcrevisor', 'lgcrevisor.iduser', '=', 'bienesevaluar.idrevisor')
            ->leftJoin('oficinassat', 'oficinassat.id', '=', 'datossat.idoficinasat')
            ->whereIn("solicitudes.id", $SolicitudRevisor)
            ->where("bienesevaluar.idrevisor", "=", Auth::user()->id)
            ->where("bienesevaluar.estatus", "<>", 'E');

        }
       
        return $BienesSolicitud;
          
    }

    protected function inicializaPlucks()
    {

        //VERIFICAR SI TIENE POSIBILIDAD DE MOSTRAR, ELIMININAR O EDITAR
        $permisos=$this->btnfunciones(auth()->user()->id,'SolicitudController');
        parent::addPluck('permisos',$permisos);   

        $Estado = Estado::select('nombre','id')->where("estatus", "=", 'A')->groupBy("nombre","id")->pluck('nombre','id');
        parent::addPluck('Estado',$Estado); 

        $Tipopersona = Tipopersona::select('nombre','id')->where("estatus", "=", 'A')->groupBy("nombre","id")->pluck('nombre','id');
        parent::addPluck('Tipopersona',$Tipopersona);

        $Tipoavaluo = Tipoavaluo::select('nombre','id')->where("estatus", "=", 'A')->groupBy("nombre","id")->pluck('nombre','id');
        parent::addPluck('Tipoavaluo',$Tipoavaluo); 

        $CantidadNotificacion=Notificacion::where("idusuario", "=", Auth::user()->id) 
            ->where("estatus", "=", 'A') 
            ->count();
        parent::addPluck('CantidadNotificacion',$CantidadNotificacion); 

        /*$Notificaciones=Notificacion::where("idusuario", "=", Auth::user()->id) 
            ->where("estatus", "=", 'A') 
            ->get();
        parent::addPluck('Notificaciones',$Notificaciones);*/

        $Notificaciones = DB::table('notificaciones')
            ->select('notificaciones.*','solicitudes.folioexterior as folioexterior','bienesevaluar.foliointerno as foliointerno')     
            ->leftJoin('solicitudes', 'solicitudes.id', '=', 'notificaciones.idsolicitud')
            ->leftJoin('bienesevaluar', 'bienesevaluar.id', '=', 'notificaciones.idtipobien')
            ->where("idusuario", "=", Auth::user()->id) 
            ->where("notificaciones.estatus", "=", 'A')
            ->orderBy('notificaciones.id', 'desc')
            ->get();
        parent::addPluck('Notificaciones',$Notificaciones);

        $Usuario = DB::table('users')
            ->select('datossat.nombre as nombresat','datoslgc.nombre as nombrelgc','datoslgc.apellidopat as apellidopat','datoslgc.apellidomat as apellidomat')     
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'users.id')
            ->leftJoin('datossat', 'datossat.iduser', '=', 'users.id')
            ->where("users.id", "=", Auth::user()->id)
            ->first();

        parent::addPluck('Usuario',$Usuario);
      
    }

    public function visualizardocumentossolicitud($archivo){

        return Storage::disk('public')->response("documentossolicitud/$archivo");

    }

    public function descargardocumentossolicitud($archivo){

        return Storage::disk('public')->download("documentossolicitud/$archivo");

    }


    public function visualizardocumentosevidencia($archivo){

        return Storage::disk('public')->response("documentoevidencia/$archivo");

    }

    public function descargardocumentosevidencia($archivo){

        return Storage::disk('public')->download("documentoevidencia/$archivo");

    }

    public function buscartipodebien(Request $request)
    {
        $Tipobien = Tipobien::select('*')
            ->where('idtipoavaluo', '=', $request->id)
            ->get();

        echo json_encode($Tipobien); 
    }


    public function buscarmunicipio(Request $request)
    {
        $Municipios = Municipio::select('*')
            ->where('idestado', '=', $request->id)
            ->get();

        echo json_encode($Municipios); 
    }


    public function descargarPDF($archivo){

        return Storage::disk('public')->download("documentopdf/$archivo");

        /*$path = storage_path('documentopdf/'.$archivo);

return response()->download($path);*/

//return Storage::download("documentopdf/$archivo");

        /*$pathtoFile = public_path().'//storage/documentopdf/'.$archivo;
        //dd($pathtoFile);
      return response()->download($pathtoFile);

        /*$public_path = public_path();
     $url = $public_path.'/storage/documentopdf/'.$archivo;
     //dd($url);

     return response()->download($url);
     //verificamos si el archivo existe y lo retornamos
     if (Storage::exists($archivo))
     {
       return response()->download($url);
     }
     //si no se encuentra lanzamos un error 404.
     abort(404);*/
    }

    public function afterUpdate($solicitud, $lrequest, Request $request, $idalterno=0){

        if(!isset($lrequest['_cancelar']) && !isset($lrequest['_asignar']) && !isset($lrequest['_valuar']) && !isset($lrequest['_cotizar']) && !isset($lrequest['_aprobar']) && !isset($lrequest['_autorizar']) && !isset($lrequest['_editar'])){

            $deletebienevaluar=Bienevaluar::where("idsolicitud", "=", $solicitud->id)->delete();

        }        

        $continuar=true;

        if(isset($lrequest['_bienevaluar'])){

            $detallesmodel=json_decode($lrequest['_bienevaluar'], true);
            $Encontro=false;
            $EncontroBien=false;

            $ModeloActual;
            $ContadorDocumentos=0;
            $ContadorBien=0;

            foreach ($detallesmodel as $key => $value) {
                $Encontro=false;
                foreach ($value as $key_model => $value_model) {
                    if($key_model=='Model'){
                        $objmodelo_detalles=$this->getModelo('App\\'.$value_model);
                        $objdetalle=$objmodelo_detalles;
                        $ModeloActual=$value_model;
                    }else{

                        if(isset($value['id']) && !$Encontro){
                            $objdetalle=$objmodelo_detalles::findOrFail($value['id']);
                            $Encontro=true;
                            $EncontroBien=true;
                                                        
                        }
                        else
                        {
                            if($key_model!='id'){
                                $objdetalle->$key_model=$value_model;
                            }
                            
                        }                   
                    }
                }

                $objdetalle->idsolicitud=$solicitud->id;

                if($ModeloActual == 'Bienevaluar'){

                    //VERIFICAMOS SI TRAJO ARCHIVO
                    if($request->hasFile('documentoevidencia')){

                        $Control=$request->file('documentoevidencia');

                        if(isset($Control[$ContadorBien])){

                            $sitioguardar='public/documentoevidencia';
                            $documentoevidencia = $request->file('documentoevidencia');
                            $nombreoriginal=$documentoevidencia[$ContadorBien]->getClientOriginalName();
                            $tipo=$this->typeFile($nombreoriginal);

                            $nombre_archivo = $documentoevidencia[$ContadorBien]->storeAs($sitioguardar,$objdetalle->id.'-'.uniqid().'.'.$tipo);
                            $nombre_archivo = str_replace("public/documentoevidencia/", "", $nombre_archivo);

                            $objdetalle->nombrerutaevidencia = $nombre_archivo;

                        }    
      
                    }

                    $ContadorBien++;

                }

                if($ModeloActual == 'Documentosolicitud'){

                    //VERIFICAMOS SI TRAJO ARCHIVO
                    if($request->hasFile('documentossolicitud')){
                        $sitioguardar='public/documentossolicitud';
                        $documentossolicitud = $request->file('documentossolicitud');
                        $nombreoriginal=$documentossolicitud[$ContadorDocumentos]->getClientOriginalName();
                        $tipo=$this->typeFile($nombreoriginal);

                        $nombre_archivo = $documentossolicitud[$ContadorDocumentos]->storeAs($sitioguardar,$objdetalle->id.'-'.uniqid().'.'.$tipo);
                        $nombre_archivo = str_replace("public/documentossolicitud/", "", $nombre_archivo);

                        $objdetalle->nombreruta = $nombre_archivo;

                        $ContadorDocumentos++;
      
                    }

                }


                if(isset($lrequest['_asignar'])){

                    if(!is_null($objdetalle->idrevisor)){

                        $objdetalle->estatus='S';
                    }
                    

                }

                if(isset($lrequest['_cotizar'])){

                    $objdetalle->estatus='O';

                }

                if(isset($lrequest['_valuar'])){

                    $objdetalle->estatus='V'; 

                    //VERIFICAMOS SI TRAJO ARCHIVO
                    if($request->hasFile('documentopdf')){
                        $sitioguardar='public/documentopdf';
                        $documentopdf = $request->file('documentopdf');
                        $nombreoriginal=$documentopdf->getClientOriginalName();
                        $tipo=$this->typeFile($nombreoriginal);

                        $nombre_archivo = $documentopdf->storeAs($sitioguardar,$objdetalle->id.'-'.uniqid().'.'.$tipo);
                        $nombre_archivo = str_replace("public/documentopdf/", "", $nombre_archivo);

                        $objdetalle->nombreruta = $nombre_archivo;
      
                    }


                }

                if(isset($lrequest['_aprobar'])){

                    $objdetalle->estatus='R'; 

                }

                if(isset($lrequest['_autorizar'])){

                    $Bienevaluarcanti=Bienevaluar::where('foliointerno', 'like', date('Y').'-%')->count();
                    
                    $Bienevaluarcanti++;
                    
                    if($Bienevaluarcanti<9){

                        $Bienevaluarcanti='0'.$Bienevaluarcanti;

                    }

                    $objdetalle->estatus='U';   
                    $objdetalle->foliointerno=date('Y').'-'.$Bienevaluarcanti;             

                }

                if(isset($lrequest['_cancelar'])){

                    $objdetalle->estatus='C';

                }

                if(!$objdetalle->save()){
                    $continuar=false;
                    $mensaje='No se pudo guardar detalles';
                    break;
                }
                else
                {

                    if(isset($lrequest['_valuar'])){

                        $configuracion = Configuracion::where('estatus','=','A')->first();

                        $contrasenia = $configuracion->pass;

                        $cer = storage_path('app/public/certificados/').$configuracion->cer;
                        $key = storage_path('app/public/certificados/').$configuracion->key;

                        $rutapdf = storage_path('app/public/documentopdf/').$objdetalle->nombreruta;

                        //storage_path('app/public/certificados/');

                        //$cer = base_path().'/public/certificados/CSD01_AAA010101AAA.cer';
                        //$key = base_path().'/public/certificados/CSD01_AAA010101AAA.key';

                        $ruta=storage_path('app/public/certificados/');

                        $nomfile=$ruta.'certificate';

                        $strshell='openssl x509 -inform DER -in '.$cer.' -out '.$nomfile.'.cer.crt';

                        $strshell2='openssl pkcs8 -inform DER -passin pass:"'.$contrasenia.'" -in "'.$key.'" -out '.$nomfile.'.key.crt';

                        $output2 = shell_exec($strshell2);
                        $output = shell_exec($strshell);

                        $tamano=filesize($nomfile.".key.crt");

                        if($tamano>0){

                            $certificate = 'file://'.storage_path('app/public/certificados/certificate.cer.crt');
                            $privkey = 'file://'.storage_path('app/public/certificados/certificate.key.crt');

                            //$rutapdf=storage_path('app/public/documentopdf/PLANILLATARJETADECREDITO.pdf');

                            $pdf = new \setasign\Fpdi\Tcpdf\Fpdi();
                            //$pdf = new \setasign\fpdi\src\tcpdf\Fpdi();
                            $pages = $pdf->setSourceFile($rutapdf);

                            $info = array(
                                'Name' => 'LGC Avalúos',
                                'Location' => 'LGC',
                                //'privkey' => $privkey,
                                //'password' => '12345678a',
                                'Reason' => 'Avalúo LGC',
                                'ContactInfo' => 'https://www.lgcavaluos.com/',
                                );

                            for ($i = 1; $i <= $pages; $i++) {
                                    $pdf->AddPage();
                                    $page = $pdf->importPage($i);
                                    $pdf->useTemplate($page, 0, 0);
                                    // set document signature
                                    $pdf->setSignature($certificate, $privkey, $contrasenia, '', 2, $info);      
                            }

                            $rutapdf=storage_path('app/public/documentopdf/Certificado_'.$objdetalle->nombreruta);
                            $pdf->Output($rutapdf,"F");
                            $pdfnosellado=$objdetalle->nombreruta;
                            $objdetalle->nombreruta='Certificado_'.$objdetalle->nombreruta;

                            $objdetalle->save();

                            return Storage::disk('public')->delete("documentopdf/".$pdfnosellado);

                            //Storage::disk('s3')->delete('folder_path/file_name.jpg');

                        }

                    }

                }
            }

            //faltan las notificaciones y detalles trabajar para que venga con el id
            if(isset($lrequest['_editar']) && !$EncontroBien){

                $deletebienevaluar=Bienevaluar::where("id", "=", $idalterno)->delete();

            }

        }

        return $continuar;

    }

    protected function complementoColeccion($id,$idalterno)
    {

        $Detallesolicitud = DB::table('detallessolicitudes')
            ->where("detallessolicitudes.idsolicitud", "=",$id)
            ->where("detallessolicitudes.idtipobien", "=",$idalterno)
            ->get();

        parent::addPluck('Detallesolicitud',$Detallesolicitud);

        $Bienevaluar = Bienevaluar::where("id", "=",$idalterno)->get();
        parent::addPluck('Bienevaluar',$Bienevaluar);

        $DocumentoSolicitud = Documentosolicitud::where("idsolicitud", "=",$id)->get();

        if(!$DocumentoSolicitud->isEmpty()){

            parent::addPluck('DocumentoSolicitud',$DocumentoSolicitud);

        }
        
        $UsuarioSAT = DB::table('solicitudes')
            ->select('datossat.cargo as cargo','oficinassat.nombre as oficina','datossat.nombre as usuariosat','tipousuarios.nombre as tipousuario')     
            ->leftJoin('users', 'users.id', '=', 'solicitudes.idusersolicitante')
            ->leftJoin('datossat', 'datossat.iduser', '=', 'users.id')
            ->leftJoin('oficinassat', 'oficinassat.id', '=', 'datossat.idoficinasat')
            ->leftJoin('tipousuarios', 'tipousuarios.id', '=', 'users.idtipousuario')
            ->where("solicitudes.id", "=",$id)
            ->first();

        parent::addPluck('UsuarioSAT',$UsuarioSAT);

        $NombreCompleto=DB::raw('CONCAT(datoslgc.nombre, " ", datoslgc.apellidopat) AS nombre');

        $Revisor = DB::table('users')
            ->select($NombreCompleto,'users.id')
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'users.id')
            ->leftJoin('tipousuarios', 'tipousuarios.id', '=', 'users.idtipousuario')
            ->where("tipousuarios.nombre", "=",'Revisor')
            ->where("users.estatus", "=",'A')
            ->pluck('nombre','id');

        parent::addPluck('Revisor',$Revisor);

        $Valuador = DB::table('users')
            ->select($NombreCompleto,'users.id')
            ->leftJoin('datoslgc', 'datoslgc.iduser', '=', 'users.id')
            ->leftJoin('tipousuarios', 'tipousuarios.id', '=', 'users.idtipousuario')
            ->where("tipousuarios.nombre", "=",'Valuador')
            ->where("users.estatus", "=",'A')
            ->pluck('nombre','id');
            
        parent::addPluck('Valuador',$Valuador);

    }

    public function notificacion($lrequest, $nuevoRegistro, $obj, $idalterno){

        if($nuevoRegistro){

            $Bien=Bienevaluar::where("idsolicitud", "=", $obj->id)
            ->first();

            $Detalle= new Detallesolicitud;
            $Detalle->fecha= date('d-m-Y');
            $Detalle->idusuario= $obj->idusersolicitante;
            $Detalle->estatus= 'Creada';
            $Detalle->idsolicitud= $obj->id;
            $Detalle->idtipobien= $idalterno;
            $Detalle->save();

            $Users = User::where("estatus", "=", 'A')->get();
            $Enviar=true;

            foreach($Users as $user){
                
                $Enviar=true;

                if($user->tipousuario['nombre'] == 'Administrador LGC'){

                    if(Auth::user()->tipousuario["nombre"] == 'Administrador LGC' && $user->id == Auth::user()->id ){

                        $Enviar=false;

                    }

                    if($Enviar){

                        $Notificacion= new Notificacion;
                        $Notificacion->fecha= date('d-m-Y');
                        $Notificacion->nombre= 'Nueva Solicitud de Avalúo';
                        $Notificacion->estatus= 'A';
                        $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
                        $Notificacion->idusuario= $user->id;
                        $Notificacion->idsolicitud= $obj->id;
                        $Notificacion->idtipobien= $Bien->id;
                        $Notificacion->save();

                        $data = array('subject'=>'Solicitud de Avalúo Creada');
                        Mail::to($user->email)->send(new SolicitudCreada($data));

                    }
                }
            }   
        }
        else{

            $Bienautorizar = Bienevaluar::findOrFail($idalterno);

            if($Bienautorizar->estatus == 'R'|| $Bienautorizar->estatus == 'C'){
                //BORRANDO LA NOTIFICACION ANTERIOR
                $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
                ->where("idusuario", "=", Auth::user()->id)
                ->where("idtipobien", "=", $Bienautorizar->id)
                ->update(['estatus' => 'V']);
            }

        }

        if(isset($lrequest['_cancelar'])){

            $Biencancelar = Bienevaluar::findOrFail($idalterno);

            //BORRANDO LA NOTIFICACION ANTERIOR
            $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("idtipobien", "=", $Biencancelar->id)
            ->update(['estatus' => 'V']);

            //NUEVA NOTIFICACION
            $Notificacion= new Notificacion;
            $Notificacion->fecha= date('d-m-Y');
            $Notificacion->nombre= 'Solicitud de Avalúo Cancelada';
            $Notificacion->estatus= 'A';
            $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
            $Notificacion->idusuario= $Biencancelar->idusercotizante;
            $Notificacion->idsolicitud= $obj->id;
            $Notificacion->idtipobien= $idalterno;
            $Notificacion->save();            

            $Detalle= new Detallesolicitud;
            $Detalle->fecha= date('d-m-Y');
            $Detalle->idusuario= $obj->idusercotizante;
            $Detalle->estatus= 'Cancelada';
            $Detalle->idsolicitud= $obj->id;
            $Detalle->idtipobien= $idalterno;
            $Detalle->save();

            $Users = User::findOrFail($Biencancelar->idusercotizante);
            $data = array('subject'=>'Solicitud de Avalúo Cancelada');
            Mail::to($Users->email)->send(new SolicitudCancelada($data));

        }

        if(isset($lrequest['_cotizar'])){

            $Users = User::findOrFail($obj->idusersolicitante);
 
            //VER SI LA NOTIFICACION YA EXISTE
            $ExisteNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("idtipobien", "=", $idalterno )
            ->where("nombre", "=", 'Solicitud de Avalúo Cotizada')
            ->where("estatus", "=", 'A')
            ->count();

            //dd($ExisteNotificacion);
            
            if($ExisteNotificacion == 0){

                //NUEVA NOTIFICACION
                $Notificacion= new Notificacion;
                $Notificacion->fecha= date('d-m-Y');
                $Notificacion->nombre= 'Solicitud de Avalúo Cotizada';
                $Notificacion->estatus= 'A';
                $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
                $Notificacion->idusuario= $obj->idusersolicitante;
                $Notificacion->idsolicitud= $obj->id;
                $Notificacion->idtipobien= $idalterno;
                $Notificacion->save();

                //BORRANDO LA NOTIFICACION ANTERIOR
                $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
                ->where("nombre", "=", 'Nueva Solicitud de Avalúo')
                ->update(['estatus' => 'V']);

                $Detalle= new Detallesolicitud;
                $Detalle->fecha= date('d-m-Y');
                $Detalle->idusuario= $obj->idusercotizante;
                $Detalle->estatus= 'Cotizada';
                $Detalle->idsolicitud= $obj->id;
                $Detalle->idtipobien= $idalterno;
                $Detalle->save();

                $data = array('subject'=>'Solicitud de Avalúo Cotizada');
                Mail::to($Users->email)->send(new SolicitudCotizada($data));

            }
            else
            {

                $data = array('subject'=>'Cambios en la cotización de la solicitud de Avalúo');
                Mail::to($Users->email)->send(new SolicitudCotizada($data));
               
            }

        }

        if(isset($lrequest['_autorizar'])){

            $Bienautorizar = Bienevaluar::findOrFail($idalterno);
            $Users = User::findOrFail($Bienautorizar->idusercotizante);      

            $Notificacion= new Notificacion;
            $Notificacion->fecha= date('d-m-Y');
            $Notificacion->nombre= 'Solicitud de Avalúo Autorizada';
            $Notificacion->estatus= 'A';
            $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
            $Notificacion->idusuario= $obj->idusercotizante;
            $Notificacion->idsolicitud= $obj->id;
            $Notificacion->idtipobien= $idalterno;
            $Notificacion->save();

            //BORRANDO LA NOTIFICACION ANTERIOR
            $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("nombre", "=", 'Solicitud de Avalúo Cotizada')
            ->where("idtipobien", "=", $Bienautorizar->id)
            ->update(['estatus' => 'V']);

            $Detalle= new Detallesolicitud;
            $Detalle->fecha= date('d-m-Y');
            $Detalle->idusuario= $obj->idusersolicitante;
            $Detalle->estatus= 'Autorizada';
            $Detalle->idsolicitud= $obj->id;
            $Detalle->idtipobien= $idalterno;
            $Detalle->save();

            $data = array('subject'=>'Solicitud de Avalúo Autorizada');
            Mail::to($Users->email)->send(new SolicitudAutorizada($data));

        }

        if(isset($lrequest['_asignar'])){

            $detallesmodel=json_decode($lrequest['_bienevaluar'], true);
            $Bienautorizar = Bienevaluar::findOrFail($idalterno);

            $arrayRevisor= array();
            $arrayValuador= array();

            foreach($detallesmodel as $key => $value){

                array_push($arrayRevisor, $value['idrevisor']);
                array_push($arrayValuador, $value['idevaluador']);

            }

            $ListaRevisor = array_values(array_unique($arrayRevisor));
            $ListaValuador = array_values(array_unique($arrayValuador));

            foreach($ListaRevisor as $value){

                $Notificacion= new Notificacion;
                $Notificacion->fecha= date('d-m-Y');
                $Notificacion->nombre= 'Usted fue asignado como Revisor';
                $Notificacion->estatus= 'A';
                $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
                $Notificacion->idusuario= $value;
                $Notificacion->idsolicitud= $obj->id;
                $Notificacion->idtipobien= $idalterno;
                $Notificacion->save();

                $DataUserRevisor = array('tipo'=>'Revisor','subject'=>'Haz sido asignado a un Avalúo');
                $UserRevisor = User::findOrFail($value);
                Mail::to($UserRevisor->email)->send(new SolicitudAsignada($DataUserRevisor));

            }

            foreach($ListaValuador as $value){
                              
                $Notificacion= new Notificacion;
                $Notificacion->fecha= date('d-m-Y');
                $Notificacion->nombre= 'Usted fue asignado como Valuador';
                $Notificacion->estatus= 'A';
                $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
                $Notificacion->idusuario= $value;
                $Notificacion->idsolicitud= $obj->id;
                $Notificacion->idtipobien= $idalterno;
                $Notificacion->save();

                $DataUserValuador = array('tipo'=>'Valuador','subject'=>'Haz sido asignado a un Avalúo');
                $UserValuador = User::findOrFail($value);
                Mail::to($UserValuador->email)->send(new SolicitudAsignada($DataUserValuador));

            }

            //BORRANDO LA NOTIFICACION ANTERIOR
            $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("nombre", "=", 'Solicitud de Avalúo Autorizada')
            ->where("idtipobien", "=", $Bienautorizar->id)
            ->update(['estatus' => 'V']);

            $Detalle= new Detallesolicitud;
            $Detalle->fecha= date('d-m-Y');
            $Detalle->idusuario= $obj->idusercotizante;
            $Detalle->estatus= 'Asignada';
            $Detalle->idsolicitud= $obj->id;
            $Detalle->idtipobien= $idalterno;
            $Detalle->save();

        }

        if(isset($lrequest['_valuar'])){

            $detallesmodel=json_decode($lrequest['_bienevaluar'], true);
            $Bienautorizar = Bienevaluar::findOrFail($idalterno);

            $arrayRevisor= array();
            $IdEvaluador=0;

            foreach($detallesmodel as $key => $value){

                array_push($arrayRevisor, $value['idrevisor']);
                //$IdEvaluador=$value['idevaluador'];

            }

            $ListaRevisor = array_values(array_unique($arrayRevisor));

            foreach($ListaRevisor as $value){

                $Notificacion= new Notificacion;
                $Notificacion->fecha= date('d-m-Y');
                $Notificacion->nombre= 'Avalúo registrado';
                $Notificacion->estatus= 'A';
                $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
                $Notificacion->idusuario= $value;
                $Notificacion->idsolicitud= $obj->id;
                $Notificacion->idtipobien= $idalterno;
                $Notificacion->save();

                $data = array('subject'=>'Avalúo registrado');
                $UserRevisor = User::findOrFail($value);
                Mail::to($UserRevisor->email)->send(new SolicitudValuada($data));

            }

            $Detalle= new Detallesolicitud;
            $Detalle->fecha= date('d-m-Y');
            $Detalle->idusuario= Auth::user()->id;
            $Detalle->estatus= 'Valuada';
            $Detalle->idsolicitud= $obj->id;
            $Detalle->idtipobien= $idalterno;
            $Detalle->save();

            //BORRANDO LA NOTIFICACION ANTERIOR
            $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("nombre", "=", 'Usted fue asignado como Valuador')
            ->where("idtipobien", "=", $Bienautorizar->id)
            ->update(['estatus' => 'V']);

            $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("nombre", "=", 'Usted fue asignado como Revisor')
            ->where("idtipobien", "=", $Bienautorizar->id)
            ->update(['estatus' => 'V']);

        }


        if(isset($lrequest['_aprobar'])){

            $Solicitud = Solicitud::findOrFail($obj->id);
            $Bienautorizar = Bienevaluar::findOrFail($idalterno);

            $data = array('subject'=>'Solicitud de Avalúo Aprobada');
            
            $UserSolicitante = User::findOrFail($Solicitud->idusersolicitante);
            Mail::to($UserSolicitante->email)->send(new SolicitudAprobada($data));

            $Notificacion= new Notificacion;
            $Notificacion->fecha= date('d-m-Y');
            $Notificacion->nombre= 'Solicitud Aprobada';
            $Notificacion->estatus= 'A';
            $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
            $Notificacion->idusuario= $obj->idusersolicitante;
            $Notificacion->idsolicitud= $obj->id;
            $Notificacion->idtipobien= $idalterno;
            $Notificacion->save();

            $UserCotizante = User::findOrFail($Bienautorizar->idusercotizante);
            Mail::to($UserCotizante->email)->send(new SolicitudAprobada($data));

            $Notificacion= new Notificacion;
            $Notificacion->fecha= date('d-m-Y');
            $Notificacion->nombre= 'Solicitud Aprobada';
            $Notificacion->estatus= 'A';
            $Notificacion->ruta= "{{ route('solicitudes.view',".$obj->id.")}}";
            $Notificacion->idusuario= $Bienautorizar->idusercotizante;
            $Notificacion->idsolicitud= $obj->id;
            $Notificacion->idtipobien= $idalterno;
            $Notificacion->save();

            //ACA QUIEN APRUEBA ES EL VALIDADOR
            $Detalle= new Detallesolicitud;
            $Detalle->fecha= date('d-m-Y');
            $Detalle->idusuario= Auth::user()->id;
            $Detalle->estatus= 'Aprobada';
            $Detalle->idsolicitud= $obj->id;
            $Detalle->idtipobien= $idalterno;
            $Detalle->save();

            //BORRANDO LA NOTIFICACION ANTERIOR
            $UltimaNotificacion = Notificacion::where("idsolicitud", "=", $obj->id )
            ->where("nombre", "=", 'Usted fue asignado como Revisor')
            ->orWhere("nombre", "=", 'Usted fue asignado como Valuador')
            ->where("idtipobien", "=", $Bienautorizar->id)
            ->update(['estatus' => 'V']);


        }


        //dd("no entro");

    }

    /**
     * Permite eliminar una entidad del modelo en la BD. Es una eliminacion lógica
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cotizar($id, $type='logica', $retorno='index')
    {
        //Instanciamos el modelo
        $modelo=$this->getModelo('App\\'.$this->nombremodelo);
        //Cargamos la entidad a eliminar
        if($obj=$modelo::find($id)){
            //Iniciamos transaccion
            DB::beginTransaction();
            $continuar=true;
            //Acciones anteriores al borrado
            if(method_exists($this, "afterCotizar")){
                $continuar=$this->afterCotizar($obj);
            }
            //Eliminar entidad
            if($continuar){
                $this->msj=$this->nombremodelo.' cotizada correctamente.';

                $obj->estatus = 'O';
                if(!$obj->save()){
                    $this->msj=$this->nombremodelo.' no cotizada, por favor intente de nuevo más tarde.';
                    $continuar=false;
                }
                
            }
            //Acciones posteriores al borrado
            if($continuar){
                if(method_exists($this, "beforeCotizar")){
                    $continuar=$this->beforeCotizar($obj);
                }
            }
            //Termina transaccion
            if($continuar){
                DB::commit();
                $type='success';
            }
            else{
                DB::rollBack();
                $type='danger';
            }
        }
        else{
            $type='warning';
            $msj=$this->nombremodelo.' not found.';
        }
        return redirect()->route(explode("/",Route::getCurrentRoute()->uri)[0].'.'.$retorno)->with($type,$this->msj);
    }

    /**
     * Permite eliminar una entidad del modelo en la BD. Es una eliminacion lógica
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function autorizar($id, $type='logica', $retorno='index')
    {
        //Instanciamos el modelo
        $modelo=$this->getModelo('App\\'.$this->nombremodelo);
        //Cargamos la entidad a eliminar
        if($obj=$modelo::find($id)){
            //Iniciamos transaccion
            DB::beginTransaction();
            $continuar=true;
            //Acciones anteriores al borrado
            if(method_exists($this, "afterCotizar")){
                $continuar=$this->afterCotizar($obj);
            }
            //Eliminar entidad
            if($continuar){
                $this->msj=$this->nombremodelo.' cotizada correctamente.';

                $obj->estatus = 'U';
                if(!$obj->save()){
                    $this->msj=$this->nombremodelo.' no cotizada, por favor intente de nuevo más tarde.';
                    $continuar=false;
                }
                
            }
            //Acciones posteriores al borrado
            if($continuar){
                if(method_exists($this, "beforeCotizar")){
                    $continuar=$this->beforeCotizar($obj);
                }
            }
            //Termina transaccion
            if($continuar){
                DB::commit();
                $type='success';
            }
            else{
                DB::rollBack();
                $type='danger';
            }
        }
        else{
            $type='warning';
            $msj=$this->nombremodelo.' not found.';
        }
        return redirect()->route(explode("/",Route::getCurrentRoute()->uri)[0].'.'.$retorno)->with($type,$this->msj);
    }


}