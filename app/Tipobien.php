<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipobien extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipobienes';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre','idtipoavaluo', 'estatus', 'created_at', 'updated_at'];
}