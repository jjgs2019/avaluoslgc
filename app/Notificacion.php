<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notificaciones';

    /**
     * @var array
     */
    protected $fillable = ['id', 'ruta','idusuario','idtipobien', 'idsolicitud','nombre','fecha','estatus', 'created_at', 'updated_at'];
}