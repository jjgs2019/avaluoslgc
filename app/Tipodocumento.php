<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipodocumento extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipodocumentos';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre', 'estatus', 'created_at', 'updated_at'];
}