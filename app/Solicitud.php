<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'solicitudes';

    /**
     * @var array
     */
    protected $fillable = ['id', 'idtipopersona','idusersolicitante','nombre','rfc','telefono','email','idestado','idmunicipio','cp','colonia','calle','numero','observaciones','idusercotizante','folioexterior','observacionesaprobacion','fechacotizacion','fecha','importeavaluo', 'nombrerepresentantelegal', 'correorepresentantelegal', 'estatus', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipopersona()
    {
        return $this->belongsTo('App\Tipopersona', 'idtipopersona', 'id');
    }
}
