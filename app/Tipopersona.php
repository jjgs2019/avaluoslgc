<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipopersona extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipopersonas';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre', 'estatus', 'created_at', 'updated_at'];
}