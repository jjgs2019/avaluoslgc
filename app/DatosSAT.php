<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosSAT extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'datossat';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombre','rfc','telefono','idoficinasat','cargo','iduser', 'estatus', 'created_at', 'updated_at'];
}
