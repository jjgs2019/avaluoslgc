<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'configuraciones';

    /**
     * @var array
     */
    protected $fillable = ['id', 'cer', 'key','pass','estatus', 'created_at', 'updated_at'];
}