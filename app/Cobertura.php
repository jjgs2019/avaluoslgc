<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cobertura extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coberturas';

    /**
     * @var array
     */
    protected $fillable = ['id', 'idestado', 'iduser', 'created_at', 'updated_at'];
}