<?php

use App\Mail\UsuarioRegistrado;






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/










//RUTA POR DEFECTO
	Route::get('/', 'Auth\LoginController@showLoginForm');

//USUARIO NO AUTENTICADO
	Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('/login', 'Auth\LoginController@login');

//PAGINA DE AVISO DE USUARIO BLOQUEADO
	Route::get('/usuariobloqueado', 'UsuariobloqueadoController@index')->name('usuariobloqueado');

//SOLO USUARIO LOGUIADOS
	Route::middleware(['auth','permisos','estaactivo','web'])->group(function () {
		Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
		Route::get('/home', 'HomeController@index')->name('home');

		//USERS
			Route::get('/users/index', 'UsersController@index')->name('users.index');
			Route::get('/users/create', 'UsersController@create')->name('users.create');
			Route::post('/users/store', 'UsersController@store')->name('users.store');
			Route::get('/users/view/{id}', 'UsersController@view')->name('users.view');
			Route::get('/users/edit/{id}/{idalterno?}', 'UsersController@edit')->name('users.edit');
			Route::put('/users/{id}/{retorno?}/{idalterno?}', 'UsersController@update')->name('users.update');
			Route::post('/users/delete/{id}', 'UsersController@delete')->name('users.delete');
			Route::get('/users/cambiarpassword/{id}', 'UsersController@cambiarpassword')->name('users.cambiarpassword');
			Route::post('/users/updatepassword', 'UsersController@updatepassword')->name('users.updatepassword');
			Route::get('/users/permisos/{id}', 'UsersController@permisos')->name('users.permisos');
			Route::post('/users/spermisos', 'UsersController@spermisos')->name('users.spermisos');
			//Configuración de las datos de usuarios
			Route::get('/users/privacidad/{id}', 'UsersController@privacidad')->name('users.privacidad');
			Route::post('/users/sprivacidad', 'UsersController@sprivacidad')->name('users.sprivacidad');
			Route::match(array('GET', 'POST'), '/users/buscartituloprofesionales', 'UsersController@buscartituloprofesionales')->name('users.buscartituloprofesionales');

			Route::get('/users/descargarestudios/{archivo}', 'UsersController@descargarestudios')->name('users.descargarestudios');
			Route::get('/users/descargardocumentos/{archivo}', 'UsersController@descargardocumentos')->name('users.descargardocumentos');

			Route::get('/users/verestudios/{archivo}', 'UsersController@visualizarestudios')->name('users.visualizarestudios');
			Route::get('/users/verdocumentos/{archivo}', 'UsersController@visualizardocumentos')->name('users.visualizardocumentos');
			Route::post('/users/buscarusuario', 'UsersController@buscarusuario')->name('users.buscarusuario');

		//SOLICITUDES
			Route::get('/solicitudes/index', 'SolicitudController@index')->name('solicitudes.index');
			Route::get('/solicitudes/create', 'SolicitudController@create')->name('solicitudes.create');
			Route::post('/solicitudes/store', 'SolicitudController@store')->name('solicitudes.store');
			Route::get('/solicitudes/view/{id}/{idalterno?}', 'SolicitudController@view')->name('solicitudes.view');
			Route::get('/solicitudes/edit/{id}/{idalterno?}', 'SolicitudController@edit')->name('solicitudes.edit');
			Route::put('/solicitudes/{id}/{retorno?}/{idalterno?}', 'SolicitudController@update')->name('solicitudes.update');
			Route::post('/solicitudes/delete/{id}', 'SolicitudController@delete')->name('solicitudes.delete');
			Route::match(array('GET', 'POST'), '/solicitudes/buscartipodebien', 'SolicitudController@buscartipodebien')->name('solicitudes.buscartipodebien');
			Route::post('/solicitudes/cotizar/{id}', 'SolicitudController@cotizar')->name('solicitudes.cotizar');
			Route::post('/solicitudes/autorizar/{id}', 'SolicitudController@autorizar')->name('solicitudes.autorizar');


			Route::get('/solicitudes/descargar/{archivo}', 'SolicitudController@descargarPDF')->name('solicitudes.pdf');



			Route::match(array('GET', 'POST'), '/solicitudes/buscarmunicipio', 'SolicitudController@buscarmunicipio')->name('solicitudes.buscarmunicipio');


			Route::get('/solicitudes/descargardocumentossolicitud/{archivo}', 'SolicitudController@descargardocumentossolicitud')->name('solicitudes.descargardocumentossolicitud');
			
			Route::get('/solicitudes/visualizardocumentossolicitud/{archivo}', 'SolicitudController@visualizardocumentossolicitud')->name('solicitudes.visualizardocumentossolicitud');



			Route::get('/solicitudes/descargardocumentosevidencia/{archivo}', 'SolicitudController@descargardocumentosevidencia')->name('solicitudes.descargardocumentosevidencia');
			
			Route::get('/solicitudes/visualizardocumentosevidencia/{archivo}', 'SolicitudController@visualizardocumentosevidencia')->name('solicitudes.visualizardocumentosevidencia');



			//CONFIGURACIONES

			Route::get('/configuraciones/index', 'ConfiguracionController@index')->name('configuraciones.index');
			Route::get('/configuraciones/create', 'ConfiguracionController@create')->name('configuraciones.create');
			Route::get('/configuraciones/view/{id}/{idalterno?}', 'ConfiguracionController@view')->name('configuraciones.view');
			Route::get('/configuraciones/edit/{id}/{idalterno?}', 'ConfiguracionController@edit')->name('configuraciones.edit');
			Route::put('/configuraciones/{id}/{retorno?}/{idalterno?}', 'ConfiguracionController@update')->name('configuraciones.update');
			Route::post('/configuraciones/delete/{id}', 'ConfiguracionController@delete')->name('configuraciones.delete');
			Route::post('/configuraciones/store', 'ConfiguracionController@store')->name('configuraciones.store');

	

	});


	/*Route::get('storage/{archivo}', function ($archivo) {
     $public_path = public_path();
     $url = $public_path.'/storage/'.$archivo;
     //verificamos si el archivo existe y lo retornamos
     if (Storage::exists($archivo))
     {
       return response()->download($url);
     }
     //si no se encuentra lanzamos un error 404.
     abort(404);

});*/
