<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Configuracionweb;
use App\User;
use App\Modulo;
use App\Permiso;
use App\Estado;
use App\Tipoavaluo;
use App\Tituloprofesional;
use App\Tipousuario;
use App\Oficinasat;
use App\Nivelestudio;
use App\Tipodocumento;
use App\Tipopersona;
use App\Tipobien;
use App\DatosSAT;
use App\DatosLGC;
use App\Municipio;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();//ACTIVANDO INSERCION MASIVA DE DATOS EN LAS TABLAS
    		//CONFIGURACIONWEB
	            Configuracionweb::create(array(
   	             'nombre'=>'nombre',
	             'parametros'=>'LGC Avalúos',
	             'tipo'=> 1,
	            ));
	            Configuracionweb::create(array(
   	             'nombre'=>'titulo',
	             'parametros'=>'LGC Avalúos',
	             'tipo'=> 1,
	            ));
	            Configuracionweb::create(array(
                 'nombre'=>'nombre_negocio',
	             'parametros'=>'Fe soluciones, C.A',
	             'tipo'=> 1,
	            ));
	            Configuracionweb::create(array(
                 'nombre'=>'abreviatura',
	             'parametros'=>'LGC',
	             'tipo'=> 1,
	            ));
	            Configuracionweb::create(array(
                 'nombre'=>'logo',
	             'parametros'=>'img/logo.png',
	             'tipo'=> 1,
	            ));
	            Configuracionweb::create(array(
                 'nombre'=>'descripcion',
	             'parametros'=>'Software para Avaluos',
	             'tipo'=> 1,
	            ));
                Configuracionweb::create(array(
                 'nombre'=>'palabrasclaves',
                 'parametros'=>'avaluos',
                 'tipo'=> 1,
                ));
                Configuracionweb::create(array(
                 'nombre'=>'Emisora',
                 'parametros'=>'00578',
                 'tipo'=> 2,
                ));

            //TIPO PERSONA
                Tipopersona::create(array(
                 'id'=> 1,
                 'nombre'=>'Física',
                ));
                Tipopersona::create(array(
                 'id'=> 2,
                 'nombre'=>'Moral',
                ));

            //NIVEL DE ESTUDIOS
                Nivelestudio::create(array(
                 'id'=> 1,
                 'nombre'=>'Licenciatura',
                ));
                Nivelestudio::create(array(
                 'id'=> 2,
                 'nombre'=>'Especialidad',
                ));
                Nivelestudio::create(array(
                 'id'=> 3,
                 'nombre'=>'Maestría',
                ));
                Nivelestudio::create(array(
                 'id'=> 4,
                 'nombre'=>'Doctorado',
                ));

            //TÍTULOS PROFESIONALES
                Tituloprofesional::create(array(
                 'nombre'=>'Arquitecto',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. Agrónomo',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. Industrial',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. Mecánico',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. Químico',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. Topógrafo',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. Civil',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Ing. En sistemas',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Otro',
                 'idnivelestudio'=> 1,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Agropecuario',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Inmueble',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Intangibles',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Maquinaria',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Negocios',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Obra de Arte',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Otros',
                 'idnivelestudio'=> 2,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Administración',
                 'idnivelestudio'=> 3,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Valuación',
                 'idnivelestudio'=> 3,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Otros',
                 'idnivelestudio'=> 3,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Administración',
                 'idnivelestudio'=> 4,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Valuación',
                 'idnivelestudio'=> 4,
                ));
                Tituloprofesional::create(array(
                 'nombre'=>'Otros',
                 'idnivelestudio'=> 4,
                ));

            //TIPOS DOCUMENTOS
                Tipodocumento::create(array(
                 'nombre'=>'Fotografía',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Firma',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Rúbrica',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Licencia',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Pasaporte',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Visa',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Currículo',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Cédula Fiscal',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'INE',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'CURP',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'SAT 32-D',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Carta Protesta',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Usuarios_Documentos_Firma Electronica',
                ));
                Tipodocumento::create(array(
                 'nombre'=>'Otros',
                ));

            //TIPOS DE USUARIOS
                Tipousuario::create(array(
                 'id'=> 1,
                 'nombre'=>'Administrador LGC',
                ));
                Tipousuario::create(array(
                 'id'=> 2,
                 'nombre'=>'Administrador SAT',
                ));
                Tipousuario::create(array(
                 'id'=> 3,
                 'nombre'=>'Usuario SAT',
                ));
                Tipousuario::create(array(
                 'id'=> 4,
                 'nombre'=>'Revisor',
                ));
                Tipousuario::create(array(
                 'id'=> 5,
                 'nombre'=>'Valuador',
                ));

            //OFICINA SAT
                Oficinasat::create(array(
                 'id'=> 1,
                 'nombre'=>'Oficina SAT Norte',
                ));
                Oficinasat::create(array(
                 'id'=> 2,
                 'nombre'=>'Oficina SAT Sur',
                ));

            //USER
                User::create(array(
                 'id'=> 1,
                 'user'=>'Admin',
                 'password'=> bcrypt('123456'),
                 'email'=>'admin@gmail.com',
                 'idtipousuario'=> 1,
                ));

                DatosLGC::create(array(
                 'iduser'=> 1,
                 'nombre'=>'Juan Manuel',
                 'apellidopat'=>'Lopez',
                 'apellidomat'=>'Martinez',
                ));

                User::create(array(
                 'id'=> 2,
                 'user'=>'Admin SAT',
                 'password'=> bcrypt('123456'),
                 'email'=>'adminsat@gmail.com',
                 'idtipousuario'=> 2,
                ));

                DatosSAT::create(array(
                 'iduser'=> 2,
                 'nombre'=>'Gregorio Jose Baldan',
                 'cargo'=>'Gerente CCO',
                 'rfc'=>'XCD2020SD',
                 'telefono'=>'029356122',
                 'idoficinasat'=> 1,
                ));

                User::create(array(
                 'id'=> 3,
                 'user'=>'Revisor',
                 'password'=> bcrypt('123456'),
                 'email'=>'revisor01@gmail.com',
                 'idtipousuario'=> 4,
                ));

                DatosLGC::create(array(
                 'iduser'=> 3,
                 'nombre'=>'Jose Luis',
                 'apellidopat'=>'Arredondo',
                 'apellidomat'=>'Perez',
                ));

                User::create(array(
                 'id'=> 4,
                 'user'=>'Revisor',
                 'password'=> bcrypt('123456'),
                 'email'=>'revisor02@gmail.com',
                 'idtipousuario'=> 4,
                ));

                DatosLGC::create(array(
                 'iduser'=> 4,
                 'nombre'=>'Maria Luisa',
                 'apellidopat'=>'Baldan',
                 'apellidomat'=>'Salazar',
                ));

                User::create(array(
                 'id'=> 5,
                 'user'=>'Revisor',
                 'password'=> bcrypt('123456'),
                 'email'=>'revisor03@gmail.com',
                 'idtipousuario'=> 4,
                ));

                DatosLGC::create(array(
                 'iduser'=> 5,
                 'nombre'=>'Juana Jose',
                 'apellidopat'=>'Acuña',
                 'apellidomat'=>'Rivera',
                ));

                User::create(array(
                 'id'=> 6,
                 'user'=>'Valuador',
                 'password'=> bcrypt('123456'),
                 'email'=>'valuador01@gmail.com',
                 'idtipousuario'=> 5,
                ));

                DatosLGC::create(array(
                 'iduser'=> 6,
                 'nombre'=>'Javier Juan',
                 'apellidopat'=>'Navarro',
                 'apellidomat'=>'Martinez',
                ));
                User::create(array(
                 'id'=> 7,
                 'user'=>'Valuador',
                 'password'=> bcrypt('123456'),
                 'email'=>'valuador02@gmail.com',
                 'idtipousuario'=> 5,
                ));
                DatosLGC::create(array(
                 'iduser'=> 7,
                 'nombre'=>'Vanesa Maria',
                 'apellidopat'=>'Salazar',
                 'apellidomat'=>'Galindo',
                ));

                User::create(array(
                 'id'=> 8,
                 'user'=>'Valuador',
                 'password'=> bcrypt('123456'),
                 'email'=>'valuador03@gmail.com',
                 'idtipousuario'=> 5,
                ));

                DatosLGC::create(array(
                 'iduser'=> 8,
                 'nombre'=>'Esteban Luis',
                 'apellidopat'=>'Lemus',
                 'apellidomat'=>'Martinez',
                ));

                User::create(array(
                 'id'=> 5000,
                 'user'=>'Control2',
                 'password'=> bcrypt('123456'),
                 'email'=>'control2@gmail.com',
                 'idtipousuario'=> 1,
                 'estatus'=> 'D',
                ));

                User::create(array(
                 'id'=> 10000,
                 'user'=>'Control',
                 'password'=> bcrypt('123456'),
                 'email'=>'control@gmail.com',
                 'idtipousuario'=> 1,
                 'estatus'=> 'D',
                ));

            //TIPOS AVALÚOS
                Tipoavaluo::create(array(
                 'id'=> 1,
                 'nombre'=>'Activos Intangibles',
                ));
                Tipoavaluo::create(array(
                 'id'=> 2,
                 'nombre'=>'Bienes Muebles',
                ));
                Tipoavaluo::create(array(
                 'id'=> 3,
                 'nombre'=>'Inmuebles Rústicos',
                ));
                Tipoavaluo::create(array(
                 'id'=> 4,
                 'nombre'=>'Inmuebles Urbanos',
                ));
                Tipoavaluo::create(array(
                 'id'=> 5,
                 'nombre'=>'Obras Artísticas',
                ));
                Tipoavaluo::create(array(
                 'id'=> 6,
                 'nombre'=>'Otros',
                ));

            //TIPOS BIENES
                Tipobien::create(array(
                 'nombre'=>'Marcas',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Nombre Comercial',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Aviso Comercial',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Patentes',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Secreto Industrial',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Modelo de Utilidad',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Diseño Industrial',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Software',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Derecho de Autor',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Obra Literaria',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Obra de Arte',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Otros',
                 'idtipoavaluo'=> 1,
                ));
                Tipobien::create(array(
                 'nombre'=>'Mobiliario de Oficina',
                 'idtipoavaluo'=> 2,
                ));
                Tipobien::create(array(
                 'nombre'=>'Equipo de Transporte',
                 'idtipoavaluo'=> 2,
                ));
                Tipobien::create(array(
                 'nombre'=>'Equipo de Cómputo',
                 'idtipoavaluo'=> 2,
                ));
                Tipobien::create(array(
                 'nombre'=>'Equipo Industrial',
                 'idtipoavaluo'=> 2,
                ));
                Tipobien::create(array(
                 'nombre'=>'Otros',
                 'idtipoavaluo'=> 2,
                ));
                Tipobien::create(array(
                 'nombre'=>'Terreno Agricola',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Terreno Temporal',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Terreno Agostadero',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Huertas Frutales',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Cerriles',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Uso Acuícola',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Otros',
                 'idtipoavaluo'=> 3,
                ));
                Tipobien::create(array(
                 'nombre'=>'Terreno',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Casa Habitación',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Local Comercial',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Bodega',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Nave Industrial',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Edificio Habitacional',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Edificio Comercial',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Plaza Comercial',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Otros',
                 'idtipoavaluo'=> 4,
                ));
                Tipobien::create(array(
                 'nombre'=>'Colecciones Científicas',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Joyas',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Medallas',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Armas',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Antiguedades',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Instrumentos de Artes y Oficio',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Otros',
                 'idtipoavaluo'=> 5,
                ));
                Tipobien::create(array(
                 'nombre'=>'Otros',
                 'idtipoavaluo'=> 6,
                ));

            //ESTADOS
                Estado::create(array(
                 'id'=> 1,
                 'nombre'=>'Aguascalientes',
                ));
                Estado::create(array(
                 'id'=> 2,
                 'nombre'=>'Baja California',
                ));
                Estado::create(array(
                 'id'=> 3,
                 'nombre'=>'Baja California Sur',
                ));
                Estado::create(array(
                 'id'=> 4,
                 'nombre'=>'Campeche',
                ));
                Estado::create(array(
                 'id'=> 5,
                 'nombre'=>'Chiapas',
                ));
                Estado::create(array(
                 'id'=> 6,
                 'nombre'=>'Chihuahua',
                ));
                Estado::create(array(
                 'id'=> 7,
                 'nombre'=>'Coahuila de Zaragoza',
                ));
                Estado::create(array(
                 'id'=> 8,
                 'nombre'=>'Colima',
                ));
                Estado::create(array(
                 'id'=> 9,
                 'nombre'=>'Durango',
                ));
                Estado::create(array(
                 'id'=> 10,
                 'nombre'=>'Estado de México',
                ));
                Estado::create(array(
                 'id'=> 11,
                 'nombre'=>'Guanajuato',
                ));
                Estado::create(array(
                 'id'=> 12,
                 'nombre'=>'Guerrero',
                ));
                Estado::create(array(
                 'id'=> 13,
                 'nombre'=>'Hidalgo',
                ));
                Estado::create(array(
                 'id'=> 14,
                 'nombre'=>'Jalisco',
                ));
                Estado::create(array(
                 'id'=> 15,
                 'nombre'=>'Michoacán de Ocampo',
                ));
                Estado::create(array(
                 'id'=> 16,
                 'nombre'=>'Morelos',
                ));
                Estado::create(array(
                 'id'=> 17,
                 'nombre'=>'Nayarit',
                ));
                Estado::create(array(
                 'id'=> 18,
                 'nombre'=>'Nuevo León',
                ));
                Estado::create(array(
                 'id'=> 19,
                 'nombre'=>'Oaxaca',
                ));
                Estado::create(array(
                 'id'=> 20,
                 'nombre'=>'Puebla',
                ));
                Estado::create(array(
                 'id'=> 21,
                 'nombre'=>'Querétaro',
                ));
                Estado::create(array(
                 'id'=> 22,
                 'nombre'=>'Quintana Roo',
                ));
                Estado::create(array(
                 'id'=> 23,
                 'nombre'=>'San Luis Potosí',
                ));
                Estado::create(array(
                 'id'=> 24,
                 'nombre'=>'Sinaloa',
                ));
                Estado::create(array(
                 'id'=> 25,
                 'nombre'=>'Sonora',
                ));
                Estado::create(array(
                 'id'=> 26,
                 'nombre'=>'Tabasco',
                ));
                Estado::create(array(
                 'id'=> 27,
                 'nombre'=>'Tamaulipas',
                ));
                Estado::create(array(
                 'id'=> 28,
                 'nombre'=>'Tlaxcala',
                ));
                Estado::create(array(
                 'id'=> 29,
                 'nombre'=>'Veracruz de Ignacio de la Llave',
                ));
                Estado::create(array(
                 'id'=> 30,
                 'nombre'=>'Yucatán',
                ));
                Estado::create(array(
                 'id'=> 31,
                 'nombre'=>'Zacatecas',
                ));
                Estado::create(array(
                 'id'=> 32,
                 'nombre'=>'Ciudad de México',
                ));


Municipio::create(array( 'idestado'=> 1,'nombre'=>'Aguascalientes',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Asientos',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Calvillo',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Cosío',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Jesús maría',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Pabellón de arteaga',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Rincón de romos',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'San josé de gracia',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'Tepezalá',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'El llano',));
Municipio::create(array( 'idestado'=> 1,'nombre'=>'San francisco de los romo',));
Municipio::create(array( 'idestado'=> 2,'nombre'=>'Ensenada',));
Municipio::create(array( 'idestado'=> 2,'nombre'=>'Mexicali',));
Municipio::create(array( 'idestado'=> 2,'nombre'=>'Tecate',));
Municipio::create(array( 'idestado'=> 2,'nombre'=>'Tijuana',));
Municipio::create(array( 'idestado'=> 2,'nombre'=>'Playas de rosarito',));
Municipio::create(array( 'idestado'=> 3,'nombre'=>'Comondú',));
Municipio::create(array( 'idestado'=> 3,'nombre'=>'Mulegé',));
Municipio::create(array( 'idestado'=> 3,'nombre'=>'La paz',));
Municipio::create(array( 'idestado'=> 3,'nombre'=>'Los cabos',));
Municipio::create(array( 'idestado'=> 3,'nombre'=>'Loreto',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Calkiní',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Campeche',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Carmen',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Champotón',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Hecelchakán',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Hopelchén',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Palizada',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Tenabo',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Escárcega',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Calakmul',));
Municipio::create(array( 'idestado'=> 4,'nombre'=>'Candelaria',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Acacoyagua',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Acala',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Acapetahua',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Altamirano',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Amatán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Amatenango de la frontera',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Amatenango del valle',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Angel albino corzo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Arriaga',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Bejucal de ocampo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Bella vista',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Berriozábal',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Bochil',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'El bosque',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Cacahoatán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Catazajá',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Cintalapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Coapilla',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Comitán de domínguez',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'La concordia',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Copainalá',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chalchihuitán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chamula',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chanal',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chapultenango',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chenalhó',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chiapa de corzo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chiapilla',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chicoasén',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chicomuselo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Chilón',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Escuintla',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Francisco león',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Frontera comalapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Frontera hidalgo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'La grandeza',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Huehuetán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Huixtán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Huitiupán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Huixtla',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'La independencia',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ixhuatán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ixtacomitán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ixtapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ixtapangajoya',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Jiquipilas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Jitotol',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Juárez',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Larráinzar',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'La libertad',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Mapastepec',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Las margaritas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Mazapa de madero',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Mazatán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Metapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Mitontic',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Motozintla',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Nicolás ruíz',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ocosingo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ocotepec',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ocozocoautla de espinosa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Ostuacán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Osumacinta',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Oxchuc',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Palenque',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Pantelhó',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Pantepec',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Pichucalco',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Pijijiapan',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'El porvenir',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Villa comaltitlán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Pueblo nuevo solistahuacán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Rayón',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Reforma',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Las rosas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Sabanilla',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Salto de agua',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'San cristóbal de las casas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'San fernando',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Siltepec',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Simojovel',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Sitalá',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Socoltenango',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Solosuchiapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Soyaló',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Suchiapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Suchiate',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Sunuapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tapachula',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tapalapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tapilula',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tecpatán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tenejapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Teopisca',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tila',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tonalá',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Totolapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'La trinitaria',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tumbalá',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tuxtla gutiérrez',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tuxtla chico',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tuzantán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Tzimol',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Unión juárez',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Venustiano carranza',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Villa corzo',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Villaflores',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Yajalón',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'San lucas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Zinacantán',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'San juan cancuc',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Aldama',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Benemérito de las américas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Maravilla tenejapa',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Marqués de comillas',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Montecristo de guerrero',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'San andrés duraznal',));
Municipio::create(array( 'idestado'=> 5,'nombre'=>'Santiago el pinar',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Ahumada',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Aldama',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Allende',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Aquiles serdán',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Ascensión',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Bachíniva',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Balleza',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Batopilas',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Bocoyna',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Buenaventura',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Camargo',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Carichí',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Casas grandes',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Coronado',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Coyame del sotol',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'La cruz',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Cuauhtémoc',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Cusihuiriachi',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Chihuahua',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Chínipas',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Delicias',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Dr. Belisario domínguez',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Galeana',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Santa isabel',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Gómez farías',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Gran morelos',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Guachochi',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Guadalupe',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Guadalupe y calvo',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Guazapares',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Guerrero',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Hidalgo del parral',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Huejotitán',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Ignacio zaragoza',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Janos',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Jiménez',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Juárez',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Julimes',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'López',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Madera',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Maguarichi',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Manuel benavides',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Matachí',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Matamoros',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Meoqui',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Morelos',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Moris',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Namiquipa',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Nonoava',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Nuevo casas grandes',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Ocampo',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Ojinaga',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Praxedis g. Guerrero',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Riva palacio',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Rosales',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Rosario',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'San francisco de borja',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'San francisco de conchos',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'San francisco del oro',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Santa bárbara',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Satevó',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Saucillo',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Temósachic',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'El tule',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Urique',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Uruachi',));
Municipio::create(array( 'idestado'=> 6,'nombre'=>'Valle de zaragoza',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Azcapotzalco',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Coyoacán',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Cuajimalpa de morelos',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Gustavo a. Madero',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Iztacalco',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Iztapalapa',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'La magdalena contreras',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Milpa alta',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Álvaro obregón',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Tláhuac',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Tlalpan',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Xochimilco',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Cuauhtémoc',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Miguel hidalgo',));
Municipio::create(array( 'idestado'=> 32, 'nombre'=>'Venustiano carranza',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Abasolo',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Acuña',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Allende',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Arteaga',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Candela',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Castaños',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Cuatro ciénegas',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Escobedo',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Francisco i. Madero',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Frontera',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'General cepeda',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Guerrero',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Hidalgo',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Jiménez',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Juárez',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Lamadrid',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Matamoros',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Monclova',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Morelos',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Múzquiz',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Nadadores',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Nava',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Ocampo',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Parras',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Piedras negras',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Progreso',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Ramos arizpe',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Sabinas',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Sacramento',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Saltillo',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'San buenaventura',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'San juan de sabinas',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'San pedro',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Sierra mojada',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Torreón',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Viesca',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Villa unión',));
Municipio::create(array( 'idestado'=> 7,'nombre'=>'Zaragoza',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Armería',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Colima',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Comala',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Coquimatlán',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Cuauhtémoc',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Ixtlahuacán',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Manzanillo',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Minatitlán',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Tecomán',));
Municipio::create(array( 'idestado'=> 8,'nombre'=>'Villa de álvarez',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Canatlán',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Canelas',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Coneto de comonfort',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Cuencamé',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Durango',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'General simón bolívar',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Gómez palacio',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Guadalupe victoria',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Guanaceví',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Hidalgo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Indé',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Lerdo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Mapimí',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Mezquital',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Nazas',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Nombre de dios',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Ocampo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'El oro',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Otáez',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Pánuco de coronado',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Peñón blanco',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Poanas',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Pueblo nuevo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Rodeo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'San bernardo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'San dimas',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'San juan de guadalupe',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'San juan del río',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'San luis del cordero',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'San pedro del gallo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Santa clara',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Santiago papasquiaro',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Súchil',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Tamazula',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Tepehuanes',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Tlahualilo',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Topia',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Vicente guerrero',));
Municipio::create(array( 'idestado'=> 9,'nombre'=>'Nuevo ideal',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Abasolo',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Acámbaro',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'San miguel de allende',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Apaseo el alto',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Apaseo el grande',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Atarjea',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Celaya',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Manuel doblado',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Comonfort',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Coroneo',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Cortazar',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Cuerámaro',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Doctor mora',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Dolores hidalgo cuna de la independencia nacional',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Guanajuato',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Huanímaro',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Irapuato',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Jaral del progreso',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Jerécuaro',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'León',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Moroleón',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Ocampo',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Pénjamo',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Pueblo nuevo',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Purísima del rincón',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Romita',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Salamanca',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Salvatierra',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'San diego de la unión',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'San felipe',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'San francisco del rincón',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'San josé iturbide',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'San luis de la paz',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Santa catarina',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Santa cruz de juventino rosas',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Santiago maravatío',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Silao de la victoria',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Tarandacuao',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Tarimoro',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Tierra blanca',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Uriangato',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Valle de santiago',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Victoria',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Villagrán',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Xichú',));
Municipio::create(array( 'idestado'=> 11, 'nombre'=>'Yuriria',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Acapulco de juárez',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Ahuacuotzingo',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Ajuchitlán del progreso',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Alcozauca de guerrero',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Alpoyeca',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Apaxtla',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Arcelia',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Atenango del río',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Atlamajalcingo del monte',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Atlixtac',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Atoyac de álvarez',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Ayutla de los libres',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Azoyú',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Buenavista de cuéllar',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Coahuayutla de josé maría izazaga',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cocula',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Copala',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Copalillo',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Copanatoyac',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Coyuca de benítez',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Coyuca de catalán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cuajinicuilapa',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cualác',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cuautepec',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cuetzala del progreso',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cutzamala de pinzón',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Chilapa de álvarez',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Chilpancingo de los bravo',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Florencio villarreal',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'General canuto a. Neri',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'General heliodoro castillo',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Huamuxtitlán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Huitzuco de los figueroa',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Iguala de la independencia',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Igualapa',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Ixcateopan de cuauhtémoc',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Zihuatanejo de azueta',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Juan r. Escudero',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Leonardo bravo',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Malinaltepec',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Mártir de cuilapan',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Metlatónoc',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Mochitlán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Olinalá',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Ometepec',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Pedro ascencio alquisiras',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Petatlán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Pilcaya',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Pungarabato',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Quechultenango',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'San luis acatlán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'San marcos',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'San miguel totolapan',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Taxco de alarcón',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tecoanapa',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Técpan de galeana',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Teloloapan',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tepecoacuilco de trujano',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tetipac',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tixtla de guerrero',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tlacoachistlahuaca',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tlacoapa',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tlalchapa',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tlalixtaquilla de maldonado',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tlapa de comonfort',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Tlapehuala',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'La unión de isidoro montes de oca',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Xalpatláhuac',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Xochihuehuetlán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Xochistlahuaca',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Zapotitlán tablas',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Zirándaro',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Zitlala',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Eduardo neri',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Acatepec',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Marquelia',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Cochoapa el grande',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'José joaquín de herrera',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Juchitán',));
Municipio::create(array( 'idestado'=> 12, 'nombre'=>'Iliatenco',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Acatlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Acaxochitlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Actopan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Agua blanca de iturbide',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Ajacuba',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Alfajayucan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Almoloya',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Apan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'El arenal',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Atitalaquia',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Atlapexco',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Atotonilco el grande',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Atotonilco de tula',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Calnali',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Cardonal',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Cuautepec de hinojosa',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Chapantongo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Chapulhuacán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Chilcuautla',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Eloxochitlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Emiliano zapata',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Epazoyucan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Francisco i. Madero',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Huasca de ocampo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Huautla',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Huazalingo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Huehuetla',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Huejutla de reyes',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Huichapan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Ixmiquilpan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Jacala de ledezma',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Jaltocán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Juárez hidalgo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Lolotla',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Metepec',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'San agustín metzquititlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Metztitlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Mineral del chico',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Mineral del monte',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'La misión',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Mixquiahuala de juárez',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Molango de escamilla',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Nicolás flores',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Nopala de villagrán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Omitlán de juárez',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'San felipe orizatlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Pacula',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Pachuca de soto',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Pisaflores',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Progreso de obregón',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Mineral de la reforma',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'San agustín tlaxiaca',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'San bartolo tutotepec',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'San salvador',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Santiago de anaya',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Santiago tulantepec de lugo guerrero',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Singuilucan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tasquillo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tecozautla',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tenango de doria',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tepeapulco',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tepehuacán de guerrero',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tepeji del río de ocampo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tepetitlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tetepango',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Villa de tezontepec',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tezontepec de aldama',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tianguistengo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tizayuca',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tlahuelilpan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tlahuiltepa',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tlanalapa',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tlanchinol',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tlaxcoapan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tolcayuca',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tula de allende',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Tulancingo de bravo',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Xochiatipan',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Xochicoatlán',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Yahualica',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Zacualtipán de ángeles',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Zapotlán de juárez',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Zempoala',));
Municipio::create(array( 'idestado'=> 13, 'nombre'=>'Zimapán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Acatic',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Acatlán de juárez',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ahualulco de mercado',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Amacueca',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Amatitán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ameca',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San juanito de escobedo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Arandas',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'El arenal',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Atemajac de brizuela',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Atengo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Atenguillo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Atotonilco el alto',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Atoyac',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Autlán de navarro',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ayotlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ayutla',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'La barca',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Bolaños',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cabo corrientes',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Casimiro castillo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cihuatlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zapotlán el grande',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cocula',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Colotlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Concepción de buenos aires',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cuautitlán de garcía barragán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cuautla',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cuquío',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Chapala',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Chimaltitán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Chiquilistlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Degollado',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ejutla',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Encarnación de díaz',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Etzatlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'El grullo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Guachinango',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Guadalajara',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Hostotipaquillo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Huejúcar',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Huejuquilla el alto',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'La huerta',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ixtlahuacán de los membrillos',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ixtlahuacán del río',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Jalostotitlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Jamay',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Jesús maría',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Jilotlán de los dolores',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Jocotepec',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Juanacatlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Juchitlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Lagos de moreno',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'El limón',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Magdalena',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Santa maría del oro',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'La manzanilla de la paz',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Mascota',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Mazamitla',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Mexticacán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Mezquitic',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Mixtlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ocotlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Ojuelos de jalisco',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Pihuamo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Poncitlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Puerto vallarta',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Villa purificación',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Quitupan',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'El salto',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San cristóbal de la barranca',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San diego de alejandría',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San juan de los lagos',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San julián',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San marcos',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San martín de bolaños',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San martín hidalgo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San miguel el alto',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Gómez farías',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San sebastián del oeste',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Santa maría de los ángeles',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Sayula',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tala',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Talpa de allende',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tamazula de gordiano',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tapalpa',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tecalitlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tecolotlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Techaluta de montenegro',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tenamaxtlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Teocaltiche',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Teocuitatlán de corona',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tepatitlán de morelos',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tequila',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Teuchitlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tizapán el alto',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tlajomulco de zúñiga',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San pedro tlaquepaque',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tolimán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tomatlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tonalá',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tonaya',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tonila',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Totatiche',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tototlán',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tuxcacuesco',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tuxcueca',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Tuxpan',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Unión de san antonio',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Unión de tula',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Valle de guadalupe',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Valle de juárez',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San gabriel',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Villa corona',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Villa guerrero',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Villa hidalgo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Cañadas de obregón',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Yahualica de gonzález gallo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zacoalco de torres',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zapopan',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zapotiltic',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zapotitlán de vadillo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zapotlán del rey',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'Zapotlanejo',));
Municipio::create(array( 'idestado'=> 14, 'nombre'=>'San ignacio cerro gordo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Acambay de ruíz castañeda',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Acolman',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Aculco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Almoloya de alquisiras',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Almoloya de juárez',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Almoloya del río',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Amanalco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Amatepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Amecameca',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Apaxco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Atenco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Atizapán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Atizapán de zaragoza',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Atlacomulco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Atlautla',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Axapusco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ayapango',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Calimaya',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Capulhuac',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Coacalco de berriozábal',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Coatepec harinas',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Cocotitlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Coyotepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Cuautitlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chalco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chapa de mota',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chapultepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chiautla',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chicoloapan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chiconcuac',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Chimalhuacán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Donato guerra',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ecatepec de morelos',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ecatzingo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Huehuetoca',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Hueypoxtla',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Huixquilucan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Isidro fabela',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ixtapaluca',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ixtapan de la sal',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ixtapan del oro',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ixtlahuaca',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Xalatlaco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Jaltenco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Jilotepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Jilotzingo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Jiquipilco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Jocotitlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Joquicingo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Juchitepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Lerma',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Malinalco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Melchor ocampo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Metepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Mexicaltzingo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Morelos',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Naucalpan de juárez',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Nezahualcóyotl',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Nextlalpan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Nicolás romero',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Nopaltepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ocoyoacac',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ocuilan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'El oro',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Otumba',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Otzoloapan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Otzolotepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Ozumba',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Papalotla',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'La paz',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Polotitlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Rayón',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'San antonio la isla',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'San felipe del progreso',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'San martín de las pirámides',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'San mateo atenco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'San simón de guerrero',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Santo tomás',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Soyaniquilpan de juárez',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Sultepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tecámac',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tejupilco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Temamatla',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Temascalapa',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Temascalcingo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Temascaltepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Temoaya',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tenancingo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tenango del aire',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tenango del valle',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Teoloyucan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Teotihuacán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tepetlaoxtoc',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tepetlixpa',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tepotzotlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tequixquiac',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Texcaltitlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Texcalyacac',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Texcoco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tezoyuca',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tianguistenco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Timilpan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tlalmanalco',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tlalnepantla de baz',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tlatlaya',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Toluca',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tonatico',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tultepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tultitlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Valle de bravo',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Villa de allende',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Villa del carbón',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Villa guerrero',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Villa victoria',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Xonacatlán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Zacazonapan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Zacualpan',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Zinacantepec',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Zumpahuacán',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Zumpango',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Cuautitlán izcalli',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Valle de chalco solidaridad',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Luvianos',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'San josé del rincón',));
Municipio::create(array( 'idestado'=> 10, 'nombre'=>'Tonanitla',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Acuitzio',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Aguililla',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Álvaro obregón',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Angamacutiro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Angangueo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Apatzingán',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Aporo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Aquila',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Ario',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Arteaga',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Briseñas',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Buenavista',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Carácuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Coahuayana',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Coalcomán de vázquez pallares',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Coeneo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Contepec',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Copándaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Cotija',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Cuitzeo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Charapan',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Charo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Chavinda',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Cherán',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Chilchota',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Chinicuila',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Chucándiro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Churintzio',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Churumuco',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Ecuandureo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Epitacio huerta',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Erongarícuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Gabriel zamora',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Hidalgo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'La huacana',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Huandacareo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Huaniqueo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Huetamo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Huiramba',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Indaparapeo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Irimbo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Ixtlán',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Jacona',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Jiménez',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Jiquilpan',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Juárez',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Jungapeo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Lagunillas',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Madero',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Maravatío',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Marcos castellanos',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Lázaro cárdenas',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Morelia',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Morelos',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Múgica',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Nahuatzen',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Nocupétaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Nuevo parangaricutiro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Nuevo urecho',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Numarán',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Ocampo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Pajacuarán',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Panindícuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Parácuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Paracho',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Pátzcuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Penjamillo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Peribán',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'La piedad',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Purépero',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Puruándiro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Queréndaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Quiroga',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Cojumatlán de régules',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Los reyes',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Sahuayo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'San lucas',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Santa ana maya',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Salvador escalante',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Senguio',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Susupuato',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tacámbaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tancítaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tangamandapio',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tangancícuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tanhuato',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Taretan',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tarímbaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tepalcatepec',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tingambato',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tingüindín',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tiquicheo de nicolás romero',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tlalpujahua',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tlazazalca',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tocumbo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tumbiscatío',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Turicato',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tuxpan',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tuzantla',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tzintzuntzan',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Tzitzio',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Uruapan',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Venustiano carranza',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Villamar',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Vista hermosa',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Yurécuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Zacapu',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Zamora',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Zináparo',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Zinapécuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Ziracuaretiro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'Zitácuaro',));
Municipio::create(array( 'idestado'=> 15, 'nombre'=>'José sixto verduzco',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Amacuzac',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Atlatlahucan',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Axochiapan',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Ayala',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Coatlán del río',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Cuautla',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Cuernavaca',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Emiliano zapata',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Huitzilac',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Jantetelco',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Jiutepec',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Jojutla',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Jonacatepec de leandro valle',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Mazatepec',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Miacatlán',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Ocuituco',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Puente de ixtla',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Temixco',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tepalcingo',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tepoztlán',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tetecala',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tetela del volcán',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tlalnepantla',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tlaltizapán de zapata',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tlaquiltenango',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Tlayacapan',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Totolapan',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Xochitepec',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Yautepec',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Yecapixtla',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Zacatepec',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Zacualpan de amilpas',));
Municipio::create(array( 'idestado'=> 16, 'nombre'=>'Temoac',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Acaponeta',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Ahuacatlán',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Amatlán de cañas',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Compostela',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Huajicori',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Ixtlán del río',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Jala',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Xalisco',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Del nayar',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Rosamorada',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Ruíz',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'San blas',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'San pedro lagunillas',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Santa maría del oro',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Santiago ixcuintla',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Tecuala',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Tepic',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Tuxpan',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'La yesca',));
Municipio::create(array( 'idestado'=> 17, 'nombre'=>'Bahía de banderas',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Abasolo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Agualeguas',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Los aldamas',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Allende',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Anáhuac',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Apodaca',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Aramberri',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Bustamante',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Cadereyta jiménez',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'El carmen',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Cerralvo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Ciénega de flores',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'China',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Doctor arroyo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Doctor coss',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Doctor gonzález',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Galeana',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'García',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'San pedro garza garcía',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'General bravo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'General escobedo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'General terán',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'General treviño',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'General zaragoza',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'General zuazua',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Guadalupe',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Los herreras',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Higueras',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Hualahuises',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Iturbide',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Juárez',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Lampazos de naranjo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Linares',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Marín',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Melchor ocampo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Mier y noriega',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Mina',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Montemorelos',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Monterrey',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Parás',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Pesquería',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Los ramones',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Rayones',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Sabinas hidalgo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Salinas victoria',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'San nicolás de los garza',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Hidalgo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Santa catarina',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Santiago',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Vallecillo',));
Municipio::create(array( 'idestado'=> 18, 'nombre'=>'Villaldama',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Abejones',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Acatlán de pérez figueroa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Asunción cacalotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Asunción cuyotepeji',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Asunción ixtaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Asunción nochixtlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Asunción ocotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Asunción tlacolulita',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ayotzintepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'El barrio de la soledad',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Calihualá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Candelaria loxicha',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ciénega de zimatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ciudad ixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Coatecas altas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Coicoyán de las flores',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'La compañía',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Concepción buenavista',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Concepción pápalo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Constancia del rosario',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Cosolapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Cosoltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Cuilápam de guerrero',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Cuyamecalco villa de zaragoza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Chahuites',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Chalcatongo de hidalgo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Chiquihuitlán de benito juárez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Heroica ciudad de ejutla de crespo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Eloxochitlán de flores magón',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'El espinal',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tamazulápam del espíritu santo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Fresnillo de trujano',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Guadalupe etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Guadalupe de ramírez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Guelatao de juárez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Guevea de humboldt',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Mesones hidalgo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa hidalgo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Heroica ciudad de huajuapan de león',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Huautepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Huautla de jiménez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ixtlán de juárez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Heroica ciudad de juchitán de zaragoza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Loma bonita',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena apasco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena jaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa magdalena jicotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena ocotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena peñasco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena teitipac',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena tequisistlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena tlacotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena zahuatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Mariscala de juárez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Mártires de tacubaya',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Matías romero avendaño',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Mazatlán villa de flores',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Miahuatlán de porfirio díaz',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Mixistlán de la reforma',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Monjas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Natividad',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Nazareno etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Nejapa de madero',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ixpantepec nieves',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago niltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Oaxaca de juárez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ocotlán de morelos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'La pe',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Pinotepa de don luis',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Pluma hidalgo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé del progreso',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Putla villa de guerrero',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina quioquitani',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Reforma de pineda',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'La reforma',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Reyes etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Rojas de cuauhtémoc',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Salina cruz',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín amatengo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín atenango',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín chayuco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín de las juntas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín loxicha',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín tlacotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San agustín yatareni',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés cabecera nueva',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés dinicuiti',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés huaxpaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés huayápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés ixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés lagunas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés nuxiño',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés paxtlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés sinaxtla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés solaga',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés teotilálpam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés tepetlapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés yaá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés zabache',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San andrés zautla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonino castillo velasco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonino el alto',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonino monte verde',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonio acutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonio de la cal',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonio huitepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonio nanahuatípam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonio sinicahua',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San antonio tepetlapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San baltazar chichicápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San baltazar loxicha',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San baltazar yatzachi el bajo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolo coyotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolomé ayautla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolomé loxicha',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolomé quialana',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolomé yucuañe',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolomé zoogocho',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolo soyaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bartolo yautepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San bernardo mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San blas atempa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San carlos yautepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San cristóbal amatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San cristóbal amoltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San cristóbal lachirioag',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San cristóbal suchixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San dionisio del mar',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San dionisio ocotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San dionisio ocotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San esteban atatlahuca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San felipe jalapa de díaz',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San felipe tejalápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San felipe usila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco cahuacuá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco cajonos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco chapulapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco chindúa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco del mar',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco huehuetlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco ixhuatán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco jaltepetongo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco lachigoló',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco logueche',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco nuxaño',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco ozolotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco sola',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco telixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco teopan',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San francisco tlapancingo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San gabriel mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San ildefonso amatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San ildefonso sola',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San ildefonso villa alta',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jacinto amilpas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jacinto tlacotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jerónimo coatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jerónimo silacayoapilla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jerónimo sosola',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jerónimo taviche',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jerónimo tecóatl',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jorge nuchita',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé ayuquila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé chiltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé del peñasco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé estancia grande',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé independencia',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé lachiguiri',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San josé tenango',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan achiutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan atepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ánimas trujano',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista atatlahuca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista coixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista cuicatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista guelache',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista jayacatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista lo de soto',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista suchitepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista tlacoatzintepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista tlachichilco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista tuxtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan cacahuatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan cieneguilla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan coatzóspam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan colorado',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan comaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan cotzocón',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan chicomezúchil',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan chilateca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan del estado',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan del río',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan diuxi',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan evangelista analco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan guelavía',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan guichicovi',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan ihualtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan juquila mixes',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan juquila vijanos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan lachao',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan lachigalla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan lajarcia',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan lalana',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan de los cués',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan mazatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan ñumí',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan ozolotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan petlapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan quiahije',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan quiotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan sayultepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan tabaá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan tamazola',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan teita',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan teitipac',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan tepeuxila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan teposcolula',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan yaeé',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan yatzona',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan yucuita',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lorenzo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lorenzo albarradas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lorenzo cacaotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lorenzo cuaunecuiltitla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lorenzo texmelúcan',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lorenzo victoria',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lucas camotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lucas ojitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lucas quiaviní',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San lucas zoquiápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San luis amatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San marcial ozolotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San marcos arteaga',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín de los cansecos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín huamelúlpam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín itunyoso',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín lachilá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín peras',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín tilcajete',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín toxpalan',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San martín zacatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo cajonos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Capulálpam de méndez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo del mar',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo yoloxochitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo etlatongo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo nejápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo peñasco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo piñas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo río hondo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo sindihui',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo tlapiltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San melchor betaza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel achiutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel ahuehuetitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel aloápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel amatitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel amatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel coatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel chicahua',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel chimalapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel del puerto',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel del río',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel ejutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel el grande',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel huautla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel panixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel peras',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel piedras',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel quetzaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel santa flor',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa sola de vega',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel soyaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel suchixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa talea de castro',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tecomatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tenango',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tequixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tilquiápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tlacamama',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tlacotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel tulancingo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San miguel yotao',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San nicolás',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San nicolás hidalgo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo coatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo cuatro venados',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo huitzo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo huixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo macuiltianguis',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo tijaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo villa de mitla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pablo yaganiza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro amuzgos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro apóstol',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro atoyac',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro cajonos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro coxcaltepec cántaros',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro comitancillo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro el alto',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro huamelula',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro huilotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro ixcatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro ixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro jaltepetongo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro jicayán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro jocotipac',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro juchatengo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro mártir',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro mártir quiechapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro mártir yucuxaco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro molinos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro nopala',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro ocopetatillo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro ocotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro pochutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro quiatoni',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro sochiápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro tapanatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro taviche',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro teozacoalco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro teutila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro tidaá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro topiltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro totolápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa de tututepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro yaneri',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro yólox',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro y san pablo ayutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa de etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro y san pablo teposcolula',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro y san pablo tequixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San pedro yucunama',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San raymundo jalpan',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián abasolo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián coatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián ixcapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián nicananduta',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián río hondo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián tecomaxtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián teitipac',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San sebastián tutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San simón almolongas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San simón zahuatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana ateixtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana cuauhtémoc',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana del valle',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana tavela',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana tlapacoyan',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana yareni',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa ana zegache',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catalina quierí',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina cuixtla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina ixtepeji',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina juquila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina lachatao',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina loxicha',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina mechoacán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina minas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina quiané',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina tayata',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina ticuá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina yosonotú',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa catarina zapoquila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz acatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz amilpas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz de bravo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz itundujia',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz mixtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz nundaco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz papalutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz tacache de mina',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz tacahua',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz tayata',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz xitla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz xoxocotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa cruz zenzontepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa gertrudis',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa inés del monte',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa inés yatzeche',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa lucía del camino',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa lucía miahuatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa lucía monteverde',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa lucía ocotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría alotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría apazco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría la asunción',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Heroica ciudad de tlaxiaco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Ayoquezco de aldama',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría atzompa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría camotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría colotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría cortijo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría coyotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría chachoápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa de chilapa de díaz',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría chilchotla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría chimalapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría del rosario',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría del tule',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría ecatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría guelacé',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría guienagati',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría huatulco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría huazolotitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría ipalapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría ixcatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría jacatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría jalapa del marqués',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría jaltianguis',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría lachixío',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría mixtequilla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría nativitas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría nduayaco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría ozolotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría pápalo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría peñoles',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría petapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría quiegolani',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría sola',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría tataltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría tecomavaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría temaxcalapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría temaxcaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría teopoxco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría tepantlali',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría texcatitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría tlahuitoltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría tlalixtac',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría tonameca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría totolapilla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría xadani',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría yalina',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría yavesía',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría yolotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría yosoyúa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría yucuhiti',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría zacatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría zaniza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa maría zoquitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago amoltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago apoala',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago apóstol',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago astata',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago atitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago ayuquililla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago cacaloxtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago camotlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago comaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago chazumba',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago choápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago del río',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago huajolotitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago huauclilla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago ihuitlán plumas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago ixcuintepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago ixtayutla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago jamiltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago jocotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago juxtlahuaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago lachiguiri',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago lalopa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago laollaga',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago laxopa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago llano grande',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago matatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago miltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago minas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago nacaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago nejapilla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago nundiche',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago nuyoó',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago pinotepa nacional',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago suchilquitongo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tamazola',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tapextla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa tejúpam de la unión',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tenango',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tepetlapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tetepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago texcalcingo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago textitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tilantongo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tillo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago tlazoyaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago xanica',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago xiacuí',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago yaitepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago yaveo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago yolomécatl',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago yosondúa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago yucuyachi',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago zacatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santiago zoochila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Nuevo zoquiápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo ingenio',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo albarradas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo armenta',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo chihuitán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo de morelos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo ixcatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo nuxaá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo ozolotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo petapa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo roayaga',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo tehuantepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo teojomulco',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo tepuxtepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo tlatayápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo tomaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo tonalá',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo tonaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo xagacía',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo yanhuitlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo yodohino',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo domingo zanatepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santos reyes nopala',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santos reyes pápalo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santos reyes tepejillo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santos reyes yucuná',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo tomás jalieza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo tomás mazaltepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo tomás ocotepec',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santo tomás tamazulapan',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San vicente coatlán',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San vicente lachixío',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San vicente nuñú',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Silacayoápam',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Sitio de xitlapehua',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Soledad etla',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa de tamazulápam del progreso',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tanetze de zaragoza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Taniche',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tataltepec de valdés',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Teococuilco de marcos pérez',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Teotitlán de flores magón',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Teotitlán del valle',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Teotongo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tepelmeme villa de morelos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Heroica villa tezoatlán de segura y luna, cuna de la independencia de oaxaca',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San jerónimo tlacochahuaya',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tlacolula de matamoros',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tlacotepec plumas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Tlalixtac de cabrera',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Totontepec villa de morelos',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Trinidad zaachila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'La trinidad vista hermosa',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Unión hidalgo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Valerio trujano',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San juan bautista valle nacional',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa díaz ordaz',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Yaxe',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Magdalena yodocono de porfirio díaz',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Yogana',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Yutanduchi de guerrero',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Villa de zaachila',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'San mateo yucutindoo',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Zapotitlán lagunas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Zapotitlán palmas',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Santa inés de zaragoza',));
Municipio::create(array( 'idestado'=> 19, 'nombre'=>'Zimatlán de álvarez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Acajete',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Acateno',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Acatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Acatzingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Acteopan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ahuacatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ahuatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ahuazotepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ahuehuetitla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ajalpan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Albino zertuche',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Aljojuca',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Altepexi',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Amixtlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Amozoc',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Aquixtla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atempan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atexcal',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atlixco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atoyatempan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atzala',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atzitzihuacán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atzitzintla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Axutla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ayotoxco de guerrero',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Calpan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Caltepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Camocuautla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Caxhuacan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Coatepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Coatzingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cohetzala',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cohuecan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Coronango',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Coxcatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Coyomeapan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Coyotepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuapiaxtla de madero',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuautempan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuautinchán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuautlancingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuayuca de andrade',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuetzalan del progreso',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cuyoaco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chalchicomula de sesma',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chapulco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chiautla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chiautzingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chiconcuautla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chichiquila',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chietla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chigmecatitlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chignahuapan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chignautla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chila',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chila de la sal',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Honey',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chilchotla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Chinantla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Domingo arenas',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Eloxochitlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Epatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Esperanza',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Francisco z. Mena',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'General felipe ángeles',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Guadalupe',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Guadalupe victoria',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Hermenegildo galeana',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huaquechula',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huatlatlauca',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huauchinango',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huehuetla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huehuetlán el chico',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huejotzingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Hueyapan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Hueytamalco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Hueytlalpan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huitzilan de serdán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huitziltepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Atlequizayan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ixcamilpa de guerrero',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ixcaquixtla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ixtacamaxtitlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ixtepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Izúcar de matamoros',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Jalpan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Jolalpan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Jonotla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Jopala',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Juan c. Bonilla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Juan galindo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Juan n. Méndez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Lafragua',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Libres',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'La magdalena tlatlauquitepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Mazapiltepec de juárez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Mixtla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Molcaxac',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Cañada morelos',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Naupan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Nauzontla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Nealtican',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Nicolás bravo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Nopalucan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ocotepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Ocoyucan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Olintla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Oriental',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Pahuatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Palmar de bravo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Pantepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Petlalcingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Piaxtla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Puebla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Quecholac',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Quimixtlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Rafael lara grajales',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Los reyes de juárez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San andrés cholula',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San antonio cañada',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San diego la mesa tochimiltzingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San felipe teotlalcingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San felipe tepatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San gabriel chilac',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San gregorio atzompa',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San jerónimo tecuanipan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San jerónimo xayacatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San josé chiapa',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San josé miahuatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San juan atenco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San juan atzompa',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San martín texmelucan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San martín totoltepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San matías tlalancaleca',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San miguel ixitlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San miguel xoxtla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San nicolás buenos aires',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San nicolás de los ranchos',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San pablo anicano',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San pedro cholula',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San pedro yeloixtlahuaca',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San salvador el seco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San salvador el verde',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San salvador huixcolotla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'San sebastián tlacotepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Santa catarina tlaltempan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Santa inés ahuatempan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Santa isabel cholula',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Santiago miahuatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Huehuetlán el grande',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Santo tomás hueyotlipan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Soltepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tecali de herrera',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tecamachalco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tecomatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tehuacán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tehuitzingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tenampulco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Teopantlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Teotlalco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepanco de lópez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepango de rodríguez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepatlaxco de hidalgo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepeaca',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepemaxalco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepeojuma',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepetzintla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepexco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepexi de rodríguez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepeyahualco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tepeyahualco de cuauhtémoc',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tetela de ocampo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Teteles de avila castillo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Teziutlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tianguismanalco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tilapa',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlacotepec de benito juárez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlacuilotepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlachichuca',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlahuapan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlaltenango',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlanepantla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlaola',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlapacoya',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlapanalá',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlatlauquitepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tlaxco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tochimilco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tochtepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Totoltepec de guerrero',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tulcingo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tuzamapan de galeana',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Tzicatlacoyan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Venustiano carranza',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Vicente guerrero',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xayacatlán de bravo',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xicotepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xicotlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xiutetelco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xochiapulco',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xochiltepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xochitlán de vicente suárez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Xochitlán todos santos',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Yaonáhuac',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Yehualtepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zacapala',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zacapoaxtla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zacatlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zapotitlán',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zapotitlán de méndez',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zaragoza',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zautla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zihuateutla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zinacatepec',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zongozotla',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zoquiapan',));
Municipio::create(array( 'idestado'=> 20, 'nombre'=>'Zoquitlán',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Amealco de bonfil',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Pinal de amoles',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Arroyo seco',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Cadereyta de montes',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Colón',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Corregidora',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Ezequiel montes',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Huimilpan',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Jalpan de serra',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Landa de matamoros',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'El marqués',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Pedro escobedo',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Peñamiller',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Querétaro',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'San joaquín',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'San juan del río',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Tequisquiapan',));
Municipio::create(array( 'idestado'=> 21, 'nombre'=>'Tolimán',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Cozumel',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Felipe carrillo puerto',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Isla mujeres',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Othón p. Blanco',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'José maría morelos',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Lázaro cárdenas',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Solidaridad',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Tulum',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Bacalar',));
Municipio::create(array( 'idestado'=> 22,'nombre'=>'Puerto morelos',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Ahualulco',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Alaquines',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Aquismón',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Armadillo de los infante',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Cárdenas',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Catorce',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Cedral',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Cerritos',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Cerro de san pedro',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Ciudad del maíz',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Ciudad fernández',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tancanhuitz',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Ciudad valles',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Coxcatlán',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Charcas',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Ebano',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Guadalcázar',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Huehuetlán',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Lagunillas',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Matehuala',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Mexquitic de carmona',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Moctezuma',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Rayón',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Rioverde',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Salinas',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'San antonio',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'San ciro de acosta',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'San luis potosí',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'San martín chalchicuautla',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'San nicolás tolentino',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Santa catarina',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Santa maría del río',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Santo domingo',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'San vicente tancuayalab',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Soledad de graciano sánchez',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tamasopo',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tamazunchale',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tampacán',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tampamolón corona',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tamuín',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tanlajás',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tanquián de escobedo',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Tierra nueva',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Vanegas',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Venado',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa de arriaga',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa de guadalupe',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa de la paz',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa de ramos',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa de reyes',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa hidalgo',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa juárez',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Axtla de terrazas',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Xilitla',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Zaragoza',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Villa de arista',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'Matlapa',));
Municipio::create(array( 'idestado'=> 23, 'nombre'=>'El naranjo',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Ahome',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Angostura',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Badiraguato',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Concordia',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Cosalá',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Culiacán',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Choix',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Elota',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Escuinapa',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'El fuerte',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Guasave',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Mazatlán',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Mocorito',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Rosario',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Salvador alvarado',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'San ignacio',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Sinaloa',));
Municipio::create(array( 'idestado'=> 24, 'nombre'=>'Navolato',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Aconchi',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Agua prieta',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Alamos',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Altar',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Arivechi',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Arizpe',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Atil',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Bacadéhuachi',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Bacanora',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Bacerac',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Bacoachi',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Bácum',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Banámichi',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Baviácora',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Bavispe',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Benjamín hill',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Caborca',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Cajeme',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Cananea',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Carbó',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'La colorada',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Cucurpe',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Cumpas',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Divisaderos',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Empalme',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Etchojoa',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Fronteras',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Granados',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Guaymas',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Hermosillo',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Huachinera',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Huásabas',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Huatabampo',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Huépac',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Imuris',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Magdalena',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Mazatán',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Moctezuma',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Naco',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Nácori chico',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Nacozari de garcía',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Navojoa',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Nogales',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Onavas',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Opodepe',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Oquitoa',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Pitiquito',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Puerto peñasco',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Quiriego',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Rayón',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Rosario',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Sahuaripa',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'San felipe de jesús',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'San javier',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'San luis río colorado',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'San miguel de horcasitas',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'San pedro de la cueva',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Santa ana',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Santa cruz',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Sáric',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Soyopa',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Suaqui grande',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Tepache',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Trincheras',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Tubutama',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Ures',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Villa hidalgo',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Villa pesqueira',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Yécora',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'General plutarco elías calles',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 25, 'nombre'=>'San ignacio río muerto',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Balancán',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Cárdenas',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Centla',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Centro',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Comalcalco',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Cunduacán',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Emiliano zapata',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Huimanguillo',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Jalapa',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Jalpa de méndez',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Jonuta',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Macuspana',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Nacajuca',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Paraíso',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Tacotalpa',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Teapa',));
Municipio::create(array( 'idestado'=> 26, 'nombre'=>'Tenosique',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Abasolo',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Aldama',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Altamira',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Antiguo morelos',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Burgos',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Bustamante',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Camargo',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Casas',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Ciudad madero',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Cruillas',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Gómez farías',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'González',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Güémez',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Guerrero',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Gustavo díaz ordaz',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Hidalgo',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Jaumave',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Jiménez',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Llera',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Mainero',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'El mante',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Matamoros',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Méndez',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Mier',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Miguel alemán',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Miquihuana',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Nuevo laredo',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Nuevo morelos',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Ocampo',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Padilla',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Palmillas',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Reynosa',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Río bravo',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'San carlos',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'San fernando',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'San nicolás',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Soto la marina',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Tampico',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Tula',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Valle hermoso',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Victoria',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Villagrán',));
Municipio::create(array( 'idestado'=> 27, 'nombre'=>'Xicoténcatl',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Amaxac de guerrero',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Apetatitlán de antonio carvajal',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Atlangatepec',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Atltzayanca',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Apizaco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Calpulalpan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'El carmen tequexquitla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Cuapiaxtla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Cuaxomulco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Chiautempan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Muñoz de domingo arenas',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Españita',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Huamantla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Hueyotlipan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Ixtacuixtla de mariano matamoros',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Ixtenco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Mazatecochco de josé maría morelos',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Contla de juan cuamatzi',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tepetitla de lardizábal',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Sanctórum de lázaro cárdenas',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Nanacamilpa de mariano arista',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Acuamanala de miguel hidalgo',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Natívitas',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Panotla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San pablo del monte',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Santa cruz tlaxcala',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tenancingo',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Teolocholco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tepeyanco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Terrenate',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tetla de la solidaridad',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tetlatlahuca',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tlaxcala',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tlaxco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tocatlán',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Totolac',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Ziltlaltépec de trinidad sánchez santos',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Tzompantepec',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Xaloztoc',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Xaltocan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Papalotla de xicohténcatl',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Xicohtzinco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Yauhquemehcan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Zacatelco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Emiliano zapata',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Lázaro cárdenas',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'La magdalena tlaltelulco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San damián texóloc',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San francisco tetlanohcan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San jerónimo zacualpan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San josé teacalco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San juan huactzinco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San lorenzo axocomanitla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'San lucas tecopilco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Santa ana nopalucan',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Santa apolonia teacalco',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Santa catarina ayometla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Santa cruz quilehtla',));
Municipio::create(array( 'idestado'=> 28, 'nombre'=>'Santa isabel xiloxoxtla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Acajete',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Acatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Acayucan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Actopan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Acula',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Acultzingo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Camarón de tejeda',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Alpatláhuac',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Alto lucero de gutiérrez barrios',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Altotonga',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Alvarado',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Amatitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Naranjos amatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Amatlán de los reyes',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Angel r. Cabada',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'La antigua',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Apazapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Aquila',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Astacinga',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Atlahuilco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Atoyac',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Atzacan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Atzalan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlaltetela',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ayahualulco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Banderilla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Boca del río',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Calcahualco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Camerino z. Mendoza',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Carrillo puerto',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Catemaco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cazones de herrera',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cerro azul',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Citlaltépetl',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coacoatzintla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coahuitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coatepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coatzacoalcos',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coatzintla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coetzala',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Colipa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Comapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Córdoba',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cosamaloapan de carpio',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cosautlán de carvajal',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coscomatepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cosoleacaque',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cotaxtla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coxquihui',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Coyutla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cuichapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Cuitláhuac',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chacaltianguis',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chalma',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chiconamel',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chiconquiaco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chicontepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chinameca',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chinampa de gorostiza',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Las choapas',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chocamán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chontla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Chumatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Emiliano zapata',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Espinal',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Filomeno mata',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Fortín',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Gutiérrez zamora',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Hidalgotitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Huatusco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Huayacocotla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Hueyapan de ocampo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Huiloapan de cuauhtémoc',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ignacio de la llave',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ilamatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Isla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixcatepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixhuacán de los reyes',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixhuatlán del café',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixhuatlancillo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixhuatlán del sureste',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixhuatlán de madero',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixmatlahuacan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ixtaczoquitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Jalacingo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Xalapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Jalcomulco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Jáltipan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Jamapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Jesús carranza',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Xico',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Jilotepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Juan rodríguez clara',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Juchique de ferrer',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Landero y coss',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Lerdo de tejada',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Magdalena',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Maltrata',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Manlio fabio altamirano',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Mariano escobedo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Martínez de la torre',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Mecatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Mecayapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Medellín de bravo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Miahuatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Las minas',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Minatitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Misantla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Mixtla de altamirano',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Moloacán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Naolinco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Naranjal',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Nautla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Nogales',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Oluta',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Omealca',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Orizaba',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Otatitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Oteapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ozuluama de mascareñas',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Pajapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Pánuco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Papantla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Paso del macho',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Paso de ovejas',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'La perla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Perote',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Platón sánchez',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Playa vicente',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Poza rica de hidalgo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Las vigas de ramírez',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Pueblo viejo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Puente nacional',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Rafael delgado',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Rafael lucio',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Los reyes',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Río blanco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Saltabarranca',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'San andrés tenejapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'San andrés tuxtla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'San juan evangelista',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Santiago tuxtla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Sayula de alemán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Soconusco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Sochiapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Soledad atzompa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Soledad de doblado',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Soteapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tamalín',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tamiahua',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tampico alto',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tancoco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tantima',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tantoyuca',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tatatila',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Castillo de teayo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tecolutla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tehuipango',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Álamo temapache',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tempoal',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tenampa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tenochtitlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Teocelo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tepatlaxco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tepetlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tepetzintla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tequila',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'José azueta',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Texcatepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Texhuacán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Texistepec',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tezonapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tierra blanca',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tihuatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlacojalpan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlacolulan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlacotalpan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlacotepec de mejía',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlachichilco',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlalixcoyan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlalnelhuayocan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlapacoyan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlaquilpa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tlilapan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tomatlán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tonayán',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Totutla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tuxpan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tuxtilla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Ursulo galván',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Vega de alatorre',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Veracruz',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Villa aldama',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Xoxocotla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Yanga',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Yecuatla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Zacualpan',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Zaragoza',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Zentla',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Zongolica',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Zontecomatlán de lópez y fuentes',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Zozocolco de hidalgo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Agua dulce',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'El higo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Nanchital de lázaro cárdenas del río',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tres valles',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Carlos a. Carrillo',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Tatahuicapan de juárez',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Uxpanapa',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'San rafael',));
Municipio::create(array( 'idestado'=> 29, 'nombre'=>'Santiago sochiapan',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Abalá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Acanceh',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Akil',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Baca',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Bokobá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Buctzotz',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Cacalchén',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Calotmul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Cansahcab',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Cantamayec',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Celestún',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Cenotillo',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Conkal',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Cuncunul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Cuzamá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chacsinkín',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chankom',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chapab',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chemax',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chicxulub pueblo',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chichimilá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chikindzonot',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chocholá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Chumayel',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzán',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzemul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzidzantún',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzilam de bravo',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzilam gonzález',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzitás',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Dzoncauich',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Espita',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Halachó',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Hocabá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Hoctún',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Homún',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Huhí',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Hunucmá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Ixil',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Izamal',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Kanasín',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Kantunil',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Kaua',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Kinchil',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Kopomá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Mama',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Maní',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Maxcanú',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Mayapán',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Mérida',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Mocochá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Motul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Muna',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Muxupip',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Opichén',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Oxkutzcab',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Panabá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Peto',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Progreso',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Quintana roo',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Río lagartos',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Sacalum',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Samahil',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Sanahcat',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'San felipe',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Santa elena',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Seyé',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Sinanché',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Sotuta',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Sucilá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Sudzal',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Suma',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tahdziú',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tahmek',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Teabo',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tecoh',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tekal de venegas',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tekantó',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tekax',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tekit',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tekom',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Telchac pueblo',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Telchac puerto',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Temax',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Temozón',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tepakán',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tetiz',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Teya',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Ticul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Timucuy',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tinum',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tixcacalcupul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tixkokob',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tixmehuac',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tixpéhual',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tizimín',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tunkás',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Tzucacab',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Uayma',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Ucú',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Umán',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Valladolid',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Xocchel',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Yaxcabá',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Yaxkukul',));
Municipio::create(array( 'idestado'=> 30, 'nombre'=>'Yobaín',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Apozol',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Apulco',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Atolinga',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Benito juárez',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Calera',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Cañitas de felipe pescador',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Concepción del oro',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Cuauhtémoc',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Chalchihuites',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Fresnillo',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Trinidad garcía de la cadena',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Genaro codina',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'General enrique estrada',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'General francisco r. Murguía',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'El plateado de joaquín amaro',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'General pánfilo natera',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Guadalupe',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Huanusco',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Jalpa',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Jerez',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Jiménez del teul',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Juan aldama',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Juchipila',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Loreto',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Luis moya',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Mazapil',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Melchor ocampo',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Mezquital del oro',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Miguel auza',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Momax',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Monte escobedo',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Morelos',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Moyahua de estrada',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Nochistlán de mejía',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Noria de ángeles',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Ojocaliente',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Pánuco',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Pinos',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Río grande',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Sain alto',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'El salvador',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Sombrerete',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Susticacán',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Tabasco',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Tepechitlán',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Tepetongo',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Teúl de gonzález ortega',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Tlaltenango de sánchez román',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Valparaíso',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Vetagrande',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Villa de cos',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Villa garcía',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Villa gonzález ortega',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Villa hidalgo',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Villanueva',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Zacatecas',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Trancoso',));
Municipio::create(array( 'idestado'=> 31, 'nombre'=>'Santa maría de la paz',));

        	//CATALOGO
                Modulo::create(array(
                 'id'=> 1,
                 'controlador'=>'UsersController',
                 'accion'=>'',
                 'nombreruta'=>'',
                 'idpadre'=> 0,
                 'icono'=>'fas fa-table',
                 'nombre'=>'Catálogos',
                 'dependede'=> null,
                 'orden'=> 100,
                 'estatus'=>'A',
                ));
                //PERMISOS A CATALOGO
                    Permiso::create(array(
                     'iduser'=> 1,
                     'idmodulo'=> 1,
                     'estatus'=>'A',
                    ));
                    Permiso::create(array(
                     'iduser'=> 5000,
                     'idmodulo'=> 1,
                     'estatus'=>'A',
                    ));

                //USUARIOS 2 - 20
                    //MODULOS
                        Modulo::create(array(
                         'id'=> 2,
                         'controlador'=>'UsersController',
                         'accion'=>'index',
                         'nombreruta'=>'users.index',
                         'idpadre'=> 1,
                         'icono'=>'fas fa-users',
                         'nombre'=>'Usuarios',
                         'color_permiso'=>'#495057',
                         'background_permiso'=>'#c9b4dd',
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Listado',
                         'descripcion'=>'Permite listar los usuarios',
                         'dependede'=> -3,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 3,
                         'controlador'=>'UsersController',
                         'accion'=>'create',
                         'nombreruta'=>'users.create',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Crear',
                         'descripcion'=>'Permite crear un usuario',
                         'dependede'=> 4,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 4,
                         'controlador'=>'UsersController',
                         'accion'=>'store',
                         'nombreruta'=>'users.store',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 5,
                         'controlador'=>'UsersController',
                         'accion'=>'edit',
                         'nombreruta'=>'users.edit',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Editar',
                         'descripcion'=>'Permite editar un usuario',
                         'dependede'=> 6,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 6,
                         'controlador'=>'UsersController',
                         'accion'=>'update',
                         'nombreruta'=>'users.update',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 7,
                         'controlador'=>'UsersController',
                         'accion'=>'delete',
                         'nombreruta'=>'users.delete',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Eliminar',
                         'descripcion'=>'Permite eliminar un usuario',
                         'dependede'=> -1,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 8,
                         'controlador'=>'UsersController',
                         'accion'=>'view',
                         'nombreruta'=>'users.view',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Mostrar',
                         'descripcion'=>'Permite mostrar un usuario',
                         'dependede'=> -2,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 9,
                         'controlador'=>'UsersController',
                         'accion'=>'cambiarpassword',
                         'nombreruta'=>'users.cambiarpassword',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Cambiar password',
                         'descripcion'=>'Permite cambiar el password de un usuario',
                         'dependede'=> 10,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 10,
                         'controlador'=>'UsersController',
                         'accion'=>'updatepassword',
                         'nombreruta'=>'users.updatepassword',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 11,
                         'controlador'=>'UsersController',
                         'accion'=>'permisos',
                         'nombreruta'=>'users.permisos',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Crear permisos',
                         'descripcion'=>'Permite crear un permiso',
                         'dependede'=> 12,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 12,
                         'controlador'=>'UsersController',
                         'accion'=>'spermisos',
                         'nombreruta'=>'users.spermisos',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 13,
                         'controlador'=>'UsersController',
                         'accion'=>'privacidad',
                         'nombreruta'=>'users.privacidad',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Usuarios',
                         'nombre_permiso'=>'Privacidad',
                         'descripcion'=>'Permite configurar los datos del usuario',
                         'dependede'=> 14,
                         'orden'=> 1,
                         'ajax'=>'1',
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 14,
                         'controlador'=>'UsersController',
                         'accion'=>'sprivacidad',
                         'nombreruta'=>'users.sprivacidad',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'ajax'=>'1',
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 15,
                         'controlador'=>'UsersController',
                         'accion'=>'buscartituloprofesionales',
                         'nombreruta'=>'users.buscartituloprofesionales',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'ajax'=>'1',
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 16,
                         'controlador'=>'UsersController',
                         'accion'=>'buscarusuario',
                         'nombreruta'=>'users.buscarusuario',
                         'idpadre'=> 2,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'ajax'=>'1',
                         'estatus'=>'A',
                        ));
                    //PERMISOS
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 2,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 3,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 4,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 5,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 6,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 7,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 8,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 9,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 10,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 11,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 12,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 13,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 14,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 15,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 16,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 1,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 2,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 3,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 4,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 5,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 6,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 7,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 8,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 9,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 10,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 11,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 12,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 13,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 14,
                         'estatus'=>'A',
                        ));





                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 2,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 3,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 4,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 5,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 6,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 7,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 8,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 9,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 10,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 11,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 12,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 13,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 14,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 15,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 16,
                         'estatus'=>'A',
                        ));

            //PROCESOS
                Modulo::create(array(
                 'id'=> 200,
                 'controlador'=>'SolicitudController',
                 'accion'=>'',
                 'nombreruta'=>'',
                 'idpadre'=> 0,
                 'icono'=>'fas fa-cogs',
                 'nombre'=>'Procesos',
                 'dependede'=> null,
                 'orden'=> 99,
                 'estatus'=>'A',
                ));
                //PERMISOS A PROCESOS
                    Permiso::create(array(
                     'iduser'=> 1,
                     'idmodulo'=> 200,
                     'estatus'=>'A',
                    ));
                    Permiso::create(array(
                     'iduser'=> 2,
                     'idmodulo'=> 200,
                     'estatus'=>'A',
                    ));
                    Permiso::create(array(
                     'iduser'=> 10000,
                     'idmodulo'=> 200,
                     'estatus'=>'A',
                    ));
                    Permiso::create(array(
                     'iduser'=> 5000,
                     'idmodulo'=> 200,
                     'estatus'=>'A',
                    ));

                //SOLICITUDES 100 - 200
                    //MODULOS
                        Modulo::create(array(
                         'id'=> 100,
                         'controlador'=>'SolicitudController',
                         'accion'=>'index',
                         'nombreruta'=>'solicitudes.index',
                         'idpadre'=> 200,
                         'icono'=>'fas fa-users',
                         'nombre'=>'Solicitudes',
                         'color_permiso'=>'#495057',
                         'background_permiso'=>'#c9b4dd',
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Listado',
                         'descripcion'=>'Permite listar las solicitud',
                         'dependede'=> -3,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 101,
                         'controlador'=>'SolicitudController',
                         'accion'=>'create',
                         'nombreruta'=>'solicitudes.create',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Crear',
                         'descripcion'=>'Permite crear una solicitud',
                         'dependede'=> 102,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 102,
                         'controlador'=>'SolicitudController',
                         'accion'=>'store',
                         'nombreruta'=>'solicitudes.store',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 103,
                         'controlador'=>'SolicitudController',
                         'accion'=>'edit',
                         'nombreruta'=>'solicitudes.edit',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Editar',
                         'descripcion'=>'Permite editar una solicitud',
                         'dependede'=> 104,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 104,
                         'controlador'=>'SolicitudController',
                         'accion'=>'update',
                         'nombreruta'=>'solicitudes.update',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 105,
                         'controlador'=>'SolicitudController',
                         'accion'=>'delete',
                         'nombreruta'=>'solicitudes.delete',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Eliminar',
                         'descripcion'=>'Permite eliminar una solicitud',
                         'dependede'=> -1,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 106,
                         'controlador'=>'SolicitudController',
                         'accion'=>'view',
                         'nombreruta'=>'solicitudes.view',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Mostrar',
                         'descripcion'=>'Permite mostrar una solicitud',
                         'dependede'=> -2,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 107,
                         'controlador'=>'SolicitudController',
                         'accion'=>'buscartipodebien',
                         'nombreruta'=>'solicitudes.buscartipodebien',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'ajax'=>'1',
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 108,
                         'controlador'=>'SolicitudController',
                         'accion'=>'cotizar',
                         'nombreruta'=>'solicitudes.cotizar',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Cotizar',
                         'descripcion'=>'Permite cotizar una solicitud',
                         'dependede'=> -1,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 109,
                         'controlador'=>'SolicitudController',
                         'accion'=>'autorizar',
                         'nombreruta'=>'solicitudes.autorizar',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Solicitudes',
                         'nombre_permiso'=>'Autorizar',
                         'descripcion'=>'Permite autorizar una solicitud',
                         'dependede'=> -1,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                        Modulo::create(array(
                         'id'=> 110,
                         'controlador'=>'SolicitudController',
                         'accion'=>'buscarmunicipio',
                         'nombreruta'=>'solicitudes.buscarmunicipio',
                         'idpadre'=> 100,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'ajax'=>'1',
                         'estatus'=>'A',
                        ));





                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 100,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 101,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 102,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 103,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 104,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 105,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 106,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 107,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 108,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 10000,
                         'idmodulo'=> 110,
                         'estatus'=>'A',
                        ));




                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 100,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 101,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 102,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 103,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 104,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 105,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 106,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 107,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 108,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 5000,
                         'idmodulo'=> 110,
                         'estatus'=>'A',
                        ));










                        


                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 100,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 101,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 102,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 103,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 104,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 105,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 106,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 107,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 108,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 110,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 100,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 101,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 102,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 103,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 104,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 105,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 106,
                         'estatus'=>'A',
                        ));
                        Permiso::create(array(
                         'iduser'=> 2,
                         'idmodulo'=> 107,
                         'estatus'=>'A',
                        ));

       	


            //CONFIGURACION
                Modulo::create(array(
                 'id'=> 300,
                 'controlador'=>'ConfiguracionController',
                 'accion'=>'',
                 'nombreruta'=>'',
                 'idpadre'=> 0,
                 'icono' => 'fa fa-tasks',
                 'nombre'=>'Configuraciones',
                 'dependede'=> null,
                 'orden'=> 50,
                 'estatus'=>'A',
                ));
                //PERMISOS A PROCESOS
                    Permiso::create(array(
                     'iduser'=> 1,
                     'idmodulo'=> 300,
                     'estatus'=>'A',
                    ));

                //CONFIGURACIONES 500 - 1000
                    Modulo::create(array(
                         'id'=> 301,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'index',
                         'nombreruta'=>'configuraciones.index',
                         'idpadre'=> 300,
                         'icono' => 'fa fa-tasks',
                         'nombre'=>'Certificados',
                         'color_permiso'=>'#495057',
                         'background_permiso'=>'#c9b4dd',
                         'modulo_permiso'=>'Certificados',
                         'nombre_permiso'=>'Listado',
                         'descripcion'=>'Permite listar los certificados',
                         'dependede'=> -3,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));

                    Modulo::create(array(
                         'id'=> 302,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'create',
                         'nombreruta'=>'configuraciones.create',
                         'idpadre'=> 300,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Certificados',
                         'nombre_permiso'=>'Crear',
                         'descripcion'=>'Permite crear un certificado',
                         'dependede'=> 303,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                    Modulo::create(array(
                         'id'=> 303,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'store',
                         'nombreruta'=>'configuraciones.store',
                         'idpadre'=> 300,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                    Modulo::create(array(
                         'id'=> 304,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'edit',
                         'nombreruta'=>'configuraciones.edit',
                         'idpadre'=> 300,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Certificados',
                         'nombre_permiso'=>'Editar',
                         'descripcion'=>'Permite editar un certificado',
                         'dependede'=> 305,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                    Modulo::create(array(
                         'id'=> 305,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'update',
                         'nombreruta'=>'configuraciones.update',
                         'idpadre'=> 300,
                         'icono'=> null,
                         'nombre'=> null,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                    Modulo::create(array(
                         'id'=> 306,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'delete',
                         'nombreruta'=>'configuraciones.delete',
                         'idpadre'=> 300,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Certificados',
                         'nombre_permiso'=>'Eliminar',
                         'descripcion'=>'Permite eliminar un certificado',
                         'dependede'=> -1,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));
                    Modulo::create(array(
                         'id'=> 307,
                         'controlador'=>'ConfiguracionController',
                         'accion'=>'view',
                         'nombreruta'=>'configuraciones.view',
                         'idpadre'=> 300,
                         'icono'=> null,
                         'nombre'=> null,
                         'modulo_permiso'=>'Certificados',
                         'nombre_permiso'=>'Mostrar',
                         'descripcion'=>'Permite mostrar un certificado',
                         'dependede'=> -2,
                         'orden'=> 1,
                         'estatus'=>'A',
                        ));

                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 301,
                         'estatus'=>'A',
                        ));
                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 302,
                         'estatus'=>'A',
                        ));
                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 303,
                         'estatus'=>'A',
                        ));
                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 304,
                         'estatus'=>'A',
                        ));
                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 305,
                         'estatus'=>'A',
                        ));
                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 306,
                         'estatus'=>'A',
                        ));
                    Permiso::create(array(
                         'iduser'=> 1,
                         'idmodulo'=> 307,
                         'estatus'=>'A',
                        ));
            

        Model::reguard();
    }
}
