<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('idnivelestudio')->nullable()->index('ESTUDIOS_NIVELESTUDIOS_FK');
            $table->integer('idtituloprofesional')->nullable()->index('ESTUDIOS_TITULOPROFESIONAL_FK');
            $table->integer('iduser')->nullable()->index('ESTUDIOS_USERS_FK');
            $table->string('comentario', 255)->nullable();
            $table->string('cedula', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
    }
}