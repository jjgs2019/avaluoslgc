@extends('plantillas.privada')
@section('content')

    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('vendor/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

    <style>

        .text-small {
          font-size: 13px;
        }

    </style>

    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('solicitudes_editar',array('id'=>$Solicitud->id,'nombre'=>$Solicitud->id>0?$pluck["ubicacion"].' '.$Solicitud->id:"Crear nueva solicitud SAT")) }}

    <!-- EDITAR USUARIOS -->
    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i><span id="span-accion">Editar solicitud SAT</span>
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="text-right">
                <a href="{{ route('solicitudes.index')}}">
                    <button type="button" class="btn btn-dark">
                        <i class="fa fa-arrow-left"></i> Volver
                    </button>
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="card shadow mb-4">
            <!-- FORMULARIO  USUARIOS -->
            {{ Form::model($Solicitud, ['action' => ['SolicitudController@update', $Solicitud->id>0?$Solicitud->id:-1,'index', $pluck["ubicacion"]!="Crear"?$pluck['Bienevaluar'][0]->id:0], 'id' => 'SolicitudEditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}

          
            <?php if( $pluck["ubicacion"] == 'Editar'):?>

                {{ Form::hidden('_editar',null,['id'=>'_editar'])}}

            <?php endif; ?>  


            {{ Form::hidden('_bienevaluar',null,['id'=>'_bienevaluar'])}}

            <?php if( $Solicitud->id <= 0 ):?>

                {{ Form::hidden('idusersolicitante',Auth::user()->id,['id'=>'idusersolicitante'])}}

                {{ Form::hidden('fecha',date('d-m-Y'),['id'=>'fecha'])}}

            <?php endif;?>


            {{ Form::hidden('_cotizar',null,['id'=>'_cotizar'])}}
            {{ Form::hidden('_autorizar',null,['id'=>'_autorizar','disabled'=>true])}}
            {{ Form::hidden('_asignar',null,['id'=>'_asignar'])}}
            {{ Form::hidden('_valuar',null,['id'=>'_valuar'])}}
            {{ Form::hidden('_aprobar',null,['id'=>'_aprobar'])}}
            {{ Form::hidden('_cancelar',null,['id'=>'_cancelar','disabled'=>true])}}

         <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && isset($pluck['Bienevaluar']) ):?>   

                {{ Form::hidden('idusercotizante',$pluck['Bienevaluar'][0]->estatus == 'A'?Auth::user()->id:$pluck['Bienevaluar'][0]->idusercotizante,['id'=>'idusercotizante'])}}

  
        <?php endif;?>
        
        		@if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
               	<div class="row">
               		<div class="col-12">
                        <div class="alert alert-success d-none" role="alert" id="alertfinal">
                            <h3 class="alert-heading">Por favor espere <i class="fa fa-circle-notch fa-spin text-success"></i></h3>
                        </div>
                    </div>
               	</div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group required">
                        {{ Form::label('folioexterior', 'Folio Externo') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'folioexterior',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'nombre',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese folio exterior',
                                            'minlength'=>'3',
                                            'maxlength'=>'100',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div> 
                </div>
                <?php if(isset($pluck['Bienevaluar'])):?>
                    <?php if($pluck['Bienevaluar'][0]->estatus != 'A' && $pluck['Bienevaluar'][0]->estatus != 'O' && $pluck['Bienevaluar'][0]->estatus != 'C'):?>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('foliointerno', 'Folio Interno') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-keyboard"></i>
                                        </span>
                                    </div>
                                        {{
                                            Form::text(
                                                'foliointerno',
                                                $pluck['Bienevaluar'][0]->foliointerno,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'foliointerno',
                                                    'disabled'=>true,
                                                ]
                                            )
                                        }}
                                </div>
                            </div> 
                        </div>
                    <?php endif;?>
                <?php endif;?>
                <?php if(isset($pluck['Bienevaluar'])):?>
                    <div class="col-md-4 pl-5">
                        <div class="form-group">
                            {{ Form::label('estatusbien', 'Estatus') }}
                            <div class="input-group text-uppercase font-weight-bold h4">
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'A'):?>
                                    Activa
                                <?php endif;?>
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'O'):?>
                                    Cotizada
                                <?php endif;?>
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'U'):?>
                                    Autorizada
                                <?php endif;?>
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'S'):?>
                                    Asignada
                                <?php endif;?>
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'V'):?>
                                    Valuada
                                <?php endif;?>
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'R'):?>
                                    Revisada
                                <?php endif;?>
                                <?php if( $pluck['Bienevaluar'][0]->estatus == 'C'):?>
                                    Cancelada
                                <?php endif;?>
                            </div>
                        </div> 
                    </div>
                <?php endif;?>
            </div>
        </div> 

        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $Solicitud->id>0 ):?>

            <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold data-usuario-sat">
                Datos Usuario SAT
            </div>
            <div class="card-body data-usuario-sat">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('tipousuariosat', 'Tipo Usuario') }}
                            {{
                                Form::text(
                                    'tipousuariosat',
                                    $Solicitud->id>0?$pluck["UsuarioSAT"]->tipousuario:'',
                                    [
                                        'class'=>'form-control no-submit datos-sat',
                                    ]
                                )
                            }}
                            
                        </div> 
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('nombresat', 'Nombre') }}
                            {{
                                Form::text(
                                    'nombresat',
                                    $Solicitud->id>0?$pluck["UsuarioSAT"]->usuariosat:'',
                                    [
                                        'class'=>'form-control no-submit datos-sat',
 
                                    ]
                                )
                            }}
                            
                        </div> 
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('oficinasatusuario', 'Oficina SAT') }}
                            {{
                                Form::text(
                                    'oficinasatusuario',
                                    $Solicitud->id>0?$pluck["UsuarioSAT"]->oficina:'',
                                    [
                                        'class'=>'form-control no-submit datos-sat',
                                    ]
                                )
                            }}
                            
                        </div> 
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('cargosat', 'Cargo') }}
                            {{
                                Form::text(
                                    'cargosat',
                                    $Solicitud->id>0?$pluck["UsuarioSAT"]->cargo:'',
                                    [
                                        'class'=>'form-control no-submit datos-sat',
                                    ]
                                )
                            }}
                            
                        </div> 
                    </div>                    
                </div>
            </div>

        <?php endif;?>
        
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Contribuyente
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group required">
                        {{ Form::label('idtipopersona', 'Tipo Persona') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-layer-group"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'idtipopersona',
                                    $pluck["Tipopersona"],
                                    null,
                                    [
                                        'id'=>'idtipopersona',
                                        'class'=>'form-control',
                                        'required'=>true,
                                        'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required">
                        {{ Form::label('nombre', 'Nombre o Razón Social') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'nombre',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'nombre',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese nombre o razón social',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div> 
                </div>
                <div class="col-md-3">
                    <div class="form-group required">
                        {{ Form::label('rfc', 'RFC') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-id-card"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'rfc',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'rfc',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese RFC',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div>                                                   
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group required">
                        {{ Form::label('telefono', 'Teléfono') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-phone"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'telefono',
                                        null,
                                        [
                                            'class'=>'form-control numero',
                                            'id'=>'telefono',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese télefono',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group required">
                        {{ Form::label('email', 'Correo Electrónico') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-envelope"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'email',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'email',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese Correo Electrónico',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div>                                                   
                </div>
            </div>
        </div>        

        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold div-legal">
            Representante Legal
        </div>
        <div class="card-body div-legal">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group required">
                        {{ Form::label('nombrerepresentantelegal', 'Nombre') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'nombrerepresentantelegal',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'nombrerepresentantelegal',
                                            'placeholder'=>'Ingrese nombre',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group required">
                        {{ Form::label('correorepresentantelegal', 'Correo Electrónico') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-envelope"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'correorepresentantelegal',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'correorepresentantelegal',
                                            'placeholder'=>'Ingrese Correo Electrónico',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div>                                                   
                </div>
            </div>
        </div>

        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Dirección
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group required">
                        {{ Form::label('idestado', 'Estado') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'idestado',
                                    $pluck["Estado"],
                                    null,
                                    [
                                        'id'=>'idestado',
                                        'required'=>true,
                                        'class'=>'form-control',
                                        'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        {{ Form::label('idmunicipio', 'Municipio') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'idmunicipio',
                                    array(),
                                    null,
                                    [
                                        'id'=>'idmunicipio',
                                        'class'=>'form-control',
                                        'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group required">
                        {{ Form::label('cp', 'Código Postal') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'cp',
                                        null,
                                        [
                                            'class'=>'form-control numero',
                                            'id'=>'cp',
                                            'placeholder'=>'Ingrese CP',
                                            'required'=>true,
                                            'maxlength'=>'7',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>
                    </div>                                                   
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group required">
                        {{ Form::label('colonia', 'Colonia') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'colonia',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'colonia',
                                            'placeholder'=>'Ingrese Colonia',
                                            'required'=>true,
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>  
                    </div>                                                 
                </div>
                <div class="col-md-5">
                    <div class="form-group required">
                        {{ Form::label('calle', 'Calle') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'calle',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'calle',
                                            'placeholder'=>'Ingrese Calle',
                                            'required'=>true,
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div>   
                    </div>                                                
                </div>
                <div class="col-md-2">
                    <div class="form-group required">
                        {{ Form::label('numero', 'Número') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                                {{
                                    Form::text(
                                        'numero',
                                        null,
                                        [
                                            'class'=>'form-control numero',
                                            'id'=>'numero',
                                            'placeholder'=>'Número',
                                            'required'=>true,
                                            'maxlength'=>'8',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                        </div> 
                    </div>                                                  
                </div>                
            </div>
        </div>

        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Bien a Evaluar
        </div>
        <div class="card-body">
            <div class="row row-add-bien">
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('addtipoavaluo', 'Tipo de Avalúo') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'addtipoavaluo',
                                    $pluck["Tipoavaluo"],
                                    null,
                                    [
                                        'id'=>'addtipoavaluo',
                                        'required'=>true,
                                        'class'=>'form-control text-small no-submit',
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('addtipobien', 'Tipo de Bien') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'addtipobien',
                                    array(),
                                    null,
                                    [
                                        'id'=>'addtipobien',
                                        'required'=>true,
                                        'class'=>'form-control text-small no-submit',
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pl-1 pr-0">
                    {{ Form::label('adddato', 'Datos') }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-keyboard"></i>
                            </span>
                        </div>
                            {{
                                Form::text(
                                    'adddato',
                                    null,
                                    [
                                        'class'=>'form-control text-small text-center no-submit',
                                        'id'=>'adddato',
                                        'placeholder'=>'Registro Incautor',
                                    ]
                                )
                            }}

                            {{
                                Form::text(
                                    'adddatoadicional',
                                    null,
                                    [
                                        'class'=>'form-control text-small text-center no-submit',
                                        'id'=>'adddatoadicional',
                                        'placeholder'=>'Numero Economico',
                                    ]
                                )
                            }}
                    </div>

                </div>
                <div class="col-md-1">
                    <div class="form-group btn-click">              
                        <label class="font-size-sm">Agregar</label>
                        <button type="button" class="btn btn-dark btn-block btn-add" id="agregar-bien">                              
                            <i class="fa fa-check mr-1"></i>                            
                        </button>
                    </div>
                </div>
            </div>
            <div class="row row-details">
                <div class="col-md-12">
                    <p class="text-muted text-center">Informacion correspondiente a los bienes a evaluar</p>                          
                    <div class="table-responsive">            
                        <table class="table table-bordered table-striped table-vcenter table-avaluo">              
                            <thead>              
                                <tr>                  
                                    <th class="text-center align-middle" style="width: 25%;">Tipo de Avalúo</th>
                                    <th class="text-center align-middle" style="width: 25%;">Tipo de Bien</th>
                                    <th class="text-center align-middle" style="width: 15%;">Datos</th>  
                                    <th class="text-center align-middle th-imagen" style="width: 15%;">Imagen</th>                                    
                                    <th class="text-center th-accion" style="width: 15%;">Accion</th>                                    
                                </tr>              
                            </thead>              
                            <tbody>
                                <?php if(isset($pluck['Bienevaluar'])):?>
                                    @foreach ($pluck['Bienevaluar'] as $bien)

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Valuador' && $bien->idevaluador == Auth::user()->id):?>

                                            <tr class="tr-contenido" style="background-color: #f8f8f8;">
                                                <td class="text-center">
                                                    {{ Form::hidden('idtipoavaluo',$bien->idtipoavaluo,
                                                        [
                                                            'data-name'=>'idtipoavaluo',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ $bien->tipoavaluo->nombre }}
                                                </td>
                                                <td class="text-center">
                                                    {{ Form::hidden('idtipobien',$bien->idtipobien,
                                                        [
                                                            'data-name'=>'idtipobien',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ $bien->tipobien->nombre }}
                                                </td>
                                                <td class="text-justify">
                                                    {{ Form::hidden('dato',$bien->dato,
                                                        [
                                                            'data-name'=>'dato',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ Form::hidden('tipodato',$bien->tipodato,
                                                        [
                                                            'data-name'=>'tipodato',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }}

                                                    <strong>{{ ($bien->tipodato!=null && $bien->tipodato!='')?$bien->tipodato.': ':'' }}</strong>{{ $bien->dato }}

                                                    {{ Form::hidden('datoadicional',$bien->datoadicional,
                                                        [
                                                            'data-name'=>'datoadicional',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ Form::hidden('tipodatoadicional',$bien->tipodatoadicional,
                                                        [
                                                            'data-name'=>'tipodatoadicional',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    <br>
                                                    <strong>{{ ($bien->tipodatoadicional!=null && $bien->tipodatoadicional!='')?$bien->tipodatoadicional.': ':'' }}</strong>{{ $bien->tipodatoadicional }}

                                                </td>
                                                <td class="text-center">
                                                    <?php if( $bien->nombrerutaevidencia != '' && $bien->nombrerutaevidencia != null ):?>
                                                        <a target="_blank" href="{{ route('solicitudes.visualizardocumentosevidencia',$bien->nombrerutaevidencia)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Visualizar">
                                                            <i class="fas fa-search-plus"></i>
                                                        </a>

                                                        <a href="{{ route('solicitudes.descargardocumentosevidencia',$bien->nombrerutaevidencia)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                            <i class="fa fa-download"></i>
                                                        </a>

                                                    <?php endif;?>
                                                </td>
                                                <td class="text-center td-btn-eliminar td-accion-eliminar">
                                                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                                </td>
                                                
                                            </tr>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Revisor' && ($bien->estatus == 'R' || $bien->estatus == 'V' || $bien->estatus == 'S') &&  $bien->estatus != 'N'):?>

                                            <tr class="tr-contenido" style="background-color: #f8f8f8;">
                                                <td class="text-center">
                                                    {{ Form::hidden('idtipoavaluo',$bien->idtipoavaluo,
                                                        [
                                                            'data-name'=>'idtipoavaluo',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ $bien->tipoavaluo->nombre }}
                                                </td>
                                                <td class="text-center">
                                                    {{ Form::hidden('idtipobien',$bien->idtipobien,
                                                        [
                                                            'data-name'=>'idtipobien',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ $bien->tipobien->nombre }}
                                                </td>
                                                <td class="text-justify">
                                                    {{ Form::hidden('dato',$bien->dato,
                                                        [
                                                            'data-name'=>'dato',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ Form::hidden('tipodato',$bien->tipodato,
                                                        [
                                                            'data-name'=>'tipodato',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }}

                                                    <strong>{{ ($bien->tipodato!=null && $bien->tipodato!='')?$bien->tipodato.': ':'' }}</strong>{{ $bien->dato }}

                                                    {{ Form::hidden('datoadicional',$bien->datoadicional,
                                                        [
                                                            'data-name'=>'datoadicional',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ Form::hidden('tipodatoadicional',$bien->tipodatoadicional,
                                                        [
                                                            'data-name'=>'tipodatoadicional',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    <br>
                                                    <strong>{{ ($bien->tipodatoadicional!=null && $bien->tipodatoadicional!='')?$bien->tipodatoadicional.': ':'' }}</strong>{{ $bien->tipodatoadicional }}

                                                </td>
                                                <td class="text-center">
                                                    <?php if( $bien->nombrerutaevidencia != '' && $bien->nombrerutaevidencia != null ):?>
                                                        <a target="_blank" href="{{ route('solicitudes.visualizardocumentosevidencia',$bien->nombrerutaevidencia)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Visualizar">
                                                            <i class="fas fa-search-plus"></i>
                                                        </a>

                                                        <a href="{{ route('solicitudes.descargardocumentosevidencia',$bien->nombrerutaevidencia)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                            <i class="fa fa-download"></i>
                                                        </a>

                                                    <?php endif;?>
                                                </td>
                                                <td class="text-center td-btn-eliminar td-accion-eliminar">
                                                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                                </td>
                                                
                                            </tr>

                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] != 'Valuador' && Auth::user()->tipousuario['nombre'] != 'Revisor'):?>

                                            <tr class="tr-contenido" style="background-color: #f8f8f8;">
                                                {{ Form::hidden('id',$bien->id,
                                                    [
                                                        'data-name'=>'id',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit',
                                                    ])
                                                }}
                                                <td class="text-center">
                                                    {{ Form::hidden('idtipoavaluo',$bien->idtipoavaluo,
                                                        [
                                                            'data-name'=>'idtipoavaluo',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ $bien->tipoavaluo->nombre }}
                                                </td>
                                                <td class="text-center">
                                                    {{ Form::hidden('idtipobien',$bien->idtipobien,
                                                        [
                                                            'data-name'=>'idtipobien',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ $bien->tipobien->nombre }}
                                                </td>
                                                <td class="text-left">
                                                    {{ Form::hidden('dato',$bien->dato,
                                                        [
                                                            'data-name'=>'dato',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ Form::hidden('tipodato',$bien->tipodato,
                                                        [
                                                            'data-name'=>'tipodato',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }}

                                                    <strong>{{ ($bien->tipodato!=null && $bien->tipodato!='')?$bien->tipodato.': ':'' }}</strong>{{ $bien->dato }}

                                                    {{ Form::hidden('datoadicional',$bien->datoadicional,
                                                        [
                                                            'data-name'=>'datoadicional',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    {{ Form::hidden('tipodatoadicional',$bien->tipodatoadicional,
                                                        [
                                                            'data-name'=>'tipodatoadicional',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }} 
                                                    <br>
                                                    <strong>{{ ($bien->tipodatoadicional!=null && $bien->tipodatoadicional!='')?$bien->tipodatoadicional.': ':'' }}</strong>{{ $bien->tipodatoadicional }}

                                                </td>
                                                <td class="text-center">
                                                    <?php if( $bien->nombrerutaevidencia != '' && $bien->nombrerutaevidencia != null ):?>
                                                        <a target="_blank" href="{{ route('solicitudes.visualizardocumentosevidencia',$bien->nombrerutaevidencia)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Visualizar">
                                                            <i class="fas fa-search-plus"></i>
                                                        </a>

                                                        <a href="{{ route('solicitudes.descargardocumentosevidencia',$bien->nombrerutaevidencia)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                            <i class="fa fa-download"></i>
                                                        </a>

                                                    <?php endif;?>
                                                </td>
                                                <td class="text-center td-btn-eliminar td-accion-eliminar" data-id="0">
                                                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                                </td>
                                                
                                            </tr>

                                        <?php endif;?>

                                        <tr class="tr-direccion bg-white" data-id="0">

                                            <td class="align-middle" colspan="2">
                                                {{ Form::label('estadodireccionbien', 'Estado') }}
                                                {{
                                                    Form::select(
                                                        'idestadobien',
                                                        $pluck["Estado"],
                                                        $bien->idestado,
                                                        [
                                                            'id'=>'idestadobien',
                                                            'class'=>'form-control text-small no-submit estado-generado',
                                                            'data-name'=>'idestado',
                                                            'data-role'=>'Bienevaluar',
                                                            'data-id'=>$bien->id,
                                                        ]
                                                    )
                                                }}
                                                <div class="row mt-2">
                                                    <div class="col-md-8">
                                                        {{ Form::label('municipiodireccionbien', 'Municipio') }}
                                                        {{
                                                            Form::select(
                                                                'idmunicipiobien',
                                                                array(),
                                                                null,
                                                                [
                                                                    'id'=>'idmunicipio'.$bien->id,
                                                                    'class'=>'form-control text-small no-submit',
                                                                    'data-name'=>'idmunicipio',
                                                                    'data-role'=>'Bienevaluar',
                                                                ]
                                                            )
                                                        }}
                                                    </div>
                                                    <div class="col-md-4">
                                                        {{ Form::label('cpdireccionbien', 'Código Postal') }}
                                                        {{
                                                            Form::text(
                                                                'cp',
                                                                $bien->cp,
                                                                [
                                                                    'class'=>'form-control text-small no-submit',
                                                                    'id'=>'cp',
                                                                    'placeholder'=>'Código Postal',
                                                                    'data-name'=>'cp',
                                                                    'data-role'=>'Bienevaluar',
                                                                ]
                                                            )
                                                        }}
                                                    </div>
                                                </div>                                                
                                            </td>
                                            <td colspan="2" class="align-middle coloniadireccionbien">
                                                {{ Form::label('coloniadireccionbien', 'Colonia') }}
                                                {{
                                                    Form::text(
                                                        'colonia',
                                                        $bien->colonia,
                                                        [
                                                            'class'=>'form-control text-small no-submit',
                                                            'id'=>'colonia',
                                                            'placeholder'=>'Colonia',
                                                            'data-name'=>'colonia',
                                                            'data-role'=>'Bienevaluar',
                                                        ]
                                                    )
                                                }}
                                                <div class="row mt-2">
                                                    <div class="col-md-8">
                                                        {{ Form::label('calledireccionbien', 'Calle') }}
                                                        {{
                                                            Form::text(
                                                                'calle',
                                                                $bien->calle,
                                                                [
                                                                    'class'=>'form-control text-small no-submit',
                                                                    'id'=>'calle',
                                                                    'placeholder'=>'Calle',
                                                                    'data-name'=>'calle',
                                                                    'data-role'=>'Bienevaluar',
                                                                ]
                                                            )
                                                        }}
                                                    </div>
                                                    <div class="col-md-4">
                                                        {{ Form::label('numerodireccionbien', 'Número') }}
                                                        {{
                                                            Form::text(
                                                                'numero',
                                                                $bien->numero,
                                                                [
                                                                    'class'=>'form-control text-small no-submit',
                                                                    'id'=>'numero',
                                                                    'placeholder'=>'Número',
                                                                    'data-name'=>'numero',
                                                                    'data-role'=>'Bienevaluar',
                                                                ]
                                                            )
                                                        }}
                                                    </div>
                                                </div>
                                                
                                            </td>

                                        </tr>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && ($bien->estatus == 'U' || $bien->estatus == 'S') && Auth::user()->id == $Solicitud->idusersolicitante  ):?>
                                           <?php if( $bien->estatus == 'U'):?>
                                            <tr class="tr-autorizacion bg-white text-center">
                                                <td colspan="4">
                                                    <strong>{{ $bien->estatus=='U'?'AUTORIZADA: A la espera de asignacion de Revisor y Valuador':'NO AUTORIZADA: No se asignara Revisor ni Valuador' }}</strong>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                        <?php endif;?>  

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && ($bien->estatus == 'U' || $bien->estatus == 'S') && Auth::user()->id != $Solicitud->idusersolicitante && Auth::user()->id != $Solicitud->idusercotizante  ):?>
                                           <?php if( $bien->estatus == 'U'):?>
                                            <tr class="tr-autorizacion bg-white text-center">
                                                <td colspan="4">
                                                    <strong>{{ $bien->estatus=='U'?'AUTORIZADA: A la espera de asignacion de Revisor y Valuador':'NO AUTORIZADA: No se asignara Revisor ni Valuador' }}</strong>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                        <?php endif;?>  



                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' ):?>
                                            <?php if( $bien->estatus == 'S'):?>
                                            <tr class="tr-cotizacion bg-white text-center">
                                                <td colspan="4">
                                                    <strong>{{ $bien->estatus=='S'?'AUTORIZADA':'NO AUTORIZADA' }}</strong>
                                                </td>
                                            </tr>
                                            <?php endif;?>



                                            <?php if( Auth::user()->id != $Solicitud->idusersolicitante ):?>


                                            <tr class="tr-cotizacion bg-white">
                                                
                                                {{ Form::hidden('idusercotizante',$bien->estatus == 'A'?Auth::user()->id:$bien->idusercotizante,
                                                    [
                                                        'data-name'=>'idusercotizante',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit',
                                                    ])
                                                }}
                                                <td>
                                                    {{ Form::label('importe', 'Importe Avalúo') }}
                                                    {{
                                                        Form::text(
                                                            'importe',
                                                            $bien->importe,
                                                            [
                                                                'class'=>'form-control no-submit text-small text text-right moneda numero',
                                                                'placeholder'=>'Ingrese importe avalúo',
                                                                'data-name'=>'importe',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                                <td colspan="4">
                                                    {{ Form::label('bienobservaciones', 'Observaciones') }}
                                                    {{
                                                        Form::text(
                                                            'bienobservaciones',
                                                            $bien->bienobservaciones,
                                                            [
                                                                'class'=>'form-control no-submit text-small',
                                                                'id'=>'bienobservaciones',
                                                                'placeholder'=>'Observaciones',
                                                                'maxlength'=>'150',
                                                                'data-name'=>'bienobservaciones',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                            </tr>

                                            <?php endif;?>

                                            <?php if( Auth::user()->id == $Solicitud->idusersolicitante && $pluck["ubicacion"] == 'Mostrar' ):?>

                                            <tr class="tr-autorizacion bg-white">
                                                <td>
                                                    {{ Form::hidden('id',$bien->id,
                                                        [
                                                            'data-name'=>'id',
                                                            'data-role'=>'Bienevaluar',
                                                            'class'=>'no-submit',
                                                        ])
                                                    }}
                                                    {{ Form::label('importe', 'Importe Avalúo') }}
                                                    {{
                                                        Form::text(
                                                            'importe',
                                                            $bien->importe,
                                                            [
                                                                'class'=>'form-control no-submit text-small text-right moneda numero',
                                                                'placeholder'=>'Esperando cotización',
                                                                'data-name'=>'importe',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                                <td class="td-bienobservaciones" colspan="4">
                                                    {{ Form::label('bienobservaciones', 'Observaciones') }}
                                                    {{
                                                        Form::text(
                                                            'bienobservaciones',
                                                            $bien->bienobservaciones,
                                                            [
                                                                'class'=>'form-control no-submit text-small',
                                                                'id'=>'bienobservaciones',
                                                                'placeholder'=>'Observaciones',
                                                                'maxlength'=>'150',
                                                                'data-name'=>'bienobservaciones',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                            </tr>

                                            <?php endif;?>


                                            <?php if( $bien->estatus == 'S'):?>
                                            <tr class="tr-cotizacion bg-white">
                                                <td class="text-center p-0" colspan="5">
                                                    <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                        <thead>
                                                            <tr class="text-center" style="background-color: #f8f8f8;">
                                                                <td>Revisor</td>
                                                                <td>Valuador</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="bg-white">
                                                                <td>
                                                                    @foreach ($pluck['Revisor'] as $revisor => $value)
                                                                        <?php if( $bien->idrevisor == $revisor ):?>
                                                                            {{ $value }}
                                                                        <?php endif;?>                                                 
                                                                    @endforeach 
                                                                </td>
                                                                <td>
                                                                    @foreach ($pluck['Valuador'] as $valuador => $value)
                                                                        <?php if( $bien->idevaluador == $valuador ):?>
                                                                            {{ $value }}
                                                                        <?php endif;?>                                                 
                                                                    @endforeach 
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>                                               
                                            </tr>
                                            <?php endif;?>                                             
                                        <?php endif;?> 

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $bien->estatus == 'U' && Auth::user()->id != $Solicitud->idusersolicitante && Auth::user()->id == $Solicitud->idusercotizante  ):?>
                                            <tr class="tr-asignacion bg-white">
                                                {{ Form::hidden('id',$bien->id,
                                                    [
                                                        'data-name'=>'id',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit select-asignacion',
                                                    ])
                                                }}
                                                <td>
                                                    {{ Form::label('idrevisor', 'Revisor') }}
                                                    {{
                                                        Form::select(
                                                            'idrevisor',
                                                            $bien->estatus=='U'?$pluck["Revisor"]:array(),
                                                            null,
                                                            [
                                                                'class'=>'form-control text-small no-submit select-asignacion',
                                                                'data-name'=>'idrevisor',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                                <td>
                                                    {{ Form::label('idevaluador', 'Valuador') }}
                                                    {{
                                                        Form::select(
                                                            'idevaluador',
                                                            $bien->estatus=='U'?$pluck["Valuador"]:array(),
                                                            null,
                                                            [
                                                                'class'=>'form-control text-small no-submit select-asignacion',
                                                                'data-name'=>'idevaluador',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>                                               
                                                <td class="text-center align-middle" colspan="2">
                                                    <strong>
                                                        {{ $bien->estatus=='U'?'AUTORIZADA':'NO AUTORIZADA' }}
                                                    </strong>
                                                </td>
                                            </tr>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT' ):?>
                                            <?php if( $bien->estatus == 'U'):?>
                                            <tr class="tr-autorizacion bg-white text-center">
                                                <td colspan="4">
                                                    <strong>{{ $bien->estatus=='U'?'AUTORIZADA: A la espera de asignacion de Revisor y Valuador':'NO AUTORIZADA: No se asignara Revisor ni Valuador' }}</strong>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                            <?php if( $bien->estatus == 'S'):?>
                                            <tr class="tr-cotizacion bg-white text-center">
                                                <td colspan="4">
                                                    <strong>{{ $bien->estatus=='S'?'AUTORIZADA':'NO AUTORIZADA' }}</strong>
                                                </td>
                                            </tr>
                                            <?php endif;?>

                                            <!-- ACA LE QUITE TR-CONTENIDO PARA PODER ELIMINAR -->
                                             <?php if( $pluck["ubicacion"] == 'Mostrar' ):?>

                                               <tr class="tr-autorizacion bg-white">
                                                    <td>
                                                        {{ Form::hidden('id',$bien->id,
                                                            [
                                                                'data-name'=>'id',
                                                                'data-role'=>'Bienevaluar',
                                                                'class'=>'no-submit',
                                                            ])
                                                        }}
                                                        {{ Form::label('importe', 'Importe Avalúo') }}
                                                        {{
                                                            Form::text(
                                                                'importe',
                                                                $bien->importe,
                                                                [
                                                                    'class'=>'form-control no-submit text-small text-right moneda numero',
                                                                    'placeholder'=>'Esperando cotización',
                                                                    'data-name'=>'importe',
                                                                    'data-role'=>'Bienevaluar',
                                                                ]
                                                            )
                                                        }}
                                                    </td>
                                                    <td class="td-bienobservaciones" colspan="4">
                                                        {{ Form::label('bienobservaciones', 'Observaciones') }}
                                                        {{
                                                            Form::text(
                                                                'bienobservaciones',
                                                                $bien->bienobservaciones,
                                                                [
                                                                    'class'=>'form-control no-submit text-small',
                                                                    'id'=>'bienobservaciones',
                                                                    'placeholder'=>'Observaciones',
                                                                    'maxlength'=>'150',
                                                                    'data-name'=>'bienobservaciones',
                                                                    'data-role'=>'Bienevaluar',
                                                                ]
                                                            )
                                                        }}
                                                    </td>
                                                </tr>

                                            <?php endif;?>
                                            


                                            <?php if( $bien->estatus == 'S'):?>
                                            <tr class="tr-autorizacion bg-white">
                                                <td class="text-center p-0" colspan="4">
                                                    <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                        <thead>
                                                            <tr class="text-center" style="background-color: #f8f8f8;">
                                                                <td>Revisor</td>
                                                                <td>Valuador</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="bg-white">
                                                                <td>
                                                                    @foreach ($pluck['Revisor'] as $revisor => $value)
                                                                        <?php if( $bien->idrevisor == $revisor ):?>
                                                                            {{ $value }}
                                                                        <?php endif;?>                                                 
                                                                    @endforeach 
                                                                </td>
                                                                <td>
                                                                    @foreach ($pluck['Valuador'] as $valuador => $value)
                                                                        <?php if( $bien->idevaluador == $valuador ):?>
                                                                            {{ $value }}
                                                                        <?php endif;?>                                                 
                                                                    @endforeach 
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>                                              
                                            </tr>
                                            <?php endif;?>                                            
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Valuador' &&  $bien->idevaluador == Auth::user()->id):?>
                                            <tr class="tr-valuar bg-white">
                                                {{ Form::hidden('id',$bien->id,
                                                    [
                                                        'data-name'=>'id',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit input-valuar',
                                                    ])
                                                }}
                                                {{ Form::hidden('idrevisor',$bien->idrevisor,
                                                    [
                                                        'data-name'=>'idrevisor',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit input-valuar',
                                                    ])
                                                }}
                                                {{ Form::hidden('idevaluador',$bien->idevaluador,
                                                    [
                                                        'data-name'=>'idevaluador',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit input-valuar',
                                                    ])
                                                }}
                                                <td>
                                                    {{ Form::label('valorbien', 'Valor del Bien') }}
                                                    {{
                                                        Form::text(
                                                            'valorbien',
                                                            $bien->valorbien,
                                                            [
                                                                'class'=>'form-control no-submit text-small input-valuar text-right moneda numero',
                                                                'placeholder'=>'Ingrese valor del bien',
                                                                'data-name'=>'valorbien',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                                <td colspan="3">
                                                    {{ Form::label('avaluoobservaciones', 'Observaciones del Avalúo') }}
                                                    {{
                                                        Form::text(
                                                            'avaluoobservaciones',
                                                            $bien->avaluoobservaciones,
                                                            [
                                                                'class'=>'form-control no-submit text-small input-valuar',
                                                                'placeholder'=>'Ingrese observaciones',
                                                                'maxlength'=>'150',
                                                                'data-name'=>'avaluoobservaciones',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                            </tr>
                                            <?php if( $bien->estatus != 'R' ):?>
                                            <tr class="bg-white">
                                                <td colspan="4">
                                                    <div class="form-group{{ $errors->has('documentopdf') ? ' has-error' : '' }} pt-1">
                                                        <input required id="documentopdf" name="documentopdf" type="file" class="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione una imagen" data-allowed-file-extensions='["pdf"]'>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Revisor' &&  $bien->estatus != 'N' ):?>
                                            <tr class="tr-valuar bg-white">
                                                {{ Form::hidden('id',$bien->id,
                                                    [
                                                        'data-name'=>'id',
                                                        'data-role'=>'Bienevaluar',
                                                        'class'=>'no-submit input-valuar',
                                                    ])
                                                }}
                                                <td>
                                                    {{ Form::label('valorbien', 'Valor del Bien') }}
                                                    {{
                                                        Form::text(
                                                            'valorbien',
                                                            $bien->valorbien,
                                                            [
                                                                'class'=>'form-control no-submit text-small input-valuar text-right moneda input-valor numero',
                                                                'placeholder'=>($bien->valorbien != '' && $bien->valorbien != null)?$bien->valorbien:'A la espera de evaluación',
                                                                'data-name'=>'valorbien',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                                <td colspan="3">
                                                    {{ Form::label('avaluoobservaciones', 'Observaciones del Avalúo') }}
                                                    {{
                                                        Form::text(
                                                            'avaluoobservaciones',
                                                            $bien->avaluoobservaciones,
                                                            [
                                                                'class'=>'form-control no-submit text-small input-valuar',
                                                                'maxlength'=>'150',
                                                                'data-name'=>'avaluoobservaciones',
                                                                'data-role'=>'Bienevaluar',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                            </tr>
                                            <?php if( $bien->estatus == 'V' ):?>
                                            <tr class="bg-white text-center">
                                                <td colspan="5" class="text-center">
                                                    <a href="{{ route('solicitudes.pdf',$bien->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                        <i class="far fa-file-pdf pr-2"></i>
                                                        <strong>Descargar Documento de Avalúo</strong>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                        <?php endif;?>


                                        <?php if( $bien->estatus == 'R' ):?>

                                            <?php if( Auth::user()->tipousuario['nombre'] != 'Revisor' && Auth::user()->tipousuario['nombre'] != 'Valuador' ):?>
                                            <tr class="tr-valuar bg-white">
                                                <td>
                                                    {{ Form::label('valorbien', 'Valor del Bien') }}
                                                    {{
                                                        Form::text(
                                                            'valorbien',
                                                            $bien->valorbien,
                                                            [
                                                                'class'=>'form-control no-submit text-small input-valuar text-right moneda input-valor numero',
                                                                'data-name'=>'valorbien',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                                <td colspan="3">
                                                    {{ Form::label('avaluoobservaciones', 'Observaciones del Avalúo') }}
                                                    {{
                                                        Form::text(
                                                            'avaluoobservaciones',
                                                            $bien->avaluoobservaciones,
                                                            [
                                                                'class'=>'form-control no-submit text-small input-valuar',
                                                                'maxlength'=>'150',
                                                                'data-name'=>'avaluoobservaciones',
                                                            ]
                                                        )
                                                    }}
                                                </td>
                                            </tr>
                                            <?php endif;?>

                                            <tr class="bg-white text-center">
                                                <td colspan="5" class="text-center">
                                                    <a href="{{ route('solicitudes.pdf',$bien->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                        <i class="far fa-file-pdf pr-2"></i>
                                                        <strong>Descargar Documento de Avalúo</strong>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endif;?>

                                    @endforeach
                                <?php endif;?>
                                <?php if(!isset($pluck['Bienevaluar'])):?>
                                    <tr>            
                                        <td colspan="5">
                                            <p class="text-muted text-center pt-4">No hay informacion registrada</p>                
                                        </td>
                                    </tr>
                                <?php endif;?>                             
                            </tbody>              
                        </table>          
                    </div>
                </div>    
            </div> 
        </div>

        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Documento
        </div>
        <div class="card-body">
            <div class="row row-add-documento-bien">
                <div class="col-md-10 pl-1 pr-0">
                    {{ Form::label('addnombre', 'Nombre') }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-keyboard"></i>
                            </span>
                        </div>
                            {{
                                Form::text(
                                    'addnombre',
                                    null,
                                    [
                                        'class'=>'form-control text-small no-submit',
                                        'id'=>'addnombre',
                                        'placeholder'=>'Nombre del Documento',
                                    ]
                                )
                            }}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group btn-click">              
                        <label class="font-size-sm">Agregar</label>
                        <button type="button" class="btn btn-dark btn-block btn-add" id="agregar-documento-bien">                              
                            <i class="fa fa-check mr-1"></i>                            
                        </button>
                    </div>
                </div>
            </div>
            <div class="row row-details">
                <div class="col-md-12">
                    <p class="text-muted text-center">Informacion correspondiente a los documentos de la solicitud</p>                          
                    <div class="table-responsive">            
                        <table class="table table-bordered table-striped table-vcenter table-documento-solicitud">              
                            <thead>              
                                <tr>                  
                                    <th class="text-center align-middle">Nombre</th>
                                    <th class="text-center align-middle">Imagen</th>
                                    <th class="text-center">Accion</th>                                    
                                </tr>              
                            </thead>              
                            <tbody>
                                <?php if(isset($pluck['DocumentoSolicitud'])):?>
                                    @foreach ($pluck['DocumentoSolicitud'] as $documentosolicitud)
                                        <tr class="tr-contenido">
                                            <td class="text-center">
                                                {{ $documentosolicitud->nombre }}
                                            </td>

                                            <td class="text-center">      
                                                <?php if( $documentosolicitud->nombreruta != '' && $documentosolicitud->nombreruta != null ):?>
                                                <a target="_blank" href="{{ route('solicitudes.visualizardocumentossolicitud',$documentosolicitud->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Visualizar">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>

                                                <a href="{{ route('solicitudes.descargardocumentossolicitud',$documentosolicitud->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                    <i class="fa fa-download"></i>
                                                </a>

                                                <?php endif;?>

                                            </td>
                                            <td class="text-center">
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                <?php endif;?>
                                <?php if(!isset($pluck['DocumentoSolicitud'])):?>
                                    <tr>            
                                        <td colspan="3">
                                            <p class="text-muted text-center pt-4">No hay informacion registrada</p>                
                                        </td>
                                    </tr>
                                <?php endif;?> 
                            </tbody>              
                        </table>          
                    </div>
                </div>    
            </div> 
        </div>

        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Observaciones
        </div>
        <div class="card-body">
            <div class="row row-details">
                <div class="col-md-6 col-observaciones">
                    <div class="form-group {{ $errors->has('observaciones') ? ' has-error' : '' }}">
                        {{ Form::label('observaciones', 'Observaciones') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                            {{
                                Form::textarea(
                                    'observaciones',
                                    null,
                                    [
                                        'class'=>'form-control',
                                        'id'=>'observaciones',
                                        'placeholder'=>'Ingrese Observaciones',
                                        'minlength'=>'6',
                                        'maxlength'=>'255',
                                        'rows'=>4,
                                        'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('observaciones'))
                            <span class="help-block">
                                <strong>{{ $errors->first('observaciones') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <?php if(isset($pluck['Bienevaluar'])):?>
                    <?php if( $pluck['Bienevaluar'][0]->estatus == 'V' || $pluck['Bienevaluar'][0]->estatus == 'S' || $pluck['Bienevaluar'][0]->estatus == 'R' ):?>

                    {{ Form::hidden('id',$pluck['Bienevaluar'][0]->id,
                        [
                            'data-name'=>'id',
                            'data-role'=>'Bienevaluar',
                            'class'=>'no-submit input-aprobar',
                        ])
                    }}

                    <div class="col-md-6 col-observacionesaprobacion">
                        <div class="form-group {{ $errors->has('observaciones') ? ' has-error' : '' }}">
                            {{ Form::label('aprobacionbservaciones', 'Observaciones aprobación') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-keyboard"></i>
                                    </span>
                                </div>
                                {{
                                    Form::textarea(
                                        'aprobacionbservaciones',
                                        $pluck['Bienevaluar'][0]->aprobacionbservaciones,
                                        [
                                            'class'=>'form-control no-submit input-aprobar',
                                            'id'=>'aprobacionbservaciones',
                                            'placeholder'=>'Ingrese Observaciones',
                                            'minlength'=>'6',
                                            'maxlength'=>'255',
                                            'rows'=>4,
                                            'data-name'=>'aprobacionbservaciones',
                                            'data-role'=>'Bienevaluar',
                                            'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                        ]
                                    )
                                }}
                            </div>
                            @if ($errors->has('observaciones'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('observaciones') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <?php endif;?>
                <?php endif;?>

            </div>           
        </div> 

        <?php if(isset($pluck['Bienevaluar'])):?>
            <?php if( $pluck['Bienevaluar'][0]->estatus == 'R' ):?>

            <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
                Detalles
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-muted text-center">Informacion correspondiente a los movimientos de la solicitud de Avalúo</p>                          
                        <div class="table-responsive">            
                            <table class="table table-bordered table-vcenter">              
                                <thead>              
                                    <tr>                  
                                        <th class="text-center" style="width: 15%;">Movimiento</th>
                                        <th class="text-center" style="width: 15%;">Fecha</th>
                                        <th class="text-center" style="width: 70%;">Detalles</th>
                                    </tr>              
                                </thead>              
                                <tbody>
                                    <tr>
                                        <td class="text-center text-uppercase btn-dark font-weight-bold align-middle">Creada</td>
                                        <td class="text-center align-middle">{{ $pluck['Detallesolicitud'][0]->fecha }}</td>
                                        <td class="p-0">
                                            <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                <thead>
                                                    <tr class="text-center" style="background-color: #f8f8f8;"><td>Observaciones</td></tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td>{{ $Solicitud->observaciones }}</td></tr> 
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td class="text-center text-uppercase btn-dark font-weight-bold align-middle">Cotizada</td>
                                        <td class="text-center align-middle">{{ $pluck['Detallesolicitud'][1]->fecha }}</td>
                                        <td class="p-0">
                                            <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                <thead>
                                                    <tr class="text-center" style="background-color: #f8f8f8;">
                                                        <td>Bien a Cotizar</td>
                                                        <td>Cotización</td>
                                                        <td>Obervación</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($pluck['Bienevaluar'] as $bien)

                                                        <tr>
                                                            <td>{{$bien->tipobien->nombre }}</td>
                                                            <td>{{ $bien->importe }}</td>
                                                            <td>{{ $bien->bienobservaciones }}</td>
                                                        </tr> 

                                                    @endforeach                   
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center text-uppercase btn-dark font-weight-bold align-middle">Autorizada</td>
                                        <td class="text-center align-middle">{{ $pluck['Detallesolicitud'][2]->fecha }}</td>
                                        <td class="p-0">
                                            <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                <thead>
                                                    <tr class="text-center" style="background-color: #f8f8f8;">
                                                        <td>Bien a Autorizar</td>
                                                        <td>Autorizada</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($pluck['Bienevaluar'] as $bien)

                                                        <tr>
                                                            <td>{{$bien->tipobien->nombre }}</td>
                                                            <td>{{ $bien->estatus=='N'?'NO':'SI' }}</td>
                                                        </tr> 

                                                    @endforeach                   
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="text-center text-uppercase btn-dark font-weight-bold align-middle">Asignada</td>
                                        <td class="text-center align-middle">{{ $pluck['Detallesolicitud'][3]->fecha }}</td>
                                        <td class="p-0">
                                            <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                <thead>
                                                    <tr class="text-center" style="background-color: #f8f8f8;">
                                                        <td>Bien a Asignar</td>
                                                        <td>Valuador</td>
                                                        <td>Revisor</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($pluck['Bienevaluar'] as $bien)
                                                        <?php if( $bien->estatus != 'N' ):?>
                                                            <tr>
                                                                <td>{{$bien->tipobien->nombre }}</td>
                                                                @foreach ($pluck['Valuador'] as $valuador => $value)
                                                                    <?php if( $bien->idevaluador == $valuador ):?>
                                                                        <td>{{ $value }}</td>
                                                                    <?php endif;?>                                                 
                                                                @endforeach
                                                                @foreach ($pluck['Revisor'] as $revisor => $value)
                                                                    <?php if( $bien->idrevisor == $revisor ):?>
                                                                        <td>{{ $value }}</td>
                                                                    <?php endif;?>                                                 
                                                                @endforeach
                                                            </tr> 
                                                        <?php endif;?>
                                                    @endforeach                   
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="text-center text-uppercase btn-dark font-weight-bold align-middle">Valuada</td>
                                        <td class="text-center align-middle">{{$pluck['Detallesolicitud'][3]->fecha}}</td>
                                        <td class="p-0">
                                            <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                <thead>
                                                    <tr class="text-center" style="background-color: #f8f8f8;">
                                                        <td>Bien a Valuar</td>
                                                        <td>Valuador</td>
                                                        <td>Valor del Bien</td>
                                                        <td>Observaciones</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($pluck['Bienevaluar'] as $bien)
                                                        <?php if( $bien->estatus != 'N' ):?>
                                                            <tr>
                                                                <td>{{$bien->tipobien->nombre }}</td>
                                                                @foreach ($pluck['Revisor'] as $revisor => $value)
                                                                    <?php if( $bien->idrevisor == $revisor ):?>
                                                                        <td>{{ $value }}</td>
                                                                    <?php endif;?>                                                 
                                                                @endforeach
                                                                <td>{{$bien->valorbien }}</td>
                                                                <td>{{$bien->avaluoobservaciones }}</td>
                                                            </tr> 
                                                        <?php endif;?>
                                                    @endforeach                   
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="text-center text-uppercase btn-dark font-weight-bold align-middle">Revisada</td>
                                        <td class="text-center align-middle">{{ $pluck['Detallesolicitud'][4]->fecha }}</td>
                                        <td class="p-0">
                                            <table style="width: 100%; border-right: 0px; border-top: 0px;">
                                                <thead>
                                                    <tr class="text-center" style="background-color: #f8f8f8;"><td>Observaciones</td></tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td>{{ $pluck['Bienevaluar'][0]->aprobacionbservaciones }}</td></tr> 
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>                          
                                </tbody>              
                            </table>          
                        </div>
                    </div>    
                </div> 
            </div>

            <?php endif;?>
        <?php endif;?>

        <div class="row mt-2 row-btn mb-4">
            <div class="col-md-12">
                <div class="text-center">
                    <button type="submit" class="btn btn-info font-weight-bold" id="btn-guardar">
                        <li class=" fas fa-file-signature"></li>
                        Guardar
                    </button>
                    <button type="button" class="btn btn-info row-btn-autorizar d-none font-weight-bold" id="row-autorizar">
                        Autorizar Avalúo
                    </button>
                    <button type="button" class="btn btn-danger row-btn-cancelar d-none font-weight-bold" id="btn-cancelar">
                        <li class="far fa-trash-alt"></li>
                        Cancelar Avalúo
                    </button>                    
                </div>
            </div>
        </div>

    </div>
        {{ Form::close() }}
            <!-- END FORMULARIO  USUARIOS -->
            <!-- SWEETALERT2-->
                <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->

            <!-- JQUERY NUMBER-->
                <script src="{{ asset('vendor/jquery-number/jquery.number.min.js') }}"></script>
            <!-- FIN DE JQUERY NUMBER-->


            <script type="text/javascript">

                window.addEventListener("keypress", function(event){
                    if (event.keyCode == 13){
                        event.preventDefault();
                    }
                }, false);

                $(document).ready(function() {

                    //Variable para hacer visible o no el campo acciones de la tabla .table-avaluo
                    var visible=true;
                    var AccionEstatus;
                    
                    $('#addtipoavaluo').change();
                    $('#idtipopersona').change();
                    $('#idestado').change();
                    $('#idestadobien').change();

                    <?php if( $pluck["ubicacion"] == 'Editar'):?>

                        $(".datos-sat").prop('disabled',true); 
                        $('.col-observacionesaprobacion').addClass('d-none');   
                        $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                        $("#observacionesaprobacion").prop('disabled',true);
                        $(".coloniadireccionbien").prop('colspan',3); 

                    <?php endif; ?>

                    <?php if( $pluck["ubicacion"] == 'Editar'):?>

                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador SAT' || '{{Auth::user()->tipousuario["nombre"]}}' == 'Usuario SAT'){

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'A'){

                                $(".tr-autorizacion").find('.no-submit').prop('disabled',true);
                                $(".tr-autorizacion").addClass('d-none');
                                
                            }

                            $('.col-observacionesaprobacion').addClass('d-none');   
                            $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                            $("#observacionesaprobacion").prop('disabled',true); 

                        }

                        $('#span-accion').text('Editar Solicitud SAT');

                    <?php endif; ?>


                    <?php if( $pluck["ubicacion"] == 'Crear'):?>

                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador SAT' || '{{Auth::user()->tipousuario["nombre"]}}' == 'Usuario SAT' || '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador LGC'){

                            $('.col-observacionesaprobacion').addClass('d-none');   
                            $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                            $("#observacionesaprobacion").prop('disabled',true); 

                        }

                        $('#btn-guardar').text('Crear Solicitud');

                        $('#span-accion').text('Crear Solicitud SAT');

                    <?php endif; ?> 

                    <?php if( $pluck["ubicacion"] == 'Mostrar'):?>

                        //$('.th-imagen').addClass('d-none');

                        $('.no-submit').prop('disabled',true);
                        $('.btn-add').prop('disabled',true);
                        $('.td-btn-eliminar').empty();
                        $('.th-accion').addClass('d-none');
                        $('.row-btn').addClass('d-none');
                        $('.td-accion-eliminar').addClass('d-none');
                        //$('.td-accion-autorizar').addClass('d-none');
                        $('.row-add-bien').addClass('d-none');
                        
                        var controlcotizante=false;

                        <?php if( isset($pluck["Bienevaluar"])):?>
                            <?php if( $pluck["Bienevaluar"][0]->idusercotizante == Auth::user()->id):?>

                                controlcotizante=true;

                            <?php endif; ?> 
                        <?php endif; ?> 

                        if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'C'){

                            $('.col-observacionesaprobacion').addClass('d-none');   
                            $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                            $("#observacionesaprobacion").prop('disabled',true); 
                            
                        }

                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador LGC'){
                            //ACA ERA ESTATUS SOLICITUD
                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'A'){

                                $(".tr-cotizacion").find('.no-submit').prop('disabled',false);
                                $('.row-btn').removeClass('d-none');                                
                                AccionEstatus="Cotizar"; 
                                
                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 

                                $('#btn-guardar').text('Cotizar Bien');

                                <?php if( Auth::user()->id == $Solicitud->idusersolicitante ):?>

                                    $('.row-btn').addClass('d-none');          

                                <?php endif;?>
                                                          
                            }

                            //ACA ERA $Solicitud->estatus
                            
                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'O'){

                                if(controlcotizante){

                                    $(".tr-cotizacion").find('.no-submit').prop('disabled',false);
                                    $('.row-btn').removeClass('d-none');                                
                                    AccionEstatus="Cotizar"; 

                                    $('#btn-guardar').text('Cotizar Bien');

                                }
                                

                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 
                                                          
                            }

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'U'){

                                //$(".tr-cotizacion").find('.no-submit').prop('disabled',false);
                                $('.row-btn').removeClass('d-none');
                                $(".select-asignacion ").prop('disabled',false);
                                AccionEstatus="Asignar";

                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 

                                $('#btn-guardar').text('Asignar');

                                if('{{Auth::user()->id}}' == '{{$Solicitud->idusersolicitante}}'){

                                    $('.row-btn').addClass('d-none');

                                }

                                if('{{Auth::user()->id}}' != '{{$pluck["Bienevaluar"][0]->idusercotizante}}'){

                                    $('.row-btn').addClass('d-none');

                                }
                 
                            }

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'S'){

                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 
                 
                            }


                        }

                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador SAT' || '{{Auth::user()->tipousuario["nombre"]}}' == 'Usuario SAT'){

                            // ACA ERA $Solicitud->estatus
                            
                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'O'){

                                //$('.th-accion').removeClass('d-none');
                                
                                $('.th-accion').addClass('d-none');
                                //$('.th-autorizar').removeClass('d-none');

                                //$('.td-accion-autorizar').removeClass('d-none');
                                $(".tr-autorizacion").find('.no-submit').prop('disabled',false);
                                $(".tr-autorizacion").find('.no-submit').prop('readonly',true);
                                
                                //$('.td-autorizar').removeClass('d-none');
                                $('.row-btn').removeClass('d-none');
                                $('.row-btn-cancelar').removeClass('d-none');
                                $('.row-btn-autorizar').removeClass('d-none');
                                $('#btn-guardar').addClass('d-none');
                                AccionEstatus="Autorizar";

                                $('#btn-guardar').text('Autorizar Avalúo');
                                
                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true);
                                
                            }

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'A' || '{{$pluck["Bienevaluar"][0]->estatus}}' == 'U'){
                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 
                            }

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'S'){

                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 
                 
                            }

                        }


                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador LGC' && '{{Auth::user()->id}}' == '{{ $Solicitud->idusersolicitante}}'){

                            // ACA ERA $Solicitud->estatus
                            
                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'O'){

                                //$('.th-accion').removeClass('d-none');
                                
                                $('.th-accion').addClass('d-none');
                                //$('.th-autorizar').removeClass('d-none');

                                //$('.td-accion-autorizar').removeClass('d-none');
                                $(".tr-autorizacion").find('.no-submit').prop('disabled',false);
                                $(".tr-autorizacion").find('.no-submit').prop('readonly',true);
                                //$('.th-accion').text('Autorizar');
                                //$('.td-autorizar').removeClass('d-none');
                                $('.row-btn').removeClass('d-none');
                                $('.row-btn-cancelar').removeClass('d-none');
                                $('.row-btn-autorizar').removeClass('d-none');
                                $('#btn-guardar').addClass('d-none');
                                AccionEstatus="Autorizar";
                                //$(".check-autorizar").prop('disabled',false);


                                $('#btn-guardar').text('Autorizar Avalúo');
                                
                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true);
                                
                            }

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'A' || '{{$pluck["Bienevaluar"][0]->estatus}}' == 'U'){
                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 
                            }

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'S'){

                                $('.col-observacionesaprobacion').addClass('d-none');   
                                $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12'); 
                                $("#observacionesaprobacion").prop('disabled',true); 
                 
                            }

                        }

                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Valuador'){

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'S' || '{{$pluck["Bienevaluar"][0]->estatus}}' == 'V'){

                                $('.row-btn').removeClass('d-none');
                                $(".input-valuar ").prop('disabled',false);
                                AccionEstatus="Valuar"; 

                                $('#btn-guardar').text('Valuar');                               
                            }


                        }


                        if(('{{$Solicitud->estatus}}' == 'S' || '{{$Solicitud->estatus}}' == 'V') && '{{Auth::user()->tipousuario["nombre"]}}' != 'Revisor'){

                            $('.col-observacionesaprobacion').addClass('d-none');   
                            $('.col-observaciones').removeClass('col-md-6').addClass('col-md-12');                       
                        }

                        if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Revisor'){

                            if('{{$pluck["Bienevaluar"][0]->estatus}}' == 'V' || '{{$pluck["Bienevaluar"][0]->estatus}}' == 'S'){

                                $('.row-btn').removeClass('d-none');
                                $("#aprobacionbservaciones").prop('disabled',false);
                                $("#observaciones").prop('disabled',true);
                                AccionEstatus="Aprobar";    

                                $('#btn-guardar').text('Aprobar Avalúo');                     
                            }


                        }

                        $('#span-accion').text('Mostrar Solicitud SAT');

                    <?php endif; ?> 

                    <?php if( $pluck["ubicacion"] == 'Mostrar'):?>

                        $('#btn-cancelar').on('click', function() {

                            $("#_cotizar").prop('disabled',true);
                            $("#_autorizar").prop('disabled',true);
                            $("#_asignar").prop('disabled',true);
                            $("#_valuar").prop('disabled',true);
                            $("#_aprobar").prop('disabled',true);
                            $("#_cancelar").prop('disabled',false);

                            //$('#SolicitudEditForm').submit();

                            Swal.fire({
                                title: '¿Seguro desea eliminar el Avalúo?',
                                text: "Usted esta por eliminar el Avalúo",
                                type: 'warning',
                                showCancelButton: true,
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'No',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: '¡Si, estoy seguro!'
                            }).then((result) => {
                                if (result.value) {

                                    var inputParams = {};
                                    var params = [];
                                    inputParams["Model"] = "Bienevaluar";
                                    inputParams['id'] = "{{ $pluck['Bienevaluar'][0]->id }}";
                                    params.push(inputParams);
                                    json= JSON.stringify(params);                      

                                    $('#_bienevaluar').val(json);
                                    $('.no-submit').prop("disabled",true);
                                    $('.submit').prop("disabled",true); 
                                    $('#SolicitudEditForm').submit();

                                }
                            });


                        });  

                        $('.row-btn-autorizar').on('click', function() {

                            $("#_cotizar").prop('disabled',true);
                            $("#_autorizar").prop('disabled',false);
                            $("#_asignar").prop('disabled',true);
                            $("#_valuar").prop('disabled',true);
                            $("#_aprobar").prop('disabled',true);
                            $("#_cancelar").prop('disabled',true);

                            Swal.fire({
                                title: '¿Seguro desea autorizar el Avalúo?',
                                text: "Usted esta por autorizar el Avalúo",
                                type: 'warning',
                                showCancelButton: true,
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'No',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: '¡Si, estoy seguro!'
                            }).then((result) => {
                                if (result.value) {

                                    var inputParams = {};
                                    var params = [];
                                    inputParams["Model"] = "Bienevaluar";
                                    inputParams['id'] = "{{ $pluck['Bienevaluar'][0]->id }}";
                                    params.push(inputParams);
                                    json= JSON.stringify(params);                      

                                    $('#_bienevaluar').val(json);
                                    $('.no-submit').prop("disabled",true);
                                    $('.submit').prop("disabled",true); 
                                    $('#SolicitudEditForm').submit();

                                }
                            });

                        });

                    <?php endif; ?>   

                    


                    var Model   = new Array("Bienevaluar","Documentosolicitud");
                    var params = [];
                    var paramsTotal=0;
                    var ingresar;

                    //$('#btn-guardar').on('click', function() {

                    $("#SolicitudEditForm").submit(function( event ) {

                        var clickedSubmit = $(this).find('button[type=submit]:focus');
                        $(clickedSubmit).prop('disabled', true);

                        var CampoVacioCotizar=true;
                        var CampoVacioValuar=true;
                        var CampoVacioAutorizar=true;
                        var CampoVacioAprobar=true;
                        var MsgAlert='';

                        $("#_cotizar").prop('disabled',true);
                        //$("#_autorizar").prop('disabled',true);
                        $("#_asignar").prop('disabled',true);
                        $("#_valuar").prop('disabled',true);
                        $("#_aprobar").prop('disabled',true);

                                              
                        
                        if($("#_cancelar").attr('disabled') && $("#_autorizar").attr('disabled'))
                        {

                            if(AccionEstatus == "Valuar"){

                                CampoVacioValuar=true;

                                $(".row-details").find('[data-name=valorbien]').each(function(i, input) {

                                    if(input.value == '' || input.value == null){

                                        CampoVacioValuar=false;
                                        MsgAlert='¡Debe valuar todos los bienes, por favor verifique!';

                                    }

                                });

                                if(CampoVacioValuar){

                                    //$(".row-details").find('[data-role=Bienevaluar]').prop('disabled',false);
                                    $("#_valuar").prop('disabled',false);

                                }

                            }

                            if(AccionEstatus == "Cotizar"){

                                CampoVacioCotizar=true;

                                $(".row-details").find('[data-name=importe]').each(function(i, input) {

                                    if(input.value == '' || input.value == null || input.value == 0 || input.value == '0.00'){

                                        CampoVacioCotizar=false;
                                        MsgAlert='¡Debe cotizar todos los bienes, por favor verifique!';

                                    }

                                });

                                if(CampoVacioCotizar){

                                    $(".row-details").find('[data-role=Bienevaluar]').prop('disabled',false);
                                    $("#_cotizar").prop('disabled',false);

                                }

                            }
                            
                            /*if(AccionEstatus == "Autorizar"){

                                $(".row-details").find('[data-role=Bienevaluar]').prop('disabled',false);
                                $("#_autorizar").prop('disabled',false);

                                CampoVacioAutorizar=false;  


                                /*$(".tr-autorizacion").find('.check-autorizar').each(function(i, input) {

                                    if($(input).is(':checked')){
                                        CampoVacioAutorizar=true;                                    
                                    }

                                });

                                if( '{{Auth::user()->tipousuario["nombre"]}}' == 'Administrador LGC' && '{{Auth::user()->id}}' == '{{ $Solicitud->idusersolicitante}}'){

                                    $(".tr-cotizacion").find('.check-autorizar').each(function(i, input) {

                                        if($(input).is(':checked')){
                                            CampoVacioAutorizar=true;                                    
                                        }

                                    });

                                }*/

                                /*if(!CampoVacioAutorizar){

                                    MsgAlert='¡Debe autorizar al menos un bien a evaluar, por favor verifique! De lo contrario cancele su solicitud';

                                }*/
                            //}
                            


                            if(AccionEstatus == "Asignar"){

                                $("#_asignar").prop('disabled',false);

                            }
                            
                            if(AccionEstatus == "Aprobar"){

                                $("#_aprobar").prop('disabled',false);
                                $("#_bienevaluar").prop('disabled',false);
                                $(".input-aprobar").prop('disabled',false);                            
                                
                                CampoVacioAprobar=true;
                                MsgAlert='¡Debe de estar al menos un bien valuado, por favor espere!';
                                
                                $(".tr-valuar").find('.input-valor').each(function(i, input) {

                                    if(input.value != '' && input.value != null){

                                        CampoVacioAprobar=false;                                    

                                    }

                                });

                                if(CampoVacioAprobar){

                                    event.preventDefault();

                                    Swal.fire({
                                      type: 'warning',
                                      title: '¡Datos incompletos!',
                                      text: MsgAlert
                                    }); 

                                    $(clickedSubmit).prop('disabled', false);

                                }


                            } 

                            if(CampoVacioCotizar && CampoVacioValuar /*&& CampoVacioAutorizar*/ ){

                                if( $('.table-avaluo').find('input[type=hidden]').length != 0){

                                    $.each( Model, function( key, value ) {

                                        paramsTotal = $(".row-details").find('[data-role='+value+']').length;

                                        if( paramsTotal != 0){

                                            var inputParams = {};
                                            inputParams["Model"] = value;
                                            ingresar=true;
                                            var encontro=false;

                                            $(".row-details").find('[data-role='+value+']').each(function(i, input) {

                                                if(!$(input).attr("disabled")){

                                                    if($(input).attr('data-type') == 'Select'){

                                                        $(input).find('option').each(function(){

                                                            if(inputParams[$(input).attr('data-name')] == undefined ){

                                                                inputParams[$(input).attr('data-name')] = $(this).val();
                                                                encontro=true;
                                                            }
                                                            else
                                                            {

                                                                params.push(inputParams);
                                                                inputParams = {};
                                                                inputParams["Model"] = value;
                                                                inputParams[$(input).attr('data-name')] = $(this).val();
                                                                encontro=true;

                                                            }                                                        
                                                            
                                                         });

                                                    }
                                                    else{

                                                        if(inputParams[$(input).attr('data-name')] == undefined ){

                                                            if($(input).attr('data-type') == 'Checkbox'){

                                                                if($(input).is(':checked')){

                                                                    inputParams[$(input).attr('data-name')] = 'U'; 
                                                                    encontro=true;

                                                                }else{

                                                                    inputParams[$(input).attr('data-name')] = 'N';
                                                                    encontro=true; 

                                                                }

                                                            }
                                                            else{

                                                                inputParams[$(input).attr('data-name')] = $(input).val();
                                                                encontro=true;

                                                            }

                                                            
                                                        }
                                                        else
                                                        {                                                        
                                                            params.push(inputParams);
                                                            inputParams = {};
                                                            inputParams["Model"] = value;

                                                            if($(input).attr('data-type') == 'Checkbox'){

                                                                if($(input).is(':checked')){

                                                                    inputParams[$(input).attr('data-name')] = 'U';
                                                                    encontro=true; 

                                                                }else{

                                                                    inputParams[$(input).attr('data-name')] = 'N';
                                                                    encontro=true; 

                                                                }

                                                            }
                                                            else{

                                                                inputParams[$(input).attr('data-name')] = $(input).val();
                                                                encontro=true;
                                                            }

                                                        }

                                                    }

                                                }
                                                else
                                                {   
                                                    ingresar=false;
                                                }                                           
                                              

                                            });

                                            if(ingresar || encontro){

                                                params.push(inputParams);

                                            }                                

                                        }

                                    });

                                    json= JSON.stringify(params);                      

                                    $('#_bienevaluar').val(json);
                                    $('.no-submit').prop("disabled",true);
                                    $('.submit').prop("disabled",true);

                                    //$('#SolicitudEditForm').submit();

                                }
                                else
                                {
                                    event.preventDefault();

                                    Swal.fire({
                                      type: 'warning',
                                      title: '¡Datos incompletos!',
                                      text: '¡Debe ingresar al menos un bien a evaluar, por favor verifique!'
                                    });  

                                    $(clickedSubmit).prop('disabled', false);
                                }

                            }
                            else
                            {

                                event.preventDefault();

                                Swal.fire({
                                  type: 'warning',
                                  title: '¡Datos incompletos!',
                                  text: MsgAlert
                                });  

                                $(clickedSubmit).prop('disabled', false);

                            }

                        }                        
   
                    }); 
            

                });

                //Permitir solo el ingreso de números
                $(".numero").keydown(function(event){

                    if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
                            return false;
                    }
                });

                $('#idtipopersona').on('change',function (e){

                    var idSelect=e.target.value;
                    var nombretipoersona=$('#idtipopersona').find('option[value='+idSelect+']').text();

                    if(nombretipoersona == 'Moral'){

                        $('.div-legal').removeClass('d-none');
                        $('.div-legal').find('input').prop('disabled',false);
                        $('.div-legal').find('input').prop('required',true);                        

                    }
                    else
                    {
                        $('.div-legal').addClass('d-none');
                        $('.div-legal').find('input').prop('disabled',true);
                        $('.div-legal').find('input').prop('required',false);
                        $('.div-legal').find('input').val('');
                    }

                    <?php if( $pluck["ubicacion"] == 'Mostrar'):?>

                        $('.div-legal').find('input').prop('disabled',true);
                        $('.div-legal').find('input').prop('required',false);

                    <?php endif; ?> 


                });

                var variablecontroChange=0;

                $('#addtipobien').on('change',function (e){

                    var idSelect=e.target.value;
                    var tipobien=$('#addtipobien').find('option[value='+idSelect+']').text();

                    variablecontroChange=idSelect;

                    //$('#adddato').removeClass('d-none');
                    //$('#adddatoadicional').removeClass('d-none');

                    //$('#adddato').val('').attr('placeholder','').prop('disabled',false);
                    //$('#adddatoadicional').val('').attr('placeholder','').prop('disabled',false);

                    var nombretipoavaluo=$('#addtipoavaluo option:selected').text();

                    if(nombretipoavaluo == 'Activos Intangibles'){

                        if(tipobien == 'Software' || tipobien == 'Derecho de Autor' || tipobien == 'Obra Literaria' || tipobien == 'Obra de Arte'){

                            $('#adddato').attr('placeholder','Registro indaudor');

                        }
                        else
                        {
                            $('#adddato').attr('placeholder','Registro IMPI');
                        }

                    }

                    if(tipobien == 'Otros'){

                        $('#adddatoadicional').addClass('d-none');
                        $('#adddatoadicional').prop('disabled',true);
                        $('#adddato').attr('placeholder','Otros');
                        $('#adddato').prop('disabled',false);

                    }
                    else
                    {
                        $('#addtipoavaluo').change();
                    }

                });


                /**
                 * Captura el evento change del select #addtipoavaluo y carga por ajax el select #addtipobien
                 * Ademas cambia los placeholder de los input #adddato y #adddatoadicional y los coloca hidden segun sea el caso
                 */
                $('#addtipoavaluo').on('change',function (e) {

                    var idSelect=e.target.value;
                    var caso=$('#addtipoavaluo').find('option[value='+idSelect+']').text();

                    $('#adddato').removeClass('d-none');
                    $('#adddatoadicional').removeClass('d-none');

                    $('#adddato').val('').attr('placeholder','').prop('disabled',false);
                    $('#adddatoadicional').val('').attr('placeholder','').prop('disabled',false); 

                    switch (caso) {
                      case 'Activos Intangibles':
                        
                        $('#adddatoadicional').addClass('d-none');
                        $('#adddatoadicional').prop('disabled',true); 

                        if($('#addtipobien option:selected').text() == 'Software' || $('#addtipobien option:selected').text() == 'Derecho de Autor' || $('#addtipobien option:selected').text() == 'Obra Literaria' || $('#addtipobien option:selected').text() == 'Obra de Arte'){

                            $('#adddato').attr('placeholder','Registro indaudor');

                        }
                        else
                        {
                            $('#adddato').attr('placeholder','Registro IMPI');
                        }

                        break;
                      case "Bienes Muebles":
                        $('#adddato').attr('placeholder','Número Económico');
                        $('#adddatoadicional').addClass('d-none');
                        $('#adddatoadicional').prop('disabled',true); 
                        break;
                      case "Inmuebles Rústicos":
                        $('#adddato').attr('placeholder','Clave catastral');
                        $('#adddatoadicional').attr('placeholder','Folio Mercantil'); 
                        break;
                      case "Inmuebles Urbanos":
                        $('#adddato').attr('placeholder','Clave catastral');
                        $('#adddatoadicional').attr('placeholder','Folio Mercantil'); 
                        break;
                      case "Obras Artísticas":
                        $('#adddatoadicional').addClass('d-none');
                        $('#adddato').prop('disabled',true);
                        $('#adddatoadicional').prop('disabled',true);
                        break;
                    case "Otros":
                        $('#adddatoadicional').addClass('d-none');
                        $('#adddatoadicional').prop('disabled',true);
                        $('#adddato').attr('placeholder','Otros');
                        break;
                    }

                    $.ajax({
                        // la URL para la petición
                        url : "{{ route('solicitudes.buscartipodebien') }}",

                        // la información a enviar
                        // (también es posible utilizar una cadena de datos)
                        data :
                        {
                            id : idSelect,
                            _token : "{{ csrf_token() }}"

                        },

                        // especifica si será una petición POST o GET
                        type : 'POST',

                        // el tipo de información que se espera de respuesta
                        dataType : 'json',

                        // código a ejecutar si la petición es satisfactoria;
                        // la respuesta es pasada como argumento a la función
                        success : function(json) {

                            //Vacio el select #addtipobien
                            $('#addtipobien').empty();

                            //Registro vacio
                            if(Object.keys(json).length == 0){
                          
                            }
                            else
                            {
                                //Recorro el select #addtipobien y añado los options
                                $.each(json, function(index,accountObj){

                                    $('#addtipobien').append('<option value="'+accountObj.id+'">'+accountObj.nombre+'</option>');

                                });

                            }

                            if(variablecontroChange != 0 ){

                                //Selecciono el primer option del select #addtipobien
                                $('#addtipobien').find('option[value='+variablecontroChange+']').prop('selected', true);

                            }
                            else
                            {

                                //Selecciono el primer option del select #addtipobien
                                $('#addtipobien').find('option:eq(0)').prop('selected', true);

                            }
                            

                            variablecontroChange=0;

                        },

                        // código a ejecutar si la petición falla;
                        // son pasados como argumentos a la función
                        // el objeto de la petición en crudo y código de estatus de la petición
                        error : function(xhr, status) {

                        },

                        // código a ejecutar sin importar si la petición falló o no
                        complete : function(xhr, status) {

                        }
                    });


                });

                



                $('#idestado').on('change',function (e) {

                    var idSelect=e.target.value;

                    $.ajax({
                        // la URL para la petición
                        url : "{{ route('solicitudes.buscarmunicipio') }}",

                        // la información a enviar
                        // (también es posible utilizar una cadena de datos)
                        data :
                        {
                            id : idSelect,
                            _token : "{{ csrf_token() }}"

                        },

                        // especifica si será una petición POST o GET
                        type : 'POST',

                        // el tipo de información que se espera de respuesta
                        dataType : 'json',

                        // código a ejecutar si la petición es satisfactoria;
                        // la respuesta es pasada como argumento a la función
                        success : function(json) {

                            //Vacio el select #addtipobien
                            $('#idmunicipio').empty();

                            //Registro vacio
                            if(Object.keys(json).length == 0){
                          
                            }
                            else
                            {
                                //Recorro el select #addtipobien y añado los options
                                $.each(json, function(index,accountObj){

                                    $('#idmunicipio').append('<option value="'+accountObj.id+'">'+accountObj.nombre+'</option>');

                                });

                            }
                                
                            

                            <?php if( $pluck["ubicacion"] == 'Crear'):?>

                                $('#idmunicipio').find('option:eq(0)').prop('selected', true);

                            <?php endif; ?> 

                            <?php if( $pluck["ubicacion"] != 'Crear'):?>

                                $('#idmunicipio').find('option[value={{$Solicitud->idmunicipio}}]').prop('selected', true);

                            <?php endif; ?> 


                        },

                        // código a ejecutar si la petición falla;
                        // son pasados como argumentos a la función
                        // el objeto de la petición en crudo y código de estatus de la petición
                        error : function(xhr, status) {

                        },

                        // código a ejecutar sin importar si la petición falló o no
                        complete : function(xhr, status) {

                        }
                    });


                });



                $('tbody').on('change',".estado-generado",function (e) {
                //$('.estado-generado').on('change',function (e) {

                    var idSelect=e.target.value;
                    var dataId=$(this).attr('data-id');

                    $.ajax({
                        // la URL para la petición
                        url : "{{ route('solicitudes.buscarmunicipio') }}",

                        // la información a enviar
                        // (también es posible utilizar una cadena de datos)
                        data :
                        {
                            id : idSelect,
                            _token : "{{ csrf_token() }}"

                        },

                        // especifica si será una petición POST o GET
                        type : 'POST',

                        // el tipo de información que se espera de respuesta
                        dataType : 'json',

                        // código a ejecutar si la petición es satisfactoria;
                        // la respuesta es pasada como argumento a la función
                        success : function(json) {

                            //Vacio el select #addtipobien
                            $('#idmunicipio'+dataId).empty();

                            //Registro vacio
                            if(Object.keys(json).length == 0){
                          
                            }
                            else
                            {
                                //Recorro el select #addtipobien y añado los options
                                $.each(json, function(index,accountObj){

                                    $('#idmunicipio'+dataId).append('<option value="'+accountObj.id+'">'+accountObj.nombre+'</option>');

                                });

                            }
                                
                            

                            <?php if( $pluck["ubicacion"] == 'Crear'):?>

                                $('#idmunicipio'+dataId).find('option:eq(0)').prop('selected', true);

                            <?php endif; ?> 

                            <?php if( $pluck["ubicacion"] != 'Crear'):?>

                                $('#idmunicipio'+dataId).find('option[value={{$pluck["Bienevaluar"][0]->idmunicipio}}]').prop('selected', true);

                            <?php endif; ?> 


                        },

                        // código a ejecutar si la petición falla;
                        // son pasados como argumentos a la función
                        // el objeto de la petición en crudo y código de estatus de la petición
                        error : function(xhr, status) {

                        },

                        // código a ejecutar sin importar si la petición falló o no
                        complete : function(xhr, status) {

                        }
                    });


                });




                $('#idestadobien').on('change',function (e) {

                    var idSelect=e.target.value;
                    var dataId=$(this).attr('data-id');

                    $.ajax({
                        // la URL para la petición
                        url : "{{ route('solicitudes.buscarmunicipio') }}",

                        // la información a enviar
                        // (también es posible utilizar una cadena de datos)
                        data :
                        {
                            id : idSelect,
                            _token : "{{ csrf_token() }}"

                        },

                        // especifica si será una petición POST o GET
                        type : 'POST',

                        // el tipo de información que se espera de respuesta
                        dataType : 'json',

                        // código a ejecutar si la petición es satisfactoria;
                        // la respuesta es pasada como argumento a la función
                        success : function(json) {

                            //Vacio el select #addtipobien
                            $('#idmunicipio'+dataId).empty();

                            //Registro vacio
                            if(Object.keys(json).length == 0){
                          
                            }
                            else
                            {
                                //Recorro el select #addtipobien y añado los options
                                $.each(json, function(index,accountObj){

                                    $('#idmunicipio'+dataId).append('<option value="'+accountObj.id+'">'+accountObj.nombre+'</option>');

                                });

                            }
                                
                            

                            <?php if( $pluck["ubicacion"] == 'Crear'):?>

                                $('#idmunicipio'+dataId).find('option:eq(0)').prop('selected', true);

                            <?php endif; ?> 

                            <?php if( $pluck["ubicacion"] != 'Crear'):?>

                                $('#idmunicipio'+dataId).find('option[value={{$pluck["Bienevaluar"][0]->idmunicipio}}]').prop('selected', true);

                            <?php endif; ?> 


                        },

                        // código a ejecutar si la petición falla;
                        // son pasados como argumentos a la función
                        // el objeto de la petición en crudo y código de estatus de la petición
                        error : function(xhr, status) {

                        },

                        // código a ejecutar sin importar si la petición falló o no
                        complete : function(xhr, status) {

                        }
                    });


                });


    

                //Si la ubicacion es mostrar no permito la accion al .td-btn-eliminar
                <?php if( $pluck["ubicacion"] != 'Mostrar'):?>

                    $("table.table-avaluo").on("click", ".td-btn-eliminar", function (event) {

                        $(this).closest("tr").remove();    

                        var dataId=$(this).attr('data-id');

                        $("tr[data-id="+dataId+"]").remove();    

                        Contador=$('table.table-avaluo tbody').find('.tr-contenido').length;
                        if(Contador == 0){

                            var NuevaFila = $("<tr>");
                            var Columnas = "";

                            Columnas += 
                                '<td colspan="5">'+
                                '<p class="text-muted text-center pt-4">No hay informacion registrada</p>'+
                                '</td></tr>';

                            NuevaFila.append(Columnas);
                            $("table.table-avaluo tbody").append(NuevaFila);
                        }

                    });

                    $("table.table-documento-solicitud").on("click", ".td-btn-eliminar", function (event) {

                        $(this).closest("tr").remove();    

                        Contador=$('table.table-documento-solicitud tbody').find('.tr-contenido').length;

                        if(Contador == 0){

                            var NuevaFila = $("<tr>");
                            var Columnas = "";

                            Columnas += 
                                '<td colspan="3">'+
                                '<p class="text-muted text-center pt-4">No hay informacion registrada</p>'+
                                '</td></tr>';

                            NuevaFila.append(Columnas);
                            $("table.table-documento-solicitud tbody").append(NuevaFila);
                        }

                    });

                <?php endif; ?> 

                // Variables globales //
                //Contador de los Bienes agregados
                var Contador=0;

                var ContadorTotal=0;

                //Permite agregar un bien a evaluar en la tabla .table-avaluo
                $("div.btn-click").on("click", "#agregar-bien", function (event) {

                    //Consulto la cantidad de row registradas en la tabla .table-avaluo que sean .tr-contenido
                    Contador=$('table.table-avaluo tbody').find('.tr-contenido').length;

                    var NuevaFila = $("<tr class='tr-contenido'>");
                    var Columnas = "";
                    var Eliminar= "fa fa-times-circle fa-2x";

                    var Dato= $('#adddato').val();
                    var Datoadicional= $('#adddatoadicional').val();
                    var Tipoavaluo= $('#addtipoavaluo option:selected').text();
                    var Tipobien= $('#addtipobien option:selected').text();

                    //Condicion que captura si el campo '#adddato esta vacio y no esta disabled (true si la condicion es aceptada, de lo contrario false)
                    var condicion1=true;

                    //Condicion que captura si el campo '#adddatoadicional esta vacio y no esta disabled (true si la condicion es aceptada, de lo contrario false)
                    var condicion2=true;

                    //Si entra es que tiene que se llenado de forma obligatoria y no permitira agregar
                    if(!$('#adddato').attr('disabled') && $('#adddato').val() == ''){

                        condicion1=false;

                    }

                     //Si entra es que tiene que se llenado de forma obligatoria y no permitira agregar
                    if(!$('#adddatoadicional').attr('disabled') && $('#adddatoadicional').val() == ''){

                        condicion2=false;

                    }


                    //Si estas las 2 condiciones true entrara en el if y procedera agregar el row
                    if( condicion1 && condicion2){

                        //Si el contador es 0, limpio el mensaje de vacio sin registros
                        if(Contador == 0){

                            $('table.table-avaluo tbody').empty();

                        }

                        Contador++;
                        ContadorTotal++;


                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Tipoavaluo + '</span>'+
                            '<input class="no-submit" data-name="idtipoavaluo" data-role="Bienevaluar" name="idtipoavaluo" type="hidden" value="'+$("#addtipoavaluo option:selected").val()+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Tipobien + '</span>'+
                            '<input class="no-submit" data-name="idtipobien" data-role="Bienevaluar" name="idtipobien" type="hidden" value="'+$("#addtipobien option:selected").val()+'">'+
                            '</td>';

                        //variable a ser tomada como case en el switch
                        var caso=$('#addtipoavaluo option:selected').text();

                        var labeldato= $('#adddato').attr('placeholder');
                        var labeldatoadicional= $('#adddatoadicional').attr('placeholder');

                        //se inicializan los campos #adddato y #adddatoadicional
                        $('#adddato').val('').attr('placeholder','').prop('disabled',false);
                        $('#adddatoadicional').val('').attr('placeholder','').prop('disabled',false); 

                        switch (caso) {
                          
                          case 'Activos Intangibles':

                            Columnas += 
                            '<td class="text-left">'+
                            '<span class="font-size-md"><strong>' + labeldato +':</strong>  '+ Dato + '</span>'+
                            '<input class="no-submit" data-name="dato" data-role="Bienevaluar" name="dato" type="hidden" value="'+Dato+'">'+
                            '<input class="no-submit" data-name="tipodato" data-role="Bienevaluar" name="tipodato" type="hidden" value="'+labeldato+'">'+
                            '</td>';
                            break;

                          case "Bienes Muebles":

                            Columnas += 
                            '<td class="text-left">'+
                            '<span class="font-size-md"><strong>' + labeldato +':</strong>  '+ Dato + '</span>'+
                            '<input class="no-submit" data-name="dato" data-role="Bienevaluar" name="dato" type="hidden" value="'+Dato+'">'+
                            '<input class="no-submit" data-name="tipodato" data-role="Bienevaluar" name="tipodato" type="hidden" value="'+labeldato+'">'+
                            '</td>';
                            break;

                          case "Inmuebles Rústicos":

                            Columnas += 
                            '<td class="text-left">'+
                            '<span class="font-size-md"><strong>' + labeldato +':</strong>  '+ Dato + '</span>'+
                            '<br>'+
                            '<span class="font-size-md"><strong>' + labeldatoadicional +':</strong>  '+ Datoadicional + '</span>'+
                            '<input class="no-submit" data-name="dato" data-role="Bienevaluar" name="dato" type="hidden" value="'+Dato+'">'+
                            '<input class="no-submit" data-name="datoadicional" data-role="Bienevaluar" name="datoadicional" type="hidden" value="'+Datoadicional+'">'+
                            '<input class="no-submit" data-name="tipodato" data-role="Bienevaluar" name="tipodato" type="hidden" value="'+labeldato+'">'+
                            '<input class="no-submit" data-name="tipodatoadicional" data-role="Bienevaluar" name="tipodatoadicional" type="hidden" value="'+labeldatoadicional+'">'+
                            '</td>';
                            break;

                          case "Inmuebles Urbanos":

                            Columnas += 
                            '<td class="text-left">'+
                            '<span class="font-size-md"><strong>' + labeldato +':</strong>  '+ Dato + '</span>'+
                            '<br>'+
                            '<span class="font-size-md"><strong>' + labeldatoadicional +':</strong>  '+ Datoadicional + '</span>'+
                            '<input class="no-submit" data-name="dato" data-role="Bienevaluar" name="dato" type="hidden" value="'+Dato+'">'+
                            '<input class="no-submit" data-name="datoadicional" data-role="Bienevaluar" name="datoadicional" type="hidden" value="'+Datoadicional+'">'+
                            '<input class="no-submit" data-name="tipodato" data-role="Bienevaluar" name="tipodato" type="hidden" value="'+labeldato+'">'+
                            '<input class="no-submit" data-name="tipodatoadicional" data-role="Bienevaluar" name="tipodatoadicional" type="hidden" value="'+labeldatoadicional+'">'+
                            '</td>';
                            break;

                          case "Obras Artísticas":
                            
                            Columnas += 
                            '<td class="text-left">'+
                            '</td>';
                            break;

                        case "Otros":
                        
                            Columnas += 
                            '<td class="text-left">'+
                            '<span class="font-size-md"><strong>' + labeldato +':</strong>  '+ Dato + '</span>'+
                            '<input class="no-submit" data-name="dato" data-role="Bienevaluar" name="dato" type="hidden" value="'+Dato+'">'+
                            '<input class="no-submit" data-name="tipodato" data-role="Bienevaluar" name="tipodato" type="hidden" value="'+labeldato+'">'+
                            '</td>';
                            break;
                            
                        break;

                        }

                        Columnas += 
                        '<td class="text-center">'+
                            '<input type="file" name="documentoevidencia[]">'+
                        '</td>';

                        Columnas += 
                            '<td class="text-center td-btn-eliminar" data-id="'+ContadorTotal+'"><i class="' + Eliminar + '" aria-hidden="true"></i></td></tr>';

                        NuevaFila.append(Columnas);
                        $("table.table-avaluo tbody").append(NuevaFila);

                        NuevaFila = $("<tr class='tr-direccion' data-id='"+ContadorTotal+"'>");
                        Columnas = "";

                        Columnas += 
                            '<td class="align-middle" colspan="2">'+
                            '<label for="idestado">Estado</label>'+
                            '<br>'+
                            '<select required id="idestado'+ContadorTotal+'" class="form-control no-submit form-control-sm estado-generado" name="idestado" data-name="idestado" data-role="Bienevaluar" data-id="'+ContadorTotal+'">'+
                            '</select>'+
                            '<div class="row mt-2">'+
                            '<div class="col-md-8">'+

                            '<label for="idmunicipio">Municipio</label>'+
                            '<br>'+
                            '<select required id="idmunicipio'+ContadorTotal+'" class="form-control no-submit form-control-sm municipio-generado" name="idmunicipio" data-name="idmunicipio" data-role="Bienevaluar" data-id="'+ContadorTotal+'">'+
                            '</select>'+

                            '</div>'+
                            '<div class="col-md-4">'+
                            '<label for="cp">Código Postal</label>'+
                            '<br>'+
                            '<input required class="no-submit form-control form-control-sm" data-name="cp" data-role="Bienevaluar" name="cp" value="'+$("#cp").val()+'">'+
                            '</div>'+
                            '</div>'+
                            '</td>';

                        Columnas += 
                            '<td colspan="3">'+
                            '<label for="cp">Colonia</label>'+
                            '<br>'+
                            '<input required class="no-submit form-control form-control-sm" data-name="colonia" data-role="Bienevaluar" name="colonia" value="'+$("#colonia").val()+'">'+
                            '<div class="row mt-2">'+
                            '<div class="col-md-8">'+
                            '<label for="cp">Calle</label>'+
                            '<br>'+
                            '<input required class="no-submit form-control form-control-sm" data-name="calle" data-role="Bienevaluar" name="calle" value="'+$("#calle").val()+'">'+
                            '</div>'+
                            '<div class="col-md-4">'+
                            '<label for="cp">Número</label>'+
                            '<br>'+
                            '<input required class="no-submit form-control form-control-sm" data-name="numero" data-role="Bienevaluar" name="numero" value="'+$("#numero").val()+'">'+
                            '</div>'+
                            '</div>'+
                            '</td></tr>';

                        NuevaFila.append(Columnas);
                        $("table.table-avaluo tbody").append(NuevaFila);                    

                        $('#idestado option').clone().appendTo("#idestado"+ContadorTotal);
                        $('#idmunicipio option').clone().appendTo("#idmunicipio"+ContadorTotal);

                        $('#idestado'+ContadorTotal+' option[value="'+$('#idestado option:selected').val()+'"]').prop("selected",true);
                        $('#idmunicipio'+ContadorTotal+' option[value="'+$('#idmunicipio option:selected').val()+'"]').prop("selected",true);

                        //Se inicializa el campo #addtipoavaluo y se genera el evento change al mismo para que cargue nuevamente el campo #addtipobien
                        $('#addtipoavaluo').find('option:eq(0)').prop('selected', true);
                        $('#addtipoavaluo').change();

                    }
                    else
                    {
                        Swal.fire({
                          type: 'warning',
                          title: '¡Datos incompletos!',
                          text: '¡Debe ingresar los datos solicitados, por favor verifique!'
                        });
                    }

                }); 

                

                $("div.btn-click").on("click", "#agregar-documento-bien", function (event) {

                    //Consulto la cantidad de row registradas en la tabla .table-avaluo que sean .tr-contenido
                    Contador=$('table.table-documento-solicitud tbody').find('.tr-contenido').length;

                    var NuevaFila = $("<tr class='tr-contenido'>");
                    var Columnas = "";
                    var Eliminar= "fa fa-times-circle fa-2x";

                    var Nombre= $('#addnombre').val();

                    var condicion=true;

                    if(!$('#addnombre').attr('disabled') && $('#addnombre').val() == ''){

                        condicion=false;

                    }

                    if( condicion ){

                        //Si el contador es 0, limpio el mensaje de vacio sin registros
                        if(Contador == 0){

                            $('table.table-documento-solicitud tbody').empty();

                        }

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Nombre + '</span>'+
                            '<input class="no-submit" data-name="nombre" data-role="Documentosolicitud" name="nombre" type="hidden" value="'+$("#addnombre").val()+'">'+
                            '</td>';

                        Columnas += 
                        '<td class="text-center">'+
                            '<input required type="file" name="documentossolicitud[]">'+
                        '</td>';

                        Columnas += 
                            '<td class="text-center td-btn-eliminar"><i class="' + Eliminar + '" aria-hidden="true"></i></td></tr>';

                        NuevaFila.append(Columnas);
                        $("table.table-documento-solicitud tbody").append(NuevaFila);
                        Contador++;

                        $('#addnombre').val('');                        

                    }
                    else
                    {
                        Swal.fire({
                          type: 'warning',
                          title: '¡Datos incompletos!',
                          text: '¡Debe ingresar los datos solicitados, por favor verifique!'
                        });
                    }

                }); 


                 /*
                 * Funcion que permite darle formato de moneda al input
                 */
                 $('.moneda').on('change',function (e) {

                    var value = $(this).val();
                    value = $.number(value, 2, '.', ',');
                    $(this).val(value);
                      
                 });

                
                    

            </script>

            <script>
                $(".file").fileinput({
                    language: "es",
                    theme: "fas",
                    allowedFileExtensions: ["pdf"],
                    /*maxFileSize: 3072,
                    minImageWidth: 232,
                    minImageHeight: 155,
                    maxImageWidth: 650,
                    maxImageHeight: 433,*/
                    maxFileCount: 1
                });
            </script>
        </div>
    </div>
@endsection