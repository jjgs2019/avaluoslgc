@extends('plantillas.privada')
@section('content')


    <!-- BOOSTRAP DATEPICKER-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
    <script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                language: 'es',
                format: "dd-mm-yyyy",
            });
        });
    </script>

    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('solicitudes_index') }}

    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i>Solicitudes SAT
            </h5>
        </div>
    </div>
    <!-- SOLO PODRAN CREAR SOLICITUDES DE AVALÚOS LOS ADMINISTRADORES SAT Y USUARIOS SAT -->
    <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'  ):?>
        
        <div class="row">
            <div class="col-12">
                <div class="text-right">
                    <a href="{{ route('solicitudes.create')}}">
                        <button type="button" class="btn btn-dark">
                            Nueva Solicitud
                        </button>
                    </a>
                </div>
            </div>
        </div>

    <?php endif; ?>
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <hr>

    <!-- Filtros -->
    <div class="card shadow mb-4">
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Filtros
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-folioexterno', 'Folio Externo') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-folioexterno',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-folioexterno',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '1'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-foliointerno', 'Folio Interno') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-foliointerno',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-foliointerno',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '2'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>                        
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-oficinasat', 'Oficina Sat') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-oficinasat',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-oficinasat',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '3'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-contribuyente', 'Contribuyente') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-contribuyente',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-contribuyente',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '4'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-solicitante', 'Solicitante') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-solicitante',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-solicitante',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '6'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-cotizante', 'Cotizante') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-cotizante',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-cotizante',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '7'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-revisor', 'Revisor') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-revisor',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-revisor',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '10'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-valuador', 'Valuador') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-valuador',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-valuador',
                                        'class'=>'form-control js-select2',
                                        'data-column' => '11'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('fil-estatus', 'Estatus') }}
                        <div class="input-group">
                            {{
                                Form::select(
                                    'fil-estatus',
                                    array(),
                                    null,
                                    [
                                        'id'=>'fil-estatus',
                                        'class'=>'form-control',
                                        'data-column' => '8'
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-header bg-ligth h6 text-uppercase font-weight-bold">
            Fechas
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="d-block">Opciones</label>
                        <div class="custom-control custom-radio custom-control-inline custom-control-primary">
                            <input type="radio" class="custom-control-input" id="example-radio-custom-inline1" name="radiofecha_rango" checked="true">
                            <label class="custom-control-label" for="example-radio-custom-inline1">Cualquier fecha</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline custom-control-primary">
                            <input type="radio" class="custom-control-input" id="example-radio-custom-inline2" name="radiofecha_rango">
                            <label class="custom-control-label" for="example-radio-custom-inline2">Rango de fecha</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-8" id="fechas">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('fecha_inicio', 'Fecha inicial') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    {{
                                        Form::text(
                                            'fecha_inicio',
                                            null,
                                            [
                                                'class'=>'datepicker form-control',
                                                'id'=>'fecha_inicio',
                                                'name'=>'fecha_inicio',
                                                'placeholder'=>'Ingrese fecha inicial',
                                            ]
                                        )
                                    }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" name="fecha_fin">
                                {{ Form::label('fecha_fin', 'Fecha final') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    {{
                                        Form::text(
                                            'fecha_fin',
                                            null,
                                            [
                                                'class'=>'datepicker form-control',
                                                'id'=>'fecha_fin',
                                                'name'=>'fecha_fin',
                                                'placeholder'=>'Ingrese fecha final',
                                            ]
                                        )
                                    }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>               
        </div>
    </div>





    <!-- TABLA DE USUARIOS -->
    <div class="card shadow mb-4">
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Lista de solicitudes
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable-Solicitud" class="table table-bordered table-striped table-hover table-vcenter" style="width: 100%;">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 15%;">Fecha</th>
                            <th class="text-center" style="width: 15%;">Folio Externo</th>
                            <th class="text-center" style="width: 15%;">Folio Interno</th>
                            <th class="text-center" style="width: 15%;">Oficina Sat</th>
                            <th class="text-center" style="width: 15%;">Contribuyente</th>
                            <th class="text-center" style="width: 15%;">Tipo Bien</th>
                            <th class="text-center" style="width: 15%;">Solicitante</th>
                            <th class="text-center" style="width: 15%;">Cotizante</th>
                            <th class="text-center" style="width: 15%;">Valuador</th>
                            <th class="text-center" style="width: 15%;">Revisor</th>
                            <th class="text-center" style="width: 15%;">Estatus</th>                            
                            <th class="text-center">Acción</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($coleccion as $solicitud)
                            <tr>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->fechasolicitud }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->foliosolicitud }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->foliobien }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->oficina }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->contribuyente }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->bien }}
                                </td>
                                <td class="font-w600 text-center">

                                    <?php if( $solicitud->usuariosolicitantesat != '' && $solicitud->usuariosolicitantesat != null):?>
                                        {{ $solicitud->usuariosolicitantesat }}
                                    <?php endif;?>

                                    <?php if( $solicitud->usuariosolicitantelgc != '' && $solicitud->usuariosolicitantelgc != null):?>
                                        {{ $solicitud->usuariosolicitantelgc }}
                                    <?php endif;?>
                                    
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->usuariocotizante }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->valuador }}
                                </td>
                                <td class="font-w600 text-center">
                                    {{ $solicitud->revisor }}
                                </td>
                                <td class="font-w600 text-center">
                                    <?php if( $solicitud->estatusbien == 'A'):?>
                                        Activa
                                    <?php endif;?>
                                    <?php if( $solicitud->estatusbien == 'O'):?>
                                        Cotizada
                                    <?php endif;?>
                                    <?php if( $solicitud->estatusbien == 'U'):?>
                                        Autorizada
                                    <?php endif;?>
                                    <?php if( $solicitud->estatusbien == 'S'):?>
                                        Asignada
                                    <?php endif;?>
                                    <?php if( $solicitud->estatusbien == 'V'):?>
                                        Valuada
                                    <?php endif;?>
                                    <?php if( $solicitud->estatusbien == 'R'):?>
                                        Revisada
                                    <?php endif;?>
                                    <?php if( $solicitud->estatusbien == 'C'):?>
                                        Cancelada
                                    <?php endif;?>
                                </td>                                
                                <td class="text-center">
                                    <div class="btn-group">

                                        <?php if( $solicitud->estatusbien == 'R'):?>
                                     
                                            <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                <i class="fas fa-search-plus"></i>
                                            </a>

                                            <a href="{{ route('solicitudes.pdf',$solicitud->nombreruta)}}" class="btn btn-sm btn-dark ml-2" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                <i class="far fa-file-pdf"></i>
                                            </a>
                                      
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT'):?>
                                            <?php if( $solicitud->estatusbien == 'U'):?>
                                         
                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>
                                          
                                            <?php endif;?>
                                        <?php endif;?>                                  

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'):?>
                                            <?php if( $solicitud->estatusbien == 'A' && Auth::user()->tipousuario['nombre'] != 'Administrador LGC'):?>
                                         
                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>
                                          
                                            <?php endif;?>

                                            <?php if( $solicitud->estatusbien == 'A' && Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && Auth::user()->id == $solicitud->solicitante ):?>
                                         
                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>
                                          
                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' ||Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT'):?>
                                            <?php if( $solicitud->estatusbien == 'S'):?>
                                         
                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>
                                          
                                            <?php endif;?>
                                        <?php endif;?>


                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' ||Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT'):?>
                                            <?php if( $solicitud->estatusbien == 'V'):?>
                                         
                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>
                                          
                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'):?>
                                            <?php if( $solicitud->estatusbien == 'A' && Auth::user()->tipousuario['nombre'] != 'Administrador LGC'):?>
                                         
                                                <a href="{{ route('solicitudes.edit',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-warning ml-1" data-toggle="tooltip" data-placement="top" title="Editar">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                          
                                            <?php endif;?>

                                            <?php if( $solicitud->estatusbien == 'A' && Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && Auth::user()->id == $solicitud->solicitante ):?>
                                         
                                                <a href="{{ route('solicitudes.edit',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-warning ml-1" data-toggle="tooltip" data-placement="top" title="Editar">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                          
                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'):?>
                                            <?php if( $solicitud->estatusbien == 'A' && Auth::user()->tipousuario['nombre'] != 'Administrador LGC'):?>
                                         
                                                {!! Form::open(['route' => ['solicitudes.delete', $solicitud->idbienevaluar], 'id'=>'FormSolicitudEliminar'.$solicitud->idbienevaluar, 'method' => 'post']) !!}

                                                {{ Form::hidden('_eliminar',null,['id'=>'_eliminar'])}}

                                                    <button type="button" class="btn btn-sm btn-danger ml-1" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar({{$solicitud->idbienevaluar}})">
                                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                {!! Form::close() !!}  
                                          
                                            <?php endif;?>

                                            <?php if( $solicitud->estatusbien == 'A' && Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && Auth::user()->id == $solicitud->solicitante ):?>
                                         
                                                {!! Form::open(['route' => ['solicitudes.delete', $solicitud->idbienevaluar], 'id'=>'FormSolicitudEliminar'.$solicitud->idbienevaluar, 'method' => 'post']) !!}

                                                {{ Form::hidden('_eliminar',null,['id'=>'_eliminar'])}}
                                                    <button type="button" class="btn btn-sm btn-danger ml-1" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar({{$solicitud->idbienevaluar}})">
                                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                {!! Form::close() !!}  
                                          
                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $solicitud->solicitante != Auth::user()->id ) :?>
                                            <?php if( $solicitud->estatusbien == 'A'):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Cotizar">
                                                    <i class="fas fa-money-bill-wave-alt"></i>
                                                </a>   

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $solicitud->solicitante != Auth::user()->id ) :?>
                                            <?php if( $solicitud->estatusbien == 'O' && $solicitud->cotizante == Auth::user()->id):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Cotizar">
                                                    <i class="fas fa-money-bill-wave-alt"></i>
                                                </a>   

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $solicitud->solicitante != Auth::user()->id ) :?>
                                            <?php if( $solicitud->estatusbien == 'O' && $solicitud->cotizante != Auth::user()->id):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>  

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $solicitud->cotizante == Auth::user()->id ):?>
                                            <?php if( $solicitud->estatusbien == 'U'):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Asignar">
                                                    <i class="fas fa-user-tie"></i>
                                                </a>   

                                            <?php endif;?>
                                        <?php endif;?>


                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $solicitud->cotizante != Auth::user()->id && $solicitud->solicitante != Auth::user()->id ):?>
                                            <?php if( $solicitud->estatusbien == 'U'):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>    

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' || Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT'  ):?>
                                            <?php if( $solicitud->estatusbien == 'C'):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>   

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC' && $solicitud->solicitante == Auth::user()->id ):?>
                                            <?php if( $solicitud->estatusbien == 'U'):?>

                                                <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>   

                                            <?php endif;?>
                                        <?php endif;?>


                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Usuario SAT'):?>
                                            <?php if( $solicitud->estatusbien == 'O'):?>

                                                    <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Autorizar">
                                                        <i class="fa fa-handshake"></i>
                                                    </a>

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador LGC'):?>
                                            <?php if( $solicitud->estatusbien == 'O' && $solicitud->solicitante == Auth::user()->id):?>

                                                    <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Autorizar">
                                                        <i class="fa fa-handshake"></i>
                                                    </a>

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Valuador'):?>
                                            <?php if( $solicitud->estatusbien == 'S' || $solicitud->estatusbien == 'V' ):?>

                                                    <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Avaluar">
                                                        <i class="fa fa-hand-holding-usd"></i>
                                                    </a>

                                            <?php endif;?>
                                        <?php endif;?>

                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Revisor'):?>
                                            <?php if( $solicitud->estatusbien == 'S' || $solicitud->estatusbien == 'V' ):?>

                                                    <a href="{{ route('solicitudes.view',[$solicitud->idsolicitud,$solicitud->idbienevaluar])}}" class="btn btn-sm btn-dark ml-1" data-toggle="tooltip" data-placement="top" title="Aprobar">
                                                        <i class="fa fa-thumbs-up"></i>
                                                    </a>

                                            <?php endif;?>
                                        <?php endif;?>
                                        
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- SWEETALERT2-->
        <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <!-- Page JS Plugins BOTONES -->
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/datatables/buttons/buttons.html5.min.js') }}"></script>
    <!-- FIN Page JS Plugins BOTONES -->


    <!--SELECT 2-->
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
    <script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $('.js-select2').select2({language: "es"});
    </script>

    <!-- FIN DE TABLA -->


    <!-- Page JS Plugins DATATABLE-->
    <script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script>
    <script src="//cdn.datatables.net/rowgroup/1.1.1/js/dataTables.rowGroup.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.20/api/sum().js"></script>
    <!-- FIN Page JS Plugins DATATABLE-->

    <script>
        function eliminar(id){
            Swal.fire({
                title: '¿Seguro desea eliminar la solicitud?',
                text: "Usted esta por eliminar la solicitud",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '¡Si, estoy seguro!'
            }).then((result) => {
                if (result.value) {
                    $('#FormSolicitudEliminar'+id).submit();
                }
            });
        }

        /* Inicializa la tabla cuando se hace uso del filtro fecha */
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {

                if($("#example-radio-custom-inline1").is(':checked')) {

                    var iFini = document.getElementById('fecha_inicio').value;
                    var iFfin = document.getElementById('fecha_inicio').value;
                    
                }
                else
                {

                    var iFini = document.getElementById('fecha_inicio').value;
                    var iFfin = document.getElementById('fecha_fin').value;

                }

                //var iFini = document.getElementById('fecha_inicio').value;
                //var iFfin = document.getElementById('fecha_fin').value;
                var iStartDateCol = 0;
                var iEndDateCol = 0;

                iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
                iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

                var datofini=data[iStartDateCol].substring(6,10) + data[iStartDateCol].substring(3,5)+ data[iStartDateCol].substring(0,2);
                var datoffin=data[iEndDateCol].substring(6,10) + data[iEndDateCol].substring(3,5)+ data[iEndDateCol].substring(0,2);

                if ( iFini === "" && iFfin === "" )
                {
                    return true;
                }
                else if ( iFini <= datofini && iFfin === "")
                {
                    return true;
                }
                else if ( iFfin >= datoffin && iFini === "")
                {
                    return true;
                }
                else if (iFini <= datofini && iFfin >= datoffin)
                {
                    return true;
                }
                return false;

            }
        );

        $(document).ready(function() {
            $('#boton_esconder_menu').click();

            $("#example-radio-custom-inline1").prop('checked',true);
            $("#fecha_fin").prop('disabled',true);

            var tabla= $('#datatable-Solicitud').DataTable( {
                "lengthMenu": [[25, 50, -1], [25, 50, "Todos"]],
                buttons:[
                {extend:"copy",className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="far fa-copy fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                }},
                {extend:"excel",messageTop: 'Información de Solicitudes ', title:'Listado de Solicitudes', className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="far fa-file-excel fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                }},
                {extend:"print",messageTop: 'Información de Solicitudes', title:'Listado de Solicitudes',className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="fa fa-print fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                }},
                {extend:"pdf",messageTop: 'Información de Solicitudes', title:'Listado de Solicitudes',className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="far fa-file-pdf fa-1x"></i>',
                    orientation:'landscape',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                }}
                ],
                "columns": [
                    {data: "fechasolicitud"},
                    {data: "foliosolicitud"},  
                    {data: "foliointerno"},
                    {data: "oficinasat"},  
                    {data: "contribuyente"},
                    {data: "bien"}, 
                    {data: "usuariosolicitante"}, 
                    {data: "usuariocotizante"},                      
                    {data: "valuador"}, 
                    {data: "revisor"},   
                    {data: "estatusbien"},                                   
                    {data: "accion", name:'accion', className:'text-center'}
                    
                ],
                initComplete: function() {
                    this.api().columns([1,2,3,4,6,7,8,10,11]).every(function() {
                        var column = this;
                        var columnIndex = column.index();

                        var selects = $('select[data-column='+columnIndex+']').append('<option value="">Todos</option>')
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                            );

                            column
                              .search(val ? '^' + val + '$' : '', true, false)
                              .draw();
                        });

                        column.data().unique().sort().each(function(d, j) {

                            if( d != '' && d != null){

                                selects.append('<option value="' + d + '">' + d + '</option>');

                            }                          

                        });
                    });
                },
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                },
                dom:
                "<'row'<'col-sm-12 col-md-12 py-2 mb-2'<'text-right'B>>>"+
                "<'row'<'col-sm-12 col-md-6 py-2 mb-2'l><'col-sm-12 col-md-6 py-2 mb-2'f>>"+
                "<'row row-records'<'col-sm-12'tr>>"+
                "<'row row-info'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
            } );

            // Evento que inicial el filtrado por fechas
            $('#fecha_inicio, #fecha_fin').change( function() {

                    $('#fecha_fin').datepicker('setStartDate', $('#fecha_inicio').val());
                    $('#fecha_inicio').datepicker('setEndDate', $('#fecha_fin').val());

                    tabla.draw();

            } );

            //NO MOSTRAR RANGO DE FECHA
            $("#example-radio-custom-inline1").click(function() {
                if($("#example-radio-custom-inline1").is(':checked')) {
                    $("#fecha_fin").prop('disabled',true);
                    $('#fecha_inicio, #fecha_fin').val(null);                    
                } else {
                    $("#fecha_fin").prop('disabled',false);
                }
                tabla.draw();
            });

            //MOSTRAR RANGO DE FECHA
            $("#example-radio-custom-inline2").click(function() {
                if($("#example-radio-custom-inline2").is(':checked')) {
                    $("#fecha_fin").prop('disabled',false);
                } else {
                    $("#fecha_fin").prop('disabled',true);
                    $('#fecha_fin').val($('#fecha_inicio').val());
                }
                tabla.draw();
            });
        });
    </script>
@endsection