<!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #8E000C;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center bg-white" href="{{ route('home') }}">

        <img class="img-responsive" src="{{ asset('lgc.png') }}" style="width: 100px; height: px;" alt="LGC Avaluos">
                   
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- HOME -->
      <li class="nav-item <?php echo (Route::currentRouteName() == 'home') ? 'active' : ''; ?>">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fas fa-fw fa-home"></i>
          <span>Inicio</span></a>
      </li>
      <!-- FIN DE HOME -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- OPCIONES -->
      <div class="sidebar-heading">
        Opciones
      </div>
      <?php echo session()->get('Menu');?>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
<!-- End of Sidebar -->
