<!-- BARRA SUPERIOR -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
            <span class="mr-2 d-none d-lg-inline text-gray-600">Notificaciones</span>
            <?php if(isset($CantidadNotificacion)):?>   
              <span class="badge badge-info mr-1">{{ $CantidadNotificacion }}</span>
            <?php endif; ?> 
            <?php if(isset($pluck['CantidadNotificacion'])):?>   
              <span class="badge badge-info mr-1">{{ $pluck['CantidadNotificacion'] }}</span>
            <?php endif; ?>
            <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block h5"></i>           
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in pl-2 pr-2" aria-labelledby="userDropdown">

            <?php if(isset($Notificaciones)):?> 
              @foreach ($Notificaciones as $notificacion)

              <span class="badge badge-info mr-1">{{ $notificacion->fecha }}</span>
              <a class="dropdown-item" href="{{ route('solicitudes.view', [$notificacion->idsolicitud, $notificacion->idtipobien])}}">
                {{ $notificacion->nombre }}
              </a>
              <?php if($notificacion->nombre == 'Nueva Solicitud de Avalúo' || $notificacion->nombre == 'Solicitud de Avalúo Cotizada'):?> 
                <div class="text-center">
                  <span class="badge badge-info mr-1">Folio Externo: {{ $notificacion->folioexterior }}</span>
                </div>
              <?php endif; ?> 
              <?php if($notificacion->nombre != 'Nueva Solicitud de Avalúo' && $notificacion->nombre != 'Solicitud de Avalúo Cotizada'):?> 
                <div class="text-center">
                  <span class="badge badge-info mr-1">Folio Interno: {{ $notificacion->foliointerno }}</span>
                </div>
              <?php endif; ?> 
              <div class="dropdown-divider"></div>            

              @endforeach
            <?php endif; ?> 

            <?php if(isset($pluck['Notificaciones'])):?> 
              @foreach ($pluck['Notificaciones'] as $notificacion)

              <span class="badge badge-info mr-1">{{ $notificacion->fecha }}</span>
              <a class="dropdown-item" href="{{ route('solicitudes.view', [$notificacion->idsolicitud, $notificacion->idtipobien])}}">
                {{ $notificacion->nombre }}
              </a>
              <?php if($notificacion->nombre == 'Nueva Solicitud de Avalúo' || $notificacion->nombre == 'Solicitud de Avalúo Cotizada'):?> 
                <div class="text-center">
                  <span class="badge badge-info mr-1">Folio Externo: {{ $notificacion->folioexterior }}</span>
                </div>
              <?php endif; ?> 
              <?php if($notificacion->nombre != 'Nueva Solicitud de Avalúo' && $notificacion->nombre != 'Solicitud de Avalúo Cotizada'):?> 
                <div class="text-center">
                  <span class="badge badge-info mr-1">Folio Interno: {{ $notificacion->foliointerno }}</span>
                </div>
              <?php endif; ?> 
              <div class="dropdown-divider"></div>            

              @endforeach
            <?php endif; ?> 

            <?php if(isset($pluck['CantidadNotificacion'])):?>
              <?php if($pluck['CantidadNotificacion']==0):?>
                <span>No posee notificaciones</span>
              <?php endif; ?> 
            <?php endif; ?> 

            <?php if(isset($CantidadNotificacion)):?>
              <?php if($CantidadNotificacion==0):?>
                <span>No posee notificaciones</span>
              <?php endif; ?> 
            <?php endif; ?> 

          </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php if(isset($Usuario)):?>

              <?php if( $Usuario->nombresat == null || $Usuario->nombresat == '' ):?>
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ $Usuario->nombrelgc.' '.$Usuario->apellidopat.' '.$Usuario->apellidomat.' ( '.auth()->user()->tipousuario["nombre"].' )' }}</span>
              <?php endif; ?>

              <?php if( $Usuario->nombresat != null && $Usuario->nombresat != '' ):?>
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ $Usuario->nombresat.' ( '.auth()->user()->tipousuario["nombre"].' )' }}</span>
              <?php endif; ?>

            <?php endif; ?> 

            <?php if(isset($pluck['Usuario'])):?>

              <?php if( $pluck['Usuario']->nombresat == null || $pluck['Usuario']->nombresat == '' ):?>
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ $pluck['Usuario']->nombrelgc.' '.$pluck['Usuario']->apellidopat.' '.$pluck['Usuario']->apellidomat.' ( '.auth()->user()->tipousuario["nombre"].' )' }}</span>
              <?php endif; ?>

              <?php if( $pluck['Usuario']->nombresat != null && $pluck['Usuario']->nombresat != '' ):?>
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ $pluck['Usuario']->nombresat.' ( '.auth()->user()->tipousuario["nombre"].' )' }}</span>
              <?php endif; ?>

            <?php endif; ?> 
            
            <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block h5"></i>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{route('users.edit',auth()->user()->id)}}">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Perfil
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Salir
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </div>
        </li>



    </ul>
</nav>
<!-- FIN DE BARRA SUPERIOR -->
