@extends('plantillas.privada')
@section('content')

    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('vendor/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

    <style>

        .text-small {
          font-size: 14px;
        }

    </style>

    

    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('configuraciones_editar',array('id'=>$Configuracion->id,'nombre'=>$Configuracion->id>0?$pluck["ubicacion"].' '.$Configuracion->id:"Crear usuario")) }}

    <!-- EDITAR USUARIOS -->
    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i><span id="span-accion">Editar Certificado</span>
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="text-right">
                <a href="{{ route('configuraciones.index')}}">
                    <button type="button" class="btn btn-dark">
                        <i class="fa fa-arrow-left"></i> Volver
                    </button>
                </a>
            </div>
        </div>
    </div>
    <br>
    <!-- FORMULARIO  USUARIOS -->
            {{ Form::model($Configuracion, ['action' => ['ConfiguracionController@update', $Configuracion->id>0?$Configuracion->id:-1], 'id' => 'UserEditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}


    <div class="card shadow mb-4">
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Datos Certificado
        </div>
        <div class="card-body">            

        		@if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
               	<div class="row">
               		<div class="col-12">
                        <div class="alert alert-success d-none" role="alert" id="alertfinal">
                            <h3 class="alert-heading">Por favor espere <i class="fa fa-circle-notch fa-spin text-success"></i></h3>
                        </div>
                    </div>
               	</div>                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('cer') ? ' has-error' : '' }} pt-3">
                            <input required class="file-cer" id="cer" name="cer" type="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione un archivo .cer" data-allowed-file-extensions='["cer"]'> 

                            @if ($errors->has('cer'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cer') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('key') ? ' has-error' : '' }} pt-3">
                            <input required class="file-key" id="key" name="key" type="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione un archivo .key" data-allowed-file-extensions='["key"]'> 

                            @if ($errors->has('key'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('key') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="form-group required{{ $errors->has('pass') ? ' has-error' : '' }}">
                            {{ Form::label('pass', 'Contraseña') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-key"></i>
                                    </span>
                                </div>
                                {{
                                    Form::password(
                                        'pass',
                                        [
                                            'class'=>'form-control text-pass',
                                            'id'=>'pass',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese Contraseña',
                                            'maxlength'=>'255',
                                        ]
                                    )
                                }}
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="cursor:pointer">
                                        <i class="fa fa-eye-slash icon"></i>
                                    </span>
                                </div>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
        </div>       
      
        <div class="row mt-2 row-btn mb-4">
            <div class="col-md-12">
                <div class="text-center">
                    <button type="submit" class="btn btn-info" id="btn-guardar">
                        <li class=" fas fa-file-signature"></li>
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
        {{ Form::close() }}
            <!-- END FORMULARIO  USUARIOS -->
            <!-- SWEETALERT2-->
                <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->
            <script type="text/javascript">

                window.addEventListener("keypress", function(event){
                    if (event.keyCode == 13){
                        event.preventDefault();
                    }
                }, false);

                $(document).ready(function() {


                });
            </script>

            <script>
                $(".file-cer").fileinput({
                    language: "es",
                    theme: "fas",
                    allowedFileExtensions: ["cer"],
                    /*maxFileSize: 3072,
                    minImageWidth: 232,
                    minImageHeight: 155,
                    maxImageWidth: 650,
                    maxImageHeight: 433,*/
                    maxFileCount: 1
                });
                $(".file-key").fileinput({
                    language: "es",
                    theme: "fas",
                    allowedFileExtensions: ["cer"],
                    /*maxFileSize: 3072,
                    minImageWidth: 232,
                    minImageHeight: 155,
                    maxImageWidth: 650,
                    maxImageHeight: 433,*/
                    maxFileCount: 1
                });
            </script>
        </div>
    </div>
@endsection

