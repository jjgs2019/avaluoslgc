@extends('plantillas.privada')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">

	<?php if(isset($Usuario)):?>

      <?php if( $Usuario->nombresat == null || $Usuario->nombresat == '' ):?>
      <h1 class="h3 mb-0 text-gray-800">Bienvenido: {{ $Usuario->nombrelgc.' '.$Usuario->apellidopat.' '.$Usuario->apellidomat }}</h1>
      <?php endif; ?>

      <?php if( $Usuario->nombresat != null && $Usuario->nombresat != '' ):?>
      <h1 class="h3 mb-0 text-gray-800">Bienvenido: {{ $Usuario->nombresat }}</h1>
      <?php endif; ?>

    <?php endif; ?> 

</div>
@endsection
