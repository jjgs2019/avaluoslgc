@component('mail::message')
# Saludos

Hola, la solicitud de Avalúo que registro en la App de LGC Avalúos ha sido aprobada


Te invitamos a ingresar y consultarla.

@component('mail::button', ['url' =>route('login')])
Ingresar a LGC Avalúos
@endcomponent

Gracias, y que estés bien !<br>
LGC {{ config('app.name') }}
@endcomponent