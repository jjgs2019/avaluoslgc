@component('mail::message')
# Saludos

Hola, ya valuaron la solicitud de Avalúo en la que esta asignado como Revisor


Te invitamos a ingresar y consultarla.

@component('mail::button', ['url' =>route('login')])
Ingresar a LGC Avalúos
@endcomponent

Gracias, y que estés bien !<br>
LGC {{ config('app.name') }}
@endcomponent
