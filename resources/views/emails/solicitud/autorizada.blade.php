@component('mail::message')
# Saludos

Hola, ya autorizaron la solicitud de Avalúo que cotizo en la App de LGC Avalúos


Te invitamos a ingresar y consultarla.

@component('mail::button', ['url' =>route('login')])
Ingresar a LGC Avalúos
@endcomponent

Gracias, y que estés bien !<br>
LGC {{ config('app.name') }}
@endcomponent
