@component('mail::message')
# Bienvenido

Hola, has sido registrado en la App de LGC Avalúos

Tus credenciales son:

#Usuario: {{ $data['usuario'] }}

#Contraseña: {{ $data['contrasena'] }}

Te invitamos a ingresar.

@component('mail::button', ['url' =>route('login')])
Ingresar a LGC Avalúos
@endcomponent

Saludos, y que estés bien !<br>
LGC {{ config('app.name') }}
@endcomponent
