<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="<?php echo session()->get('Configuracion.Web.descripcion');?>">
        <meta name="keywords" content="<?php echo session()->get('Configuracion.Web.palabrasclaves');?>" />
        <meta name="author" content="{{ env('APP_AUTOR') }}">

        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            <?php echo session()->get('Configuracion.Web.titulo');?>
        </title>

        <!-- FONT -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- ESTILOS -->
        <link rel="stylesheet" id="css-main" href="{{ asset('css/sb-admin-2.min.css') }}">
        <link href="{{ asset('vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">

        <!-- JS-->
            <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <!-- FIN DE JS-->

        <!--DATATABLE-->
            <link rel="stylesheet" href="{{ asset('vendor/datatables/dataTables.bootstrap4.css') }}">
            <link rel="stylesheet" href="{{ asset('vendor/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
            <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('vendor/datatables/buttons/dataTables.buttons.min.js') }}"></script>
            <script src="{{ asset('vendor/datatables/buttons/buttons.print.min.js') }}"></script>
            <script src="{{ asset('vendor/datatables/buttons/buttons.html5.min.js') }}"></script>
            <script src="{{ asset('vendor/datatables/buttons/buttons.flash.min.js') }}"></script>
            <script src="{{ asset('vendor/datatables/buttons/buttons.colVis.min.js') }}"></script>
            <script src="{{ asset('vendor/be_tables_datatables.min.js') }}"></script>
        <!--FIN DE DATATABLE-->
        <script type="text/javascript">
            function AcceptNum(evt){
                //ACEPTA NUMERO
                var key = evt.which || evt.keyCode;
                return (key <= 13 || (key >= 48 && key <= 57) );
            }
            function AcceptNumPunto(evt){
                //ACEPTA NUMERO
                var key = evt.which || evt.keyCode;
                return (key <= 13 || key <= 46 || (key >= 48 && key <= 57) );
            }
            function AcceptLetra(evt){
                //ACEPTA LETRAS
                var key = evt.which || evt.keyCode;
                if((key!=32) && (key<65) || (key>90) && (key<97) || (key>122 && key != 241 && key != 209 && key != 225 && key != 233 && key != 237 && key != 243 && key != 250 && key != 193 && key != 201 && key != 205 && key != 211 && key != 218)){
                    if(key==0 || key==8 || key==9 || key==17 || key==18 || key==46 || key==37 || key==38 || key==39 || key==40 || key==116){
                        return key;
                    }else{
                        return false;
                    }
                }else{
                    return key;
                }
            }
            function trunc (x, posiciones = 2) {
                //FUNCION PARA TRUNCAR NUMEROS FLOTANTES
                var s = x.toString()
                var l = s.length
                var decimalLength = s.indexOf('.') + 1

                if (l - decimalLength <= posiciones){
                return x
                }
                // Parte decimal del número
                var isNeg  = x < 0
                var decimal =  x % 1
                var entera  = isNeg ? Math.ceil(x) : Math.floor(x)
                // Parte decimal como número entero
                // Ejemplo: parte decimal = 0.77
                // decimalFormated = 0.77 * (10^posiciones)
                // si posiciones es 2 ==> 0.77 * 100
                // si posiciones es 3 ==> 0.77 * 1000
                var decimalFormated = Math.floor(
                Math.abs(decimal) * Math.pow(10, posiciones)
                )
                // Sustraemos del número original la parte decimal
                // y le sumamos la parte decimal que hemos formateado
                var finalNum = entera +
                ((decimalFormated / Math.pow(10, posiciones))*(isNeg ? -1 : 1))

                return finalNum
            }
            $(document).ready(function() {
                  $(".alert").fadeOut(5000);
            });
        </script>
    </head>
    <body id="page-top">
        <?php
            $ruta= Route::getCurrentRoute()->getActionName();
            $accion=explode('@', Route::getCurrentRoute()->getActionName())[1];
        ?>
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- MENU -->
                @include('elementos.menu')
            <!-- FIN DE MENU -->

            <!-- Contenido -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- BARRA SUPERIOR -->
                      @include('elementos.barrasuperior')
                    <!-- FIN DE BARRA SUPERIOR -->

                    <div class="container-fluid">
                        <!-- CONTENIDO -->
                            @yield('content')
                        <!-- FIN DE CONTENIDO -->
                    </div>
                </div>

                <!-- Footer -->
                  @include('elementos.footer')

                <!-- End of Footer -->
            </div>
        </div>

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- JS -->
            <!-- Bootstrap core JavaScript-->
            <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

            <!-- Core plugin JavaScript-->
            <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

            <!-- Custom scripts for all pages-->
            <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

            <script>
                /*Habilitando tooltip*/
                $(function () {
                  $('[data-toggle="tooltip"]').tooltip()
                })
            </script>
        <!-- FIN DE JS -->
    </body>
</html>
