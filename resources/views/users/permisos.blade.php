@extends('plantillas.privada')
@section('content')
        <!-- ANEXANDO NAVEGACION -->
        {{ Breadcrumbs::render('users_editar',array('id'=>$User->id,'username'=>$user)) }}

        @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <br>
        <div class="row">
            <div class="col-md-6 offset-md-6">
                <div class="text-right">
                    <a href="#">
                        <button type="button" class="btn btn-primary" style="padding-top: 10px;">
                            <label>Activar/Desactivar <input type="checkbox"  id="MarcarTodos" name="MarcarTodos" value="MarcarTodos"/> </label>
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="m-0 font-weight-bold text-primary">Permisos</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table id="datatable-permisosusuarios" class="table table-bordered table-striped table-hover table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%;">#</th>
                                <th class="text-left" style="width: 15%;">Módulo</th>
                                <th class="text-left" style="width: 30%;">Nombre</th>
                                <th class="text-left" style="width: 40%;">Descripción</th>
                                <th class="text-center" style="width: 10%;">Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=0;
                                $modulo_permiso_anterior=null;
                            ?>
                            @foreach ($modulos as $data)
                                <?php
                                    $color= '';
                                    $background_color= '';
                                    if($data->modulo_permiso != $modulo_permiso_anterior)
                                    {
                                        $i++;
                                        $j=1;
                                        $color= $data->color_permiso;
                                        $background_color= $data->background_permiso;
                                ?>
                                        {{-- <tr>
                                            <td colspan="5" class="text-center">
                                                {{ $i.'. '.$data->modulo_permiso }}
                                                <input type="hidden" id="{{$data->id}}__id" name="{{$data->id}}__id" value="{{false}}">
                                            </td>
                                        </tr> --}}
                                <?php
                                    }
                                ?>
                                <tr style="color:<?=$color;?>;background-color:<?=$background_color;?>;">
                                    <td class="text-center">
                                        {{ $i.'.'.$j }}
                                    </td>
                                    <td class="text-left">
                                        {{ $data->modulo_permiso }}
                                    </td>
                                    <td class="text-left">
                                        {{ $data->nombre_permiso }}
                                    </td>
                                    <td class="text-left">
                                        {{ $data->descripcion }}
                                    </td>
                                    <td class="text-center">
                                        <div class="custom-control custom-checkbox custom-control-primary mb-1">
                                            <input type="checkbox" class="custom-control-input activar" id="{{$data->id}}__estatus_checkbox" name="{{$data->id}}__estatus_checkbox" onchange="cambiarhidden({{$data->id}});" <?php if($data->estatususer=='A'){ echo 'checked=""';}?>>
                                            <input type="hidden" class="custom-control-input" id="{{$data->id}}__estatus" name="{{$data->id}}__estatus" value="{{$data->estatususer}}">
                                            <label class="custom-control-label" for="{{$data->id}}__estatus_checkbox"></label>
                                        </div>
                                        <input type="hidden" id="{{$data->id}}__id" name="{{$data->id}}__id" value="{{$data->id}}">
                                    </td>
                                </tr>
                                <?php
                                    $modulo_permiso_anterior=$data->modulo_permiso;
                                    $ids[]=$data->id;
                                    $j++;
                                ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button id="guardar" class="btn btn-success"><i class="far fa-save"></i> Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SWEETALERT2-->
            <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
            <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
        <!-- FIN DE SWEETALERT2-->
        <script>
            function cambiarhidden(id){
                if($('#'+id+'__estatus_checkbox').is(':checked')){
                    $('#'+id+'__estatus').val("A");
                }else{
                    $('#'+id+'__estatus').val("D");
                }
            }

            $(document).ready(function() {
                $('#boton_esconder_menu').click();

                t = $('#datatable-permisosusuarios').DataTable( {
                    "lengthMenu": [[50, -1], [50, "Todos"]],
                    "scrollX": true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                    }
                });
            });

            $('#guardar').click(function() {
                Swal.fire({
                    title: '¿Seguro desea guardar?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'No quiero aún',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: '¡Si, estoy seguro!'
                }).then((result) => {
                    if (result.value) {
                        // Option 1: using the serialized version
                        ar = $()
                        json_obj = {}
                        for (var i = 0; i < t.rows()[0].length; i++) {
                            ar = ar.add(t.row(i).node())
                        }
                        ar.find('select,input,textarea').each(function(i, el) {
                            json_obj[$(el).attr('name')] = $(el).val();
                        });
                        ar=JSON.stringify(json_obj);

                        $.ajax({
                            type: "POST",
                            url: "{{ route('users.spermisos')}}",
                            data: {
                                    "ar": ar,
                                    "_token": "{{ csrf_token() }}",
                                    "ids": "{{ json_encode($ids) }}",
                                    "iduser": "{{ $User->id }}",
                            },
                            dataType:"html",
                            success: function(datarecepcion) {
                                var obj = jQuery.parseJSON(datarecepcion);
                                console.log('Error: '+obj.error);
                                Swal.fire({
                                  type: obj.type,
                                  title: obj.title,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: 'Ok'
                                }).then((result) => {
                                  if (result.value) {
                                    if(obj.accion==1){
                                        setTimeout (window.location.href = '{{url('users/index')}}', 20000000);
                                    }
                                  }
                                });
                            },
                        });
                    }
                })
            });

           $("#MarcarTodos").click(function () {
              $(".activar").click();
              $("input:checkbox").prop('checked', $(this).prop("checked"));
           });
        </script>
@endsection

