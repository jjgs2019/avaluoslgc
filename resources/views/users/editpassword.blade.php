@extends('plantillas.privada')
@section('content')
    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('users_editar',array('id'=>$User->id,'username'=>$username)) }}

    <!-- EDITAR USUARIOS -->
    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i>Asignar nueva clave a usuario
            </h5>
        </div>
    </div>
    <br>
    <div class="card shadow mb-4">
        <div class="card-body">
            <!-- FORMULARIO  USUARIOS -->
            {{ Form::model($User, ['action' => ['UsersController@updatepassword'], 'id' => 'UserEditPasswordForm','method' => 'post', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" name="id" value="{{$User->id}}">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group required{{ $errors->has('password') ? ' has-error' : '' }}">
                                {{ Form::label('password', 'Contraseña nueva') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-key"></i>
                                        </span>
                                    </div>
                                    {{
                                        Form::password(
                                            'password',
                                            [
                                                'class'=>'form-control',
                                                'id'=>'password',
                                                'required'=>true,
                                                'placeholder'=>'Ingrese contraseña',
                                                'minlength'=>'6',
                                                'maxlength'=>'255',
                                            ]
                                        )
                                    }}
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" onclick="mostrarPassword();" style="cursor:pointer">
                                            <i class="fa fa-eye-slash icon"></i>
                                        </span>
                                    </div>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group required{{ $errors->has('repassword') ? ' has-error' : '' }}">
                                {{ Form::label('repassword', 'Confirme contraseña nueva') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-key"></i>
                                        </span>
                                    </div>
                                    {{
                                        Form::password(
                                            'repassword',
                                            [
                                                'class'=>'form-control',
                                                'id'=>'repassword',
                                                'required'=>true,
                                                'placeholder'=>'Ingrese nuevamente la contraseña',
                                                'minlength'=>'6',
                                                'maxlength'=>'255',
                                            ]
                                        )
                                    }}
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" onclick="mostrarPassword();" style="cursor:pointer">
                                            <i class="fa fa-eye-slash icon"></i>
                                        </span>
                                    </div>
                                </div>
                                @if ($errors->has('repassword'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('repassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div id="alertPasswordCanti" class="col-sm-12 d-none">
                                <div class="alert alert-danger"><div>Contraseña no segura.</div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-warning" id="boton">
                                <i class=" fas fa-file-signature"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
            <!-- END FORMULARIO  USUARIOS -->
            <!-- SWEETALERT2-->
                <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->
            <script type="text/javascript">
                /**
                 * Permite mostrar contrasena visualmente
                 *
                 */
                function mostrarPassword(){
                    var password = document.getElementById("password");
                    var repassword = document.getElementById("repassword");
                    if((password.type == "password")&&(repassword.type == "password")){
                        password.type = "text";
                        repassword.type = "text";
                        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    }else{
                        password.type = "password";
                        repassword.type = "password";
                        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    }
                }

                $(document).ready(function() {
                    $('#password').keypress(function(event){
                        if($('#password').val().length<6){
                            $('#boton').prop("disabled",true);
                            $('#alertPasswordCanti').removeClass('d-none');
                        }else{
                            $('#alertPasswordCanti').addClass('d-none');
                            $('#boton').prop("disabled",false);
                        }
                    });
                    $('#repassword').focusout(function(event){
                        if($('#password').val()!=$('#repassword').val()){
                            Swal.fire({
                              type: 'warning',
                              title: '¡Contraseña no coinciden!',
                              text: '¡Las contreseñas suministradas no coinciden, por favor verifique!'
                            });
                            $('#boton').prop("disabled",true);
                        }else{
                            $('#alertPassword').addClass('d-none');
                            $('#boton').prop("disabled",false);
                        }
                    });
                });
            </script>
        </div>
    </div>
@endsection

