@extends('plantillas.privada')
@section('content')

    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('vendor/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

    <style>

        .text-small {
          font-size: 14px;
        }

    </style>

    

    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('users_editar',array('id'=>$User->id,'nombre'=>$User->id>0?$pluck["ubicacion"].' '.$User->email:"Crear usuario")) }}

    <!-- EDITAR USUARIOS -->
    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i><span id="span-accion">Editar usuario</span>
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="text-right">
                <a href="{{ route('users.index')}}">
                    <button type="button" class="btn btn-dark">
                        <i class="fa fa-arrow-left"></i> Volver
                    </button>
                </a>
            </div>
        </div>
    </div>
    <br>
    <!-- FORMULARIO  USUARIOS -->
            {{ Form::model($User, ['action' => ['UsersController@update', $User->id>0?$User->id:-1], 'id' => 'UserEditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}


            <?php if( $pluck["ubicacion"] == 'Editar'):?>

                {{ Form::hidden('_editar',null,['id'=>'_editar'])}}

            <?php endif; ?>  


    <div class="card shadow mb-4">
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Datos Usuario
        </div>
        <div class="card-body">
            

            {{ Form::hidden('_estudios_documentos_cobertura_datos',null,['id'=>'estudios_documentos_cobertura_datos'])}}


        		@if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
               	<div class="row">
               		<div class="col-12">
                        <div class="alert alert-success d-none" role="alert" id="alertfinal">
                            <h3 class="alert-heading">Por favor espere <i class="fa fa-circle-notch fa-spin text-success"></i></h3>
                        </div>
                    </div>
               	</div>                
                <div class="row">

                    <div class="col-md-6 div-input-file">
                        <?php if( $pluck["ubicacion"] != 'Mostrar'):?>
                            <div class="form-group{{ $errors->has('foto_perfil') ? ' has-error' : '' }} pt-3">
                                <input id="foto_perfil" name="foto_perfil" type="file" class="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione una imagen" data-allowed-file-extensions='["jpg","jpeg","png","gif"]'> 

                                @if ($errors->has('foto_perfil'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('foto_perfil') }}</strong>
                                    </span>
                                @endif
                            </div>
                         <?php endif; ?> 
                    </div>

                    <?php if( $pluck["ubicacion"] != 'Crear'):?>

                        <div class="col-md-2">
                            <div class="row">
                                <img src="{{ asset('../storage/app/public/img/'.$User->nombreruta) }}" style="height: 200px; width: 150px;" />
                            </div>
                        </div>

                    <?php endif; ?> 

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required">
                                    {{ Form::label('idtipousuario', 'Tipo Usuario') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-layer-group"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::select(
                                                'idtipousuario',
                                                $pluck["Tipousuario"],
                                                $User->id>0?$User->idtipousuario:null,
                                                [
                                                    'id'=>'idtipousuario',
                                                    'class'=>'form-control input-nosat',
                                                    'required'=>true,
                                                    'disabled'=>$User->id>0?true:false
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-md-12">
                                <div class="form-group required{{ $errors->has('user') ? ' has-error' : '' }}">
                                    {{ Form::label('email ', 'Correo electrónico') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::email(
                                                'email',
                                                null,
                                                [
                                                    'class'=>'form-control input-nosat',
                                                    'id'=>'email',
                                                    'required'=>true,
                                                    'autofocus'=>'autofocus',
                                                    'placeholder'=>'Ingrese Correo electrónico',
                                                    'minlength'=>'3',
                                                    'maxlength'=>'255',
                                                    'disabled'=>$User->id>0?true:false
                                                ]
                                            )
                                        }}
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-text-password">
                                <div class="form-group required{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {{ Form::label('password', 'Contraseña') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-key"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::password(
                                                'password',
                                                [
                                                    'class'=>'form-control text-password',
                                                    'id'=>'password',
                                                    'required'=>true,
                                                    'placeholder'=>'Ingrese Contraseña',
                                                    'minlength'=>'6',
                                                    'maxlength'=>'255',
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" onclick="mostrarPassword();" style="cursor:pointer">
                                                <i class="fa fa-eye-slash icon"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-text-password">
                                <div class="form-group required{{ $errors->has('recontrasena') ? ' has-error' : '' }}">
                                    {{ Form::label('recontrasena', 'Confirmación de contraseña') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-key"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::password(
                                                'recontrasena',
                                                [
                                                    'class'=>'form-control text-password',
                                                    'id'=>'recontrasena',
                                                    'required'=>true,
                                                    'placeholder'=>'Confirme contraseña',
                                                    'minlength'=>'6',
                                                    'maxlength'=>'255',
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" onclick="mostrarPassword();" style="cursor:pointer">
                                                <i class="fa fa-eye-slash icon"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @if ($errors->has('recontrasena'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('recontrasena') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div id="alertPasswordCanti" class="col-sm-12 d-none">
                                <div class="alert alert-danger"><div>Contraseña no segura.</div></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold data-lgc">
            Datos LGC
        </div>
        <div class="card-body data-lgc">
            <div class="row row-details">
                <?php if(isset($pluck['DatosusuarioLGC'])):?>
                    {{ Form::hidden('id',$pluck['DatosusuarioLGC']->id,
                        [
                            'id'=>'iddatosusuarioLGC',
                            'data-name'=>'id',
                            'data-role'=>'DatosLGC',
                            'class'=>'input-lgc',
                        ])
                    }}                     
                <?php endif;?>
                <div class="col-md-4">
                    <div class="form-group required{{ $errors->has('nombrelgc') ? ' has-error' : '' }}">
                        {{ Form::label('nombrelgc ', 'Nombre') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-user-tie"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'nombre',
                                    isset($pluck['DatosusuarioLGC'])?$pluck['DatosusuarioLGC']->nombre:null,
                                    [
                                        'class'=>'form-control input-lgc',
                                        'id'=>'nombrelgc',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Nombre',
                                        'minlength'=>'3',
                                        'required'=>true,
                                        'maxlength'=>'255',
                                        'data-name'=>"nombre",
                                        'data-role'=>"DatosLGC",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('nombrelgc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombrelgc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group required{{ $errors->has('apellidopat') ? ' has-error' : '' }}">
                        {{ Form::label('apellidopat ', 'Apellido Paterno') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-user-tie"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'apellidopat',
                                    isset($pluck['DatosusuarioLGC'])?$pluck['DatosusuarioLGC']->apellidopat:null,
                                    [
                                        'class'=>'form-control input-lgc',
                                        'id'=>'apellidopat',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Apellido Paterno',
                                        'minlength'=>'3',
                                        'required'=>true,
                                        'maxlength'=>'255',
                                        'data-name'=>"apellidopat",
                                        'data-role'=>"DatosLGC",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('apellidopat'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellidopat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group required{{ $errors->has('apellidomat') ? ' has-error' : '' }}">
                        {{ Form::label('apellidomat ', 'Apellido Materno') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-user-tie"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'apellidomat',
                                    isset($pluck['DatosusuarioLGC'])?$pluck['DatosusuarioLGC']->apellidomat:null,
                                    [
                                        'class'=>'form-control input-lgc',
                                        'id'=>'apellidomat',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Apellido Materno',
                                        'minlength'=>'3',
                                        'required'=>true,
                                        'maxlength'=>'255',
                                        'data-name'=>"apellidomat",
                                        'data-role'=>"DatosLGC",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('apellidomat'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellidomat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row row-details">
                <div class="col-md-6">
                    <div class="form-group required">
                        {{ Form::label('celular', 'Celular') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-mobile-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'celular',
                                    isset($pluck['DatosusuarioLGC'])?$pluck['DatosusuarioLGC']->celular:null,
                                    [
                                        'class'=>'form-control input-lgc numero',
                                        'id'=>'celular',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Celular',
                                        'minlength'=>'3',
                                        'maxlength'=>'255',
                                        'data-name'=>"celular",
                                        'data-role'=>"DatosLGC",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('celular'))
                            <span class="help-block">
                                <strong>{{ $errors->first('celular') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required{{ $errors->has('telefonofijo') ? ' has-error' : '' }}">
                        {{ Form::label('telefonofijo', 'Teléfono Fijo') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-phone"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'telefonofijo',
                                    isset($pluck['DatosusuarioLGC'])?$pluck['DatosusuarioLGC']->telefonofijo:null,
                                    [
                                        'class'=>'form-control input-lgc numero',
                                        'id'=>'telefonofijo',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Teléfono Fijo',
                                        'minlength'=>'3',
                                        'maxlength'=>'255',
                                        'data-name'=>"telefonofijo",
                                        'data-role'=>"DatosLGC",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('telefonofijo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telefonofijo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold data-sat">
            Datos SAT
        </div>
        <div class="card-body data-sat">
            <div class="row row-details">
                <?php if(isset($pluck['DatosusuarioSAT'])):?>
                    {{ Form::hidden('id',$pluck['DatosusuarioSAT']->id,
                        [
                            'id'=>'iddatosusuarioSAT',
                            'data-name'=>'id',
                            'data-role'=>'DatosSAT',
                            'class'=>'input-sat',
                        ])
                    }}                     
                <?php endif;?>
                <div class="col-md-6">
                    <div class="form-group required{{ $errors->has('nombre') ? ' has-error' : '' }}">
                        {{ Form::label('nombre ', 'Nombre') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-user-tie"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'nombre',
                                    isset($pluck['DatosusuarioSAT'])?$pluck['DatosusuarioSAT']->nombre:null,
                                    [
                                        'class'=>'form-control input-sat',
                                        'id'=>'nombre',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Nombre',
                                        'minlength'=>'3',
                                        'required'=>true,
                                        'maxlength'=>'255',
                                        'data-name'=>"nombre",
                                        'data-role'=>"DatosSAT",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group required{{ $errors->has('rfc') ? ' has-error' : '' }}">
                        {{ Form::label('rfc', 'RFC') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-id-card"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'rfc',
                                    isset($pluck['DatosusuarioSAT'])?$pluck['DatosusuarioSAT']->rfc:null,
                                    [
                                        'class'=>'form-control input-sat',
                                        'id'=>'rfc',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese RFC',
                                        'minlength'=>'3',
                                        'maxlength'=>'255',
                                        'data-name'=>"rfc",
                                        'data-role'=>"DatosSAT",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('rfc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rfc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>                    
                <div class="col-md-3">
                    <div class="form-group required{{ $errors->has('telefono') ? ' has-error' : '' }}">
                        {{ Form::label('telefono', 'Teléfono') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-phone"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'telefono',
                                    isset($pluck['DatosusuarioSAT'])?$pluck['DatosusuarioSAT']->telefono:null,
                                    [
                                        'class'=>'form-control input-sat numero',
                                        'id'=>'rfc',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Telefono',
                                        'minlength'=>'3',
                                        'maxlength'=>'255',
                                        'data-name'=>"telefono",
                                        'data-role'=>"DatosSAT",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('telefono'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row row-details">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('idoficinasat', 'Oficina SAT') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'idoficinasat',
                                    $pluck["Oficinasat"],
                                    isset($pluck['DatosusuarioSAT'])?$pluck['DatosusuarioSAT']->idoficinasat:null,
                                    [
                                        'id'=>'idoficinasat',
                                        'class'=>'form-control input-sat',
                                        'required'=>true,
                                        'data-name'=>"idoficinasat",
                                        'data-role'=>"DatosSAT",
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required{{ $errors->has('cargo') ? ' has-error' : '' }}">
                        {{ Form::label('cargo', 'Cargo') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                            {{
                                Form::text(
                                    'cargo',
                                    isset($pluck['DatosusuarioSAT'])?$pluck['DatosusuarioSAT']->cargo:null,
                                    [
                                        'class'=>'form-control input-sat',
                                        'id'=>'cargo',
                                        'required'=>true,
                                        'placeholder'=>'Ingrese Cargo',
                                        'minlength'=>'3',
                                        'maxlength'=>'255',
                                        'data-name'=>"cargo",
                                        'data-role'=>"DatosSAT",
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('cargo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('cargo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold data-lgc">
            Documentación
        </div>
        <div class="card-body data-lgc">
            <div class="row">
                <div class="col-md-4 pr-0">
                    <div class="form-group">
                        {{ Form::label('addtipodocumento', 'Tipo Documento') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'addtipodocumento',
                                    $pluck["Tipodocumento"],
                                    null,
                                    [
                                        'id'=>'addtipodocumento',
                                        'class'=>'form-control text-small no-submit',
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-5 pr-0">
                    {{ Form::label('adddescripcion', 'Descripción') }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-keyboard"></i>
                            </span>
                        </div>
                            {{
                                Form::text(
                                    'adddescripcion',
                                    null,
                                    [
                                        'class'=>'form-control text-small no-submit',
                                        'id'=>'adddescripcion',
                                        'placeholder'=>'Ingrese Descripción',
                                        'minlength'=>'3',
                                        'maxlength'=>'255',
                                    ]
                                )
                            }}
                    </div>                                                   
                </div>
                <div class="col-md-2 pr-0">
                    {{ Form::label('addfecha', 'Fecha') }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                            {{
                                Form::text(
                                    'addfecha',
                                    date('d-m-Y'),
                                    [
                                        'class'=>'form-control text-small text-center no-submit',
                                        'id'=>'addfecha',
                                        'readonly'=>true,
                                    ]
                                )
                            }}
                    </div>                    
                </div>
                <div class="col-md-1">
                    <div class="form-group btn-click">              
                        <label class="font-size-sm">Agregar</label>
                        <button type="button" class="btn btn-dark btn-block btn-add" id="agregar-titulo" name="boton-titulo-agregar">                              
                            <i class="fa fa-check mr-1"></i>                            
                        </button>
                    </div>
                </div>
            </div>   
            <div class="row row-details">
                <div class="col-md-12">
                    <p class="text-muted text-center">Informacion correspondiente a la documentación del usuario</p>                          
                    <div class="table-responsive">            
                        <table class="table table-bordered table-hover table-striped table-vcenter table-titulo">              
                            <thead>              
                                <tr>                  
                                    <th class="text-center">Tipo Documento</th>
                                    <th class="text-center">Descripción</th>
                                    <th class="text-center">Fecha</th>
                                    <th class="text-center">Imagen</th>
                                    <th class="text-center">Accion</th>
                                </tr>              
                            </thead>              
                            <tbody>
                                <?php if(isset($pluck['Documentos'])):?>
                                    @foreach ($pluck['Documentos'] as $documento)
                                        <tr class="tr-contenido">
                                            {{ Form::hidden('id',$documento->id,
                                                [
                                                    'data-name'=>'id',
                                                    'data-role'=>'Documento',
                                                    'class'=>'input-lgc',
                                                ])
                                            }} 
                                            <td class="text-center">
                                                {{ Form::hidden('idtipodocumento',$documento->idtipodocumento,
                                                    [
                                                        'data-name'=>'idtipodocumento',
                                                        'data-role'=>'Documento',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $documento->tipodocumento->nombre }}
                                            </td>
                                            <td class="text-center">
                                                {{ Form::hidden('descripcion',$documento->descripcion,
                                                    [
                                                        'data-name'=>'descripcion',
                                                        'data-role'=>'Documento',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $documento->descripcion }}
                                            </td>
                                            <td class="text-center">
                                                {{ Form::hidden('fecha',$documento->fecha,
                                                    [
                                                        'data-name'=>'fecha',
                                                        'data-role'=>'Documento',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $documento->fecha }}
                                            </td>

                                            <td class="text-center">      
                                                <?php if( $documento->nombreruta != '' && $documento->nombreruta != null ):?>
                                                <a target="_blank" href="{{ route('users.visualizardocumentos',$documento->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Visualizar">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>

                                                <a href="{{ route('users.descargardocumentos',$documento->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                    <i class="fa fa-download"></i>
                                                </a>

                                                <?php endif;?>

                                            </td>
                                            <td class="text-center td-btn-eliminar">
                                                <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                            </td>
                                        </tr>
                                    @endforeach
                                <?php endif;?>
                                <?php if(!isset($pluck['Documentos'])):?>
                                    <tr>            
                                        <td colspan="5">
                                            <p class="text-muted text-center pt-4">No hay informacion registrada</p>                
                                        </td>
                                    </tr>
                                <?php endif;?>                            
                            </tbody>              
                        </table>          
                    </div>
                </div>    
            </div>
        </div>
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold data-lgc">
            Estudios
        </div>
        <div class="card-body data-lgc">
            <div class="row">
                <div class="col-md-4 pr-0">
                    <div class="form-group">
                        {{ Form::label('addnivelestudio', 'Nivel Estudio') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'addnivelestudio',
                                    $pluck["Nivelestudio"],
                                    null,
                                    [
                                        'id'=>'addnivelestudio',
                                        'class'=>'form-control text-small no-submit',
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-5 pr-0">
                    <div class="form-group">
                        {{ Form::label('addtituloprofesional', 'Titulo Profesional') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'addtituloprofesional',
                                    array(),
                                    null,
                                    [
                                        'id'=>'addtituloprofesional',
                                        'class'=>'form-control text-small no-submit',
                                    ]
                                )
                            }}
                        </div>
                    </div>                                                  
                </div>
                <div class="col-md-2 pr-0">
                    {{ Form::label('addcedula', 'Cedula') }}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-keyboard"></i>
                            </span>
                        </div>
                            {{
                                Form::text(
                                    'addcedula',
                                    null,
                                    [
                                        'class'=>'form-control text-small text-center no-submit',
                                        'id'=>'addcedula',
                                    ]
                                )
                            }}
                    </div>                    
                </div>
                <div class="col-md-1">
                    <div class="form-group btn-click">              
                        <label class="font-size-sm">Agregar</label>
                        <button type="button" class="btn btn-dark btn-block btn-add" id="agregar-titulo2" name="boton-titulo-agregar2">                              
                            <i class="fa fa-check mr-1"></i>                            
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('addcomentario') ? ' has-error' : '' }}">
                        {{ Form::label('addcomentario', 'Comentario') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                            {{
                                Form::textarea(
                                    'addcomentario',
                                    null,
                                    [
                                        'class'=>'form-control text-small no-submit',
                                        'id'=>'addcomentario',
                                        'placeholder'=>'Ingrese Comentario',
                                        'minlength'=>'6',
                                        'maxlength'=>'255',
                                        'rows'=>3,
                                        'disabled'=>$pluck["ubicacion"]=="Mostrar"?true:false
                                    ]
                                )
                            }}
                        </div>
                        @if ($errors->has('addcomentario'))
                            <span class="help-block">
                                <strong>{{ $errors->first('addcomentario') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>               
            </div>
               
            <div class="row row-details">
                <div class="col-md-12">
                    <p class="text-muted text-center">Informacion correspondiente a los estudios del usuario</p>                          
                    <div class="table-responsive">            
                        <table class="table table-bordered table-hover table-striped table-vcenter table-titulo2">              
                            <thead>              
                                <tr>                  
                                    <th class="text-center">Nivel Estudio</th>
                                    <th class="text-center">Titulo Profesional</th>
                                    <th class="text-center">Cedula</th>
                                    <th class="text-center">Comentario</th>
                                    <th class="text-center">Imagen</th>
                                    <th class="text-center">Accion</th>
                                </tr>              
                            </thead>              
                            <tbody>
                                <?php if(isset($pluck['Estudios'])):?>
                                    @foreach ($pluck['Estudios'] as $estudio)
                                        <tr class="tr-contenido">
                                            {{ Form::hidden('id',$estudio->id,
                                                [
                                                    'data-name'=>'id',
                                                    'data-role'=>'Estudio',
                                                    'class'=>'input-lgc',
                                                ])
                                            }} 
                                            <td class="text-center">
                                                {{ Form::hidden('idnivelestudio',$estudio->idnivelestudio,
                                                    [
                                                        'data-name'=>'idnivelestudio',
                                                        'data-role'=>'Estudio',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $estudio->nivelestudio->nombre }}
                                            </td>
                                            <td class="text-center">
                                                {{ Form::hidden('idtituloprofesional',$estudio->idtituloprofesional,
                                                    [
                                                        'data-name'=>'idtituloprofesional',
                                                        'data-role'=>'Estudio',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $estudio->tituloprofesional->nombre }}
                                            </td>
                                            <td class="text-center">
                                                {{ Form::hidden('cedula',$estudio->cedula,
                                                    [
                                                        'data-name'=>'cedula',
                                                        'data-role'=>'Estudio',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $estudio->cedula }}
                                            </td>
                                            <td>
                                                {{ Form::hidden('comentario',$estudio->comentario,
                                                    [
                                                        'data-name'=>'comentario',
                                                        'data-role'=>'Estudio',
                                                        'class'=>'input-lgc',
                                                    ])
                                                }} 
                                                {{ $estudio->comentario }}
                                            </td>
                                            <td class="text-center"> 
                                                <?php if( $estudio->nombreruta != '' && $estudio->nombreruta != null ):?>
                                                <a target="_blank" href="{{ route('users.visualizarestudios',$estudio->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Visualizar">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>

                                                <a href="{{ route('users.descargarestudios',$estudio->nombreruta)}}" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                                <?php endif;?>
                                                
                                            </td>
                                            <td class="text-center td-btn-eliminar2">
                                                <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                            </td>
                                        </tr>
                                    @endforeach
                                <?php endif;?>
                                <?php if(!isset($pluck['Estudios'])):?>
                                    <tr>            
                                        <td colspan="6">
                                            <p class="text-muted text-center pt-4">No hay informacion registrada</p>                
                                        </td>
                                    </tr>
                                <?php endif;?>                             
                            </tbody>              
                        </table>          
                    </div>
                </div>    
            </div>
        </div>
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold data-lgc">
            Coberturas
        </div>
        <div class="card-body data-lgc">
            <div class="row row-details">
                <div class="col-md-5">
                    <div class="form-group">
                        {{ Form::label('noseleccionados', 'No seleccionados') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'origen',
                                    isset($pluck['Nocoberturas'])?$pluck['Nocoberturas']:$pluck["Estado"],
                                    null,
                                    [
                                        'id'=>'origen',
                                        'class'=>'form-control no-submit',
                                        'multiple'=>'multiple',
                                        'size'=>'8',
                                    ]
                                )
                            }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2 text-center pt-5 mt-4">
                    <button type="button" class="btn btn-dark pasartodos izq btn-add">
                        Todos
                        <li class="fa fa-angle-double-right"></li>                            
                    </button>
                    <button type="button" class="btn btn-dark quitartodos der mt-3 btn-add">
                        <li class="fa fa-angle-double-left"></li>
                        Todos
                    </button>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        {{ Form::label('seleccionados', 'Seleccionados') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-list-alt"></i>
                                </span>
                            </div>
                            {{
                                Form::select(
                                    'idestado',
                                    isset($pluck['Coberturas'])?$pluck['Coberturas']:array(),
                                    null,
                                    [
                                        'id'=>'destino',
                                        'class'=>'form-control no-submit input-lgc',
                                        'multiple'=>'multiple',
                                        'size'=>'8',
                                        'data-role'=>"Cobertura",
                                        'data-name'=>"idestado",
                                        'data-type'=>"Select",
                                    ]
                                )
                            }}
                        </div>
                    </div>  
                </div>                    
            </div>            
        </div>
        <div class="row mt-2 row-btn mb-4">
            <div class="col-md-12">
                <div class="text-center">
                    <button type="submit" class="btn btn-info" id="btn-guardar">
                        <li class=" fas fa-file-signature"></li>
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
        {{ Form::close() }}
            <!-- END FORMULARIO  USUARIOS -->
            <!-- SWEETALERT2-->
                <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->
            <script type="text/javascript">

                $(".numero").keydown(function(event){

                    if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
                            return false;
                    }
                });


            /**
             * Permite ejecutar un change
             */
            $('#addnivelestudio').on('change',function (e) {
            //$("select[data-name=id]").change(function() {

                //var row = $(this).attr('data-id');

                var idSelect=e.target.value;

                $.ajax({
                    // la URL para la petición
                    url : "{{ route('users.buscartituloprofesionales') }}",

                    // la información a enviar
                    // (también es posible utilizar una cadena de datos)
                    data :
                    {
                        id : idSelect,
                        _token : "{{ csrf_token() }}"

                    },

                    // especifica si será una petición POST o GET
                    type : 'POST',

                    // el tipo de información que se espera de respuesta
                    dataType : 'json',

                    // código a ejecutar si la petición es satisfactoria;
                    // la respuesta es pasada como argumento a la función
                    success : function(json) {

                        //$('#description'+row).val(json.description)
                        //$('#cost'+row).val(json.cost)
                        //$('#preferredvendor'+row).val(json.preferredvendor);

                        $('#addtituloprofesional').empty();
                        //$('#addtituloprofesional').append('<option value="0">'+'Seleccione...'+'</option>');
                        //$('#select-programa[name=generado] option[value=0]').prop("selected",true);
                        //$('#select-programa[name=generado]').attr("disabled",true);

                        //Registro vacio
                        if(Object.keys(json).length == 0){

                            /*$('#addtituloprofesional').append('<option value="-1">'+'NO SE ENCONTRARON PROGRAMAS ACTIVOS EN '+NombreNucleo+'</option>');

                            $('#select-programa[name=generado] option[value=-1]').prop("selected",true);*/
                      
                        }
                        else
                        {
                            
                            //$('#select-programa[name=generado]').append('<option value="0">'+'Seleccione...'+'</option>');
                            //$('#select-programa[name=generado] option[value=0]').prop("selected",true);
                            //$('#select-programa[name=generado]').attr("disabled",false);

                            $.each(json, function(index,accountObj){

                                $('#addtituloprofesional').append('<option value="'+accountObj.id+'">'+accountObj.nombre+'</option>');

                            });

                        }

                        $('#addtituloprofesional').find('option:eq(0)').prop('selected', true);

                    },

                    // código a ejecutar si la petición falla;
                    // son pasados como argumentos a la función
                    // el objeto de la petición en crudo y código de estatus de la petición
                    error : function(xhr, status) {

                    },

                    // código a ejecutar sin importar si la petición falló o no
                    complete : function(xhr, status) {

                    }
                });


            });










                // Variables globales //

                var Contador=0;

                $("div.btn-click").on("click", "#agregar-titulo", function (event) {


                    Contador=$('table.table-titulo tbody').find('.tr-contenido').length;

                    var NuevaFila = $("<tr class='tr-contenido'>");
                    var Columnas = "";
                    var Confirmado= "fa fa-times-circle fa-2x";

                    var Fecha= $('#addfecha').val();
                    var Descripcion= $('#adddescripcion').val();
                    var Tipodocumento= $('#addtipodocumento option:selected').text();

                    if($('#adddescripcion').val() != ''){


                        if(Contador == 0){

                            $('table.table-titulo tbody').empty();


                        }
                        Contador++;

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Tipodocumento + '</span>'+
                            '<input class="no-submit input-lgc" data-name="idtipodocumento" data-role="Documento" name="idtipodocumento" type="hidden" value="'+$("#addtipodocumento option:selected").val()+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Descripcion + '</span>'+
                            '<input class="no-submit input-lgc" data-name="descripcion" data-role="Documento" name="descripcion" type="hidden" value="'+Descripcion+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Fecha + '</span>'+
                            '<input class="no-submit input-lgc" data-name="fecha" data-role="Documento" name="fecha" type="hidden" value="'+$("#addfecha").val()+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                                '<input required type="file" name="documentos[]">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center td-btn-eliminar"><i class="' + Confirmado + '" aria-hidden="true"></i></td></tr>';
              
                        NuevaFila.append(Columnas);
                        $("table.table-titulo tbody").append(NuevaFila);                      

                        $('#adddescripcion').val('');
                        $('#addtipodocumento').find('option:eq(0)').prop('selected', true);

                    }
                    else
                    {
                        Swal.fire({
                          type: 'warning',
                          title: '¡Datos incompletos!',
                          text: '¡Debe ingresar los datos solicitados, por favor verifique!'
                        });
                    }

                });


            <?php if( $pluck["ubicacion"] != 'Mostrar'):?>
                    
                $("table.table-titulo").on("click", ".td-btn-eliminar", function (event) {

                    $(this).closest("tr").remove();    

                    Contador=$('table.table-titulo tbody').find('.tr-contenido').length;

                    if(Contador == 0){

                        var NuevaFila = $("<tr>");
                        var Columnas = "";

                        Columnas += 
                            '<td colspan="5">'+
                            '<p class="text-muted text-center pt-4">No hay informacion registrada</p>'+
                            '</td></tr>';

                        NuevaFila.append(Columnas);
                        $("table.table-titulo tbody").append(NuevaFila);
                    }

                }); 

            <?php endif; ?>  







                var Contador2=0;

                $("div.btn-click").on("click", "#agregar-titulo2", function (event) {

                    Contador2=$('table.table-titulo2 tbody').find('.tr-contenido').length;

                    var NuevaFila = $("<tr class='tr-contenido'>");
                    var Columnas = "";
                    var Confirmado= "fa fa-times-circle fa-2x";

                    var Cedula= $('#addcedula').val();
                    var Nivelestudio= $('#addnivelestudio option:selected').text();
                    var Tituloprofesional= $('#addtituloprofesional option:selected').text();
                    var Comentario= $('#addcomentario').val();

                    if($('#addcedula').val() != ''){


                        if(Contador2 == 0){

                            $('table.table-titulo2 tbody').empty();

                        }

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Nivelestudio + '</span>'+
                            '<input class="no-submit input-lgc" data-name="idnivelestudio" data-role="Estudio" name="idnivelestudio" type="hidden" value="'+$("#addnivelestudio option:selected").val()+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Tituloprofesional + '</span>'+
                            '<input class="no-submit input-lgc" data-name="idtituloprofesional" data-role="Estudio" name="idtituloprofesional" type="hidden" value="'+$("#addtituloprofesional option:selected").val()+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Cedula + '</span>'+
                            '<input class="no-submit input-lgc" data-name="cedula" data-role="Estudio" name="cedula" type="hidden" value="'+Cedula+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                            '<span class="font-size-md">' + Comentario + '</span>'+
                            '<input class="no-submit input-lgc" data-name="comentario" data-role="Estudio" name="comentario" type="hidden" value="'+Comentario+'">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center">'+
                                '<input required type="file" name="estudios[]">'+
                            '</td>';

                        Columnas += 
                            '<td class="text-center td-btn-eliminar2"><i class="' + Confirmado + '" aria-hidden="true"></i></td></tr>';
              
                        NuevaFila.append(Columnas);
                        $("table.table-titulo2 tbody").append(NuevaFila);
                        Contador2++;

                        $('#addcedula').val('');
                        $('#addcomentario').val('');
                        $('#addnivelestudio').find('option:eq(0)').prop('selected', true);
                        $('#addnivelestudio').change();

                    }
                    else
                    {
                        Swal.fire({
                          type: 'warning',
                          title: '¡Datos incompletos!',
                          text: '¡Debe ingresar los datos solicitados, por favor verifique!'
                        });
                    }

                });


            <?php if( $pluck["ubicacion"] != 'Mostrar'):?>

                $("table.table-titulo2").on("click", ".td-btn-eliminar2", function (event) {

                    $(this).closest("tr").remove();       
                    
                    Contador2=$('table.table-titulo2 tbody').find('.tr-contenido').length;

                    if(Contador2 == 0){

                        var NuevaFila = $("<tr>");
                        var Columnas = "";

                        Columnas += 
                            '<td colspan="6">'+
                            '<p class="text-muted text-center pt-4">No hay informacion registrada</p>'+
                            '</td>';

                        NuevaFila.append(Columnas);
                        $("table.table-titulo2 tbody").append(NuevaFila);
                    }

                }); 

            <?php endif; ?>  







                /**
                 * Permite mostrar contrasena visualmente
                 *
                 */
                function mostrarPassword(){
                    var password = document.getElementById("password");
                    var recontrasena = document.getElementById("recontrasena");
                    if((password.type == "password")&&(recontrasena.type == "password")){
                        password.type = "text";
                        recontrasena.type = "text";
                        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    }else{
                        password.type = "password";
                        recontrasena.type = "password";
                        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    }
                }

                $('#idtipousuario').on('change',function (e) {

                    var idSelect=e.target.value;

                    if($('#idtipousuario option:selected').text() == 'Administrador SAT' || $('#idtipousuario option:selected').text() == 'Usuario SAT' ){

                        $('.data-sat').removeClass('d-none');
                        $('.input-sat').prop('disabled',false);

                        $('.data-lgc').addClass('d-none');
                        $('.input-lgc').prop('disabled',true);

                    }
                    else
                    {
                        $('.data-sat').addClass('d-none');
                        $('.input-sat').prop('disabled',true);

                        $('.data-lgc').removeClass('d-none');
                        $('.input-lgc').prop('disabled',false);
                    }
                
                });


                $(document).ready(function() {

                    $('#addnivelestudio').change();

                    <?php if( $pluck["ubicacion"] != 'Crear'):?>

                        $('.div-input-file').removeClass('col-md-6').addClass('col-md-4');

                    <?php endif; ?> 

                    <?php if( $pluck["ubicacion"] == 'Mostrar'):?>

                        $('.col-text-password').addClass('d-none');
                        $('.row-btn').addClass('d-none');
                        $('.text-password').prop('disabled',true);

                        if($('#idtipousuario option:selected').text() == 'Administrador SAT' || $('#idtipousuario option:selected').text() == 'Usuario SAT' ){

                            $('.data-sat').removeClass('d-none');
                            $('.data-lgc').addClass('d-none');

                        }
                        else
                        {
                            $('.data-sat').addClass('d-none'); 
                            $('.data-lgc').removeClass('d-none');                           
                        }

                        $('.input-sat').prop('disabled',true);
                        $('.input-lgc').prop('disabled',true);
                        $('.input-nosat').prop('disabled',true);
                        $('.no-submit').prop('disabled',true);
                        $('.btn-add').prop('disabled',true);

                        $('.td-btn-eliminar').empty();
                        $('.td-btn-eliminar2').empty();

                        $('#span-accion').text('Mostrar Usuario');

                    <?php endif; ?>  

                    <?php if( $pluck["ubicacion"] == 'Editar'):?>

                        $('.col-text-password').addClass('d-none');
                        $('.text-password').prop('disabled',true);

                        $('#span-accion').text('Editar Usuario');

                    <?php endif; ?>  

                    <?php if( $pluck["ubicacion"] == 'Crear'):?>

                        $('#span-accion').text('Crear Usuario');

                    <?php endif; ?>  

                    $('#password').keypress(function(event){
                        if($('#password').val().length<6){
                            $('#boton').prop("disabled",true);
                            $('#alertPasswordCanti').removeClass('d-none');
                        }else{
                            $('#alertPasswordCanti').addClass('d-none');
                            $('#boton').prop("disabled",false);
                        }
                    });
                    $('#recontrasena').focusout(function(event){
                        if($('#password').val()!=$('#recontrasena').val()){
                            Swal.fire({
                              type: 'warning',
                              title: '¡Contraseña no coinciden!',
                              text: '¡Las contreseñas suministradas no coinciden, por favor verifique!'
                            });
                            $('#boton').prop("disabled",true);
                        }else{
                            $('#alertPassword').addClass('d-none');
                            $('#boton').prop("disabled",false);
                        }
                    });

                    var Model   = new Array("Estudio","Documento","Cobertura", "DatosLGC", "DatosSAT");
                    var params = [];
                    var paramsTotal=0;
                    var ingresar;


                    $("#UserEditForm").submit(function( event ) {

                        var clickedSubmit = $(this).find('button[type=submit]:focus');
                        $(clickedSubmit).prop('disabled', true);

                        event.preventDefault();

                        var ExiteUsuario=false;

                        $.ajax({
                            // la URL para la petición
                            url : "{{ route('users.buscarusuario') }}",

                            // la información a enviar
                            // (también es posible utilizar una cadena de datos)
                            data :
                            {
                                email : $('#email').val(),
                                _token : "{{ csrf_token() }}"

                            },

                            // especifica si será una petición POST o GET
                            type : 'POST',

                            // el tipo de información que se espera de respuesta
                            dataType : 'json',

                            // código a ejecutar si la petición es satisfactoria;
                            // la respuesta es pasada como argumento a la función
                            success : function(json) {
                              
                              if(!$('#email').attr("disabled")){

                                if(json){                                    

                                    ExiteUsuario=true;
                              
                                }

                              }
                                

                            },

                            // código a ejecutar si la petición falla;
                            // son pasados como argumentos a la función
                            // el objeto de la petición en crudo y código de estatus de la petición
                            error : function(xhr, status) {

                            },

                            // código a ejecutar sin importar si la petición falló o no
                            complete : function(xhr, status) {

                            }
                        }).done(function(){

                            if(!ExiteUsuario){

                                $('#recontrasena').prop("disabled",true);

                                $.each( Model, function( key, value ) {

                                    paramsTotal = $(".row-details").find('[data-role='+value+']').length;

                                    if( paramsTotal != 0){

                                        var inputParams = {};
                                        inputParams["Model"] = value;
                                        ingresar=true;

                                        $(".row-details").find('[data-role='+value+']').each(function(i, input) {

                                            //if($(input).attr("id") != null){ 'data-type'="Select",
                                            if(!$(input).attr("disabled")){

                                                if($(input).attr('data-type') == 'Select'){

                                                    if($(input).find('option').length != 0){

                                                        $(input).find('option').each(function(){

                                                            if(inputParams[$(input).attr('data-name')] == undefined ){

                                                                inputParams[$(input).attr('data-name')] = $(this).val();
                                                            }
                                                            else
                                                            {

                                                                params.push(inputParams);
                                                                inputParams = {};
                                                                inputParams["Model"] = value;
                                                                inputParams[$(input).attr('data-name')] = $(this).val();

                                                            }
                                                        
                                                        
                                                        });

                                                    }
                                                    else{

                                                        ingresar=false;

                                                    }                                           

                                                }
                                                else
                                                {

                                                    if(inputParams[$(input).attr('data-name')] == undefined ){

                                                        inputParams[$(input).attr('data-name')] = $(input).val();
                                                    }
                                                    else
                                                    {

                                                        params.push(inputParams);
                                                        inputParams = {};
                                                        inputParams["Model"] = value;
                                                        inputParams[$(input).attr('data-name')] = $(input).val();

                                                    }

                                                }

                                            }
                                            else
                                            {
                                                ingresar=false;
                                            }
                                            
                                            //}

                                        });

                                        if(ingresar){

                                            params.push(inputParams);

                                        }                                

                                    }

                                });

                                json= JSON.stringify(params);

                                $('#estudios_documentos_cobertura_datos').val(json);
                                $('.no-submit').prop("disabled",true);
                                $('.input-sat').prop("disabled",true);
                                $('.input-lgc').prop("disabled",true);

                                event.target.submit();
                                
                            }
                            else
                            {

                                Swal.fire({
                                  type: 'warning',
                                  title: '¡Correo Electrónico ya exite!',
                                  text: '¡Ya hay un usuario registrado con ese correo electrónico!'
                                });

                                $(clickedSubmit).prop('disabled', false);
                                
                            }

                        });  

                    }); 


                    <?php if( $pluck["ubicacion"] != 'Mostrar'):?>

                        $('#idtipousuario').change(); 

                    <?php endif; ?>  


                    $('#origen').change(function() { return !$('#origen option:selected').remove().appendTo('#destino'); });  
                    $('#destino').change(function() { return !$('#destino option:selected').remove().appendTo('#origen'); });
                    $('.pasartodos').click(function() { $('#origen option').each(function() { $(this).remove().appendTo('#destino'); }); });
                    $('.quitartodos').click(function() { $('#destino option').each(function() { $(this).remove().appendTo('#origen'); }); });
                    $('.submit').click(function() { $('#destino option').prop('selected', 'selected'); });

                });
            </script>

            <script>
                $(".file").fileinput({
                    language: "es",
                    theme: "fas",
                    allowedFileExtensions: ["jpg","jpeg", "png", "gif"],
                    /*maxFileSize: 3072,
                    minImageWidth: 232,
                    minImageHeight: 155,
                    maxImageWidth: 650,
                    maxImageHeight: 433,*/
                    maxFileCount: 1
                });
            </script>

            <script type="text/javascript">
    function inputBtn(){
    var input=document.createElement('input');
    input.type="file";
    input.name="files[]";
    //without this next line, you'll get nuthin' on the display
    document.getElementById('target_div').appendChild(input);
}
</script>


        </div>
    </div>
@endsection

