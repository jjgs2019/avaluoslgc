@extends('plantillas.privada')
@section('content')
    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('vendor/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

    <!-- ANEXANDO NAVEGACION -->

    <!-- Mensajes-->
     @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <br>
    <!-- Configurar datos de privacidad de usuario-->
    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i>Configurar datos de privacidad de usuario
            </h5>
        </div>
    </div>
    <br>
    <div class="card shadow mb-4">
        <div class="card-body">
            <!-- FORMULARIO  USUARIOS -->
            {{ Form::model($User, ['action' => ['UsersController@sprivacidad'], 'id' => 'UserEditForm','method' => 'post','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                {!! Form::hidden('id', $User->id) !!}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-success d-none" role="alert" id="alertfinal">
                            <h3 class="alert-heading">Por favor espere <i class="fa fa-circle-notch fa-spin text-success"></i></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group{{ $errors->has('foto_perfil') ? ' has-error' : '' }}">
                            <input id="foto_perfil" name="foto_perfil" type="file" class="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione una imagen" data-allowed-file-extensions='["jpg","jpeg","png","gif"]'>
                            <script>
                                $("#foto_perfil").fileinput({
                                    language: "es",
                                    theme: "fas",
                                    allowedFileExtensions: ["jpg","jpeg", "png", "gif"],
                                    /*maxFileSize: 3072,
                                    minImageWidth: 232,
                                    minImageHeight: 155,
                                    maxImageWidth: 650,
                                    maxImageHeight: 433,*/
                                    maxFileCount: 1
                                });
                            </script>
                            @if ($errors->has('foto_perfil'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_perfil') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="text-center">
                            <div class="h4">Imagen de perfil</div>
                            <img class="img-fluid" src="{{ asset('storage/'.$User->foto_perfil) }}" alt="Avatar">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required{{ $errors->has('user') ? ' has-error' : '' }}">
                                    {{ Form::label('user ', 'Nombre de Usuario') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-user-alt"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::text(
                                                'user',
                                                $username,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'user',
                                                    'autofocus'=>'autofocus',
                                                    'required'=>true,
                                                    'placeholder'=>'Ingrese Nombre de usuario',
                                                    'minlength'=>'3',
                                                    'maxlength'=>'255',
                                                ]
                                            )
                                        }}
                                    </div>
                                    @if ($errors->has('user'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    {{ Form::label('password', 'Contraseña') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-key"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::password(
                                                'password',
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'password',
                                                    'placeholder'=>'Ingrese Contraseña',
                                                    'minlength'=>'6',
                                                    'maxlength'=>'255',
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                           <span class="input-group-text" onclick="mostrarPassword();" style="cursor:pointer">
                                                <i class="fa fa-eye-slash icon"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('recontrasena') ? ' has-error' : '' }}">
                                    {{ Form::label('recontrasena', 'Confirmación de contraseña') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-key"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::password(
                                                'recontrasena',
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'recontrasena',
                                                    'placeholder'=>'Confirme contraseña',
                                                    'minlength'=>'6',
                                                    'maxlength'=>'255',
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" onclick="mostrarPassword();" style="cursor:pointer">
                                                <i class="fa fa-eye-slash icon"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @if ($errors->has('recontrasena'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('recontrasena') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div id="alertPasswordCanti" class="col-sm-12 d-none">
                                    <div class="alert alert-danger"><div>Contraseña no segura.</div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <h5 class="h5 mb-0 text-gray-800">Datos personales</h5>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            {{ Form::label('nombre ', 'Nombre') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-file-signature"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'nombre',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'nombre',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese Nombre',
                                            'minlength'=>'3',
                                            'required'=>true,
                                            'maxlength'=>'255',
                                        ]
                                    )
                                }}
                            </div>
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required{{ $errors->has('email') ? ' has-error' : '' }}">
                            {{ Form::label('email', 'Correo electrónico') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                </div>
                                {{
                                    Form::email(
                                        'email',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'email',
                                            'required'=>true,
                                            'placeholder'=>'Ingrese Correo electrónico',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                        ]
                                    )
                                }}
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="boton">
                                <li class=" fas fa-file-signature"></li>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
            <!-- END FORMULARIO  USUARIOS -->
            <!-- SWEETALERT2-->
                <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->
            <script type="text/javascript">
                /**
                 * Permite mostrar contrasena visualmente
                 *
                 */
                function mostrarPassword(){
                    var password = document.getElementById("password");
                    var recontrasena = document.getElementById("recontrasena");
                    if((password.type == "password")&&(recontrasena.type == "password")){
                        password.type = "text";
                        recontrasena.type = "text";
                        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    }else{
                        password.type = "password";
                        recontrasena.type = "password";
                        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    }
                }

                $(document).ready(function() {
                    $('#password').keypress(function(event){
                        if($('#password').val().length<6){
                            $('#boton').prop("disabled",true);
                            $('#alertPasswordCanti').removeClass('d-none');
                        }else{
                            $('#alertPasswordCanti').addClass('d-none');
                            $('#boton').prop("disabled",false);
                        }
                    });
                    $('#recontrasena').focusout(function(event){
                        if($('#password').val()!=$('#recontrasena').val()){
                            Swal.fire({
                              type: 'warning',
                              title: '¡Contraseña no coinciden!',
                              text: '¡Las contreseñas suministradas no coinciden, por favor verifique!'
                            });
                            $('#boton').prop("disabled",true);
                        }else{
                            $('#alertPassword').addClass('d-none');
                            $('#boton').prop("disabled",false);
                        }
                    });
                });
            </script>
        </div>
    </div>
@endsection

