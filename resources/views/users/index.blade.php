@extends('plantillas.privada')
@section('content')
    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('users_index') }}

    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i>Usuarios
            </h5>
        </div>
    </div>
    <!-- SOLO PODRAN CREAR USUARIOS LOS ADMINISTRADORES SAT Y LGC -->
    <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'  ):?>

        <div class="row">
            <div class="col-12">
                <div class="text-right">
                    <a href="{{ route('users.create')}}">
                        <button type="button" class="btn btn-dark">
                            Nuevo Usuario
                        </button>
                    </a>
                </div>
            </div>
        </div>
        
    <?php endif; ?>
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <hr>
    <!-- TABLA DE USUARIOS -->
    <div class="card shadow mb-4">
        <div class="card-header bg-dark text-white h6 text-uppercase font-weight-bold">
            Lista de usuarios
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="datatable-User" class="table table-bordered table-striped table-hover table-vcenter">
                    <thead>
                        <tr>
                            <th class="text-left" style="width: 15%;">Tipo Usuario</th>
                            <th class="text-center" style="width: 45%;">Nombre</th>
                            <th class="text-center" style="width: 15%;">Correo</th>
                            <th class="text-center" style="width: 15%;">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($coleccion as $usuario)
                            <tr>
                                <td class="font-w600">
                                    {{ $usuario->tipousuario }}
                                </td>
                                <td class="font-w600">

                                    <?php if( $usuario->nombresat != '' && $usuario->nombresat != null):?>
                                        {{ $usuario->nombresat }}
                                    <?php endif;?>

                                    <?php if( $usuario->nombrelgc != '' && $usuario->nombrelgc != null):?>
                                        {{ $usuario->nombrelgc }} {{ $usuario->apellidopat }}
                                    <?php endif;?>

                                </td>
                                <td class="font-w600">
                                    {{ $usuario->correo }}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'  ):?>
                                            <a href="{{ route('users.view',$usuario->idusuario)}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver">
                                                <i class="fas fa-search-plus"></i>
                                            </a>
                                        <?php endif;?>
                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'  ):?>
                                            <a href="{{ route('users.edit',$usuario->idusuario)}}" class="btn btn-sm btn-warning ml-1" data-toggle="tooltip" data-placement="top" title="Editar">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        <?php endif;?>
                                        <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT' || Auth::user()->tipousuario['nombre'] == 'Administrador LGC'  ):?>
                                            <?php if($usuario->idusuario!=auth()->user()->id):?>
                                                {!! Form::open(['route' => ['users.delete', $usuario->idusuario], 'id'=>'FormUserEliminar'.$usuario->idusuario, 'method' => 'post']) !!}
                                                    <button type="button" class="btn btn-sm btn-danger ml-1" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar({{$usuario->idusuario}})">
                                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                {!! Form::close() !!}
                                            <?php endif;?>
                                        <?php endif;?>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- SWEETALERT2-->
        <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <!-- Page JS Plugins BOTONES -->
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/datatables/buttons/buttons.html5.min.js') }}"></script>
    <!-- FIN Page JS Plugins BOTONES -->

    <script>
        function eliminar(id){
            Swal.fire({
                title: '¿Seguro desea eliminar el usuario?',
                text: "Usted esta por eliminar el usuario",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '¡Si, estoy seguro!'
            }).then((result) => {
                if (result.value) {
                    $('#FormUserEliminar'+id).submit();
                }
            });
        }

        $(document).ready(function() {
            $('#boton_esconder_menu').click();

            var visible=false;
      
            <?php if( Auth::user()->tipousuario['nombre'] == 'Administrador SAT'):?>

                visible=true;

            <?php endif; ?>

            $('#datatable-User').DataTable( {
                "lengthMenu": [[25, 50, -1], [25, 50, "Todos"]],
                buttons:[
                {extend:"copy",className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="far fa-copy fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2]
                }},
                {extend:"excel",messageTop: 'Información de Usuarios ', title:'Listado de Usuarios', className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="far fa-file-excel fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2]
                }},
                {extend:"print",messageTop: 'Información de Usuarios', title:'Listado de Usuarios',className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="fa fa-print fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2]
                }},
                {extend:"pdf",messageTop: 'Información de Usuarios', title:'Listado de Usuarios',className:"btn btn-outline-primary js-click-ripple-enabled", text:'<i class="far fa-file-pdf fa-1x"></i>',
                    exportOptions: {
                    columns: [ 0, 1, 2]
                }}
                ],
                "columns": [
                    {data: "idtipousuario"},
                    {data: "nombre"}, 
                    {data: "correo"},                   
                    {data: "accion", name:'accion', className:'text-center'}
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                },
                dom:
                "<'row'<'col-sm-12 col-md-12 py-2 mb-2'<'text-right'B>>>"+
                "<'row'<'col-sm-12 col-md-6 py-2 mb-2'l><'col-sm-12 col-md-6 py-2 mb-2'f>>"+
                "<'row row-records'<'col-sm-12'tr>>"+
                "<'row row-info'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
            } );
        });
    </script>
@endsection

