@extends('plantillas.privada')
@section('content')
    <!-- ANEXANDO NAVEGACION -->
    {{ Breadcrumbs::render('users_mostrar',array('id'=>$User->id,'username'=>$username)) }}

    <div class="row">
        <div class="col-12">
            <h5 class="h5 mb-0 text-gray-800">
                <i class="fa fa-angle-right text-muted mr-1"></i>Usuario
            </h5>
        </div>
    </div>
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <br>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive ">
                <table class="table table-bordered  table-vcenter js-dataTable-full">
                    <tbody>
                        <tr>
                            <td rowspan="4">
                                <div class="text-center">
                                    <div class="h4">Imagen de perfil</div>
                                    <img class="img-fluid" src="{{ asset('storage/'.$User->foto_perfil) }}" alt="Avatar" style="max-width:150px;max-height:300px;">
                                </div>
                            </td>
                        </tr>
                        <tr>
                           <td colspan="2">Nombre de usuario: <strong><?php echo $username;?></strong> </td>
                           <td colspan="2">Correo: <strong><?php echo $User->email;?></strong> </td>
                        </tr>
                        <tr>
                           <td colspan="2">Nombres: <strong><?php echo $User->nombre;?></strong> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

